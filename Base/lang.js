var CURRENT = "fa";
export function ChangeLang() {
    CURRENT = CURRENT == "fa" ? "eng" : "fa";
}
export const FA = {
    Login :{
        phone : 'تلفن همراه',
        code : 'کد تایید ارسال شده',
        login : 'ورود',
        confirm: 'تایید',
        register : 'ثبت نام',
        firstName: 'نام',
        lastName: 'نام خانوادگی',
        invitationCode : 'کد معرف (اختیاری)',
        resendCode : 'ارسال مجدد کد',
        codeIsSended : 'کد تایید ارسال شده',
        secondsToResend : 'ثانیه برای امکان ارسال مجدد صبر کنید',
        enterPhone : 'تلفن همراه باید ۱۱ عدد باشد'
    },
    Base: {
        Titles : {
            menu : 'منو کاربری',
            dashboard : 'مدیریت رزروها',
            newReserve : 'رزرو جدید',
            currentReserve : 'رزرو جاری',
            auth : 'مدیریت رزروها',
            wallet : 'کیف پول',
            cars : 'خودروها',
            profile: 'پروفایل',
            guide: 'راهنما'
        },
        Footer : {
            newReserve : 'رزرو جای پارک جدید',
            return : 'بازگشت به داشبورد',
            sharje : 'افزایش موجودی حساب',
            support : 'ارتباط با پشتیبانی'
        },
        Loading: {
            wait: 'لطفا کمی صبر کنید',
            searchingParking : 'در حال جستجوی جای پارک',
        },
        Error :{
            simple: 'خطایی رخ داده، دوباره تلاش کنید',
            net : 'اتصال اینترنت را بررسی کنید',
            chooseCar : 'خودرو رزرو را انتخاب کنید'
        }
    },
    Home: {
        Dashboard : {
            Dashboard : {
                today : "امروز",
                otherDays : "روزهای آتی",
                currentReserve: "مشاهده رزرو جاری"
            },
            UserReserveItem : {
                from : 'از',
                to : "تا",
                changeCar: 'تغییر خودرو',
                cancelReserve : "لغو رزرو",
                alert : "گزارش خطا",
                enter : "ورود به پارکینگ"
            }
        },
        NewReserve: {
            chooseParking : 'انتخاب پارکینگ',
            chooseDay : 'انتخاب روز',
            chooseTime : 'انتخاب زمان',
            enterTime: 'زمان ورود',
            quitTime: 'زمان خروج',
            choose: 'انتخاب کنید',
            noStart: 'زود ترین',
            noEnd: 'بدون انتها',
            previous: "مرحله قبل",
            search: "جستجو جای پارک",
            enter: "زمان ورود را انتخاب کنید",
            quit:"زمان خروج را انتخاب کنید",
            confirm:"تایید",
            cancel: "لغو",
            chooseCar : "خودرو رزرو را انتخاب کنید",
            success : 'رزرو با موفقیت انجام شد!',
            confrim : 'تایید رزرو',
            isReserving : 'در حال رزرو جای پارک برای شما'
        }
    },
    Cars : {
        newCar : 'خودرو جدید',
        editCar : 'ویرایش خودرو',
        carName : 'نام خودرو',
        submitCar : 'ایجاد خودرو',
        submitChanges : 'تایید تغییرات',
        delete : 'حذف خودرو',
    },
    Wallet : {
        currentBalance : 'موجودی',
        currency : 'تومان',
        history : 'سوابق رزرو و تراکنش ها',
        ReserveInfo : {
            title : 'اطلاعات رزرو',
            duration : 'مدت پارک',
            code : 'کد پیگیری',
            totalPrice : 'کل',
        },
        Charge : {
            title : 'افزایش موجودی',
            priceTitle : 'مبلغ مورد نظر را وارد کنید',
            placeholder : 'مبلغ به تومان',
            pay: 'پرداخت'
        }
    },
    Menu : {
        dashboard : "مدیریت رزروها",
        cars : "خودروها",
        profile : "پروفایل",
        wallet : "سوابق رزرو و مالی",
        guide : 'راهنما',
        support : "تماس با پشتیبانی",
        logOut: "خروج از حساب",
    }
}
export const ENG = {
    Login :{
        phone : 'Username',
        code : 'Passcode',
        login : 'Login',
        register : 'Register',
        firstName: 'First Name',
        lastName: 'Last Name',
        invitationCode : 'Introducer code',
        resendCode : 'Resend Code',
        codeIsSended : 'Enter login code',
        secondsToResend : '',
        enterPhone : 'First enter your username',
        confirm: 'Confirm',
    },
    Base: {
        Titles : {
            menu : 'Menu',
            dashboard : 'Reserves',
            newReserve : 'New Reserve',
            currentReserve : 'Current Reserve',
            auth : 'Reserves',
            wallet : 'Wallet',
            cars : 'Cars',
            profile: 'Profile',
            guide: 'Help'
        },
        Footer : {
            newReserve : 'New Reserve',
            return : 'Return',
            sharje : 'Charge your account',
            support : 'Support'
        },
        Loading: {
            wait: 'Loading...',
            searchingParking : 'Searching...'
        },
        Error :{
            simple: 'An error occured!',
            net : 'Check network connection',
            chooseCar : 'First choose your car'
        }
    },
    Home: {
        Dashboard : {
            Dashboard : {
                today : "Today",
                otherDays : "Future",
                currentReserve: "See current reserve"
            },
            UserReserveItem : {
                from : 'From',
                to : "To",
                changeCar: "Change car",
                cancelReserve : "Cancel",
                alert : "Report",
                enter : "Enter parking"
            }
        },
        NewReserve: {
            chooseParking : 'Parking',
            chooseDay : 'Day',
            chooseTime : 'Time',
            enterTime: 'Enter',
            quitTime: 'Quit',
            choose: 'Choose',
            noStart: 'Earlier',
            noEnd: 'Endless',
            previous: "Previous step",
            search: "Search",
            enter: "Choose enter time :",
            quit:"Choose quit time :",
            confirm:"Confirm",
            cancel: "Cancel and reset",
            chooseCar : "Choose your car",
            success : 'Your parking successfully reserved!',
            confrim : 'Confirm',
            isReserving : 'Reserving...'
        }
    },
    Cars : {
        newCar : 'New car',
        editCar : 'Edit info',
        carName : 'Car name',
        submitCar : 'Submit',
        submitChanges : 'Submit changes',
        delete : 'Delete',
    },
    Wallet : {
        currentBalance : 'Balance',
        currency : '$',
        history : 'History',
        ReserveInfo : {
            title : 'Reserve info',
            duration : 'Duration (min)',
            code : 'Parking code',
            totalPrice : 'Total',
        },
        Charge : {
            title : 'Charge',
            priceTitle : 'Enter amount :',
            placeholder : 'Price in dollar',
            pay: 'Charge'
        }
    },
    Menu : {
        dashboard : "Dashboard",
        cars : "Cars",
        wallet : "Wallet and history",
        support : "Support",
        logOut: "Logout",
        guide : 'Guide',
        profile : "Profile",
    }
}
export const LANG = CURRENT == "fa" ? FA : ENG;