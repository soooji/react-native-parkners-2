import {Dimensions, Platform, PixelRatio} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { isIphoneX } from 'react-native-iphone-x-helper';
//Base
export const CURRENT_VERSION = '1.0.0';

export const SUPPORT_NUMBER = "09126653006"
export const BASE_URL = "http://dev.parkners.com/api/v1/"
export const BASE_IMG_URL = "http://dev.parkners.com"

export const USER_ROLE = "user"
var
persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
arabicNumbers  = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g];
export const FIX_NUMS = function (str)
{
  if(typeof str === 'string')
  {
    for(var i=0; i<10; i++)
    {
      str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
    }
  }
  return str;
};
///functions
export const ISEXPIRED = (error) => {
  if(error.response) {
      if(error.response.status == 403 || error.response.status == "403") {
          return true;
      } else {
          return false;
      }
  } else {
    return false
  }
}
export const SETTOKEN = token => {
  if(token != "")
      AsyncStorage.setItem("token",token);
  return
}
//Appearance
export const COLORS = {
  White: '#FFFFFF',
    Black:"#000000", //#000000
    Green: "#3ED135",
    Red: "#FF4747",
    DarkBlue: "#202042", //202042
    Orange: "#FFB647",
    LightGrey:"#F4F4FA",
    LighterGrey: "#DEDDE3",
    LightBlue: "#06D4E2",
    LightViolet: "#8A47FF",
    LightRed: "#FF5947",
    DarkGrey:"#AEAEAE"
};
export const FONTS = {
    thin: "IRANYekanMobileFN-Thin",
    light: "IRANYekanMobileFN-Light",
    regular: "IRANYekanMobileFN",
    medium:'IRANYekanMobileFN-Medium',
    bold:'IRANYekanMobileFN-Bold',
    extraBold:'IRANYekanMobileFN-ExtraBold',
    black:'IRANYekanMobileFN-Black',
    extraBlack:'IRANYekanMobileFN-ExtraBlack',
    secondFontFamily: "Kalameh-Bold"
}
export const FLEX = {
  CNwFsCS: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignContent: 'stretch'
  },
  RNwFsCS: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignContent: 'stretch'
  },
  RrNwFsCS: {
    display: 'flex',
    flexDirection: 'row-reverse',
    flexWrap: 'nowrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignContent: 'stretch'
  },
  RNwSbCS: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'stretch'
  },
  RrNwSbCS: {
    display: 'flex',
    flexDirection: 'row-reverse',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'stretch'
  },
  RNwSbFsS: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    alignContent: 'stretch'
  },
  RNwFeCS: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'flex-end',
    alignItems: 'center',
    alignContent: 'stretch',
  },
  RrNwFeCS: {
    display: 'flex',
    flexDirection: 'row-reverse',
    flexWrap: 'nowrap',
    justifyContent: 'flex-end',
    alignItems: 'center',
    alignContent: 'stretch',
  },
  RWSbFsFs : {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    alignContent: 'flex-start'
  },
  RWFsFsFs : {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    alignContent: 'flex-start'
  },
  RrWFsFsFs : {
    display: 'flex',
    flexDirection: 'row-reverse',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignContent: 'flex-start'
  },
  RWFsFsFs : {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
    alignContent: 'flex-start'
  },
  CWCC : {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  RNwSbCC : {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center'
  },
  RrNwSbCC : {
    display: 'flex',
    flexDirection: 'row-reverse',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignContent: 'center'
  },
  RNwCCC : {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center'
  },
  CNwSbSS: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'nowrap',
    justifyContent: 'space-between',
    alignItems: 'stretch',
    alignContent: 'stretch'
  }
}
export const BORDER_RADIUS = {
  base: 35,
  items: 20
};
export const ICONS_SIZE = 15;

// Paddings
export const PADDING = {
    box : 13,
    button : 11
}
//Font icon
import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from './font-config.json';
export const Sooji = createIconSetFromFontello(fontelloConfig);
//Font size
export const {width: WIDTH,height: HEIGHT} = Dimensions.get('window');
export const DIMENSIONS = Dimensions.get('window');
const scale = WIDTH / 320;
export function Normalize(size) {
  const newSize = size * scale 
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}
//Modal
export const MODAL_STYLES = {
    bottom: {
        outside : {
            justifyContent: "center",
            margin: 0,
            position:'relative',
        },
        inside : {
          marginTop:'auto',
          paddingBottom:0,
          borderTopLeftRadius:BORDER_RADIUS.base,
          borderTopRightRadius:BORDER_RADIUS.base,
          borderBottomLeftRadius:isIphoneX() ? BORDER_RADIUS.base : 0,
          borderBottomRightRadius:isIphoneX() ? BORDER_RADIUS.base : 0,
          backgroundColor:COLORS.LightGrey,
          overflow:'scroll',
          padding: 19,
          minHeight:200,
          maxHeight:DIMENSIONS.height - 200,
        },
    },
    center :{
      outside : {
        justifyContent: "center",margin: 0,position:'relative',
      },
      inside : {
        marginTop:'auto',
        marginBottom:'auto',
        marginLeft:20,
        marginRight:20,
        paddingBottom:0,
        borderRadius:BORDER_RADIUS.base,
        backgroundColor:COLORS.LightGrey,
        overflow:'scroll',
        padding: 19,
        minHeight:200,
        maxHeight:DIMENSIONS.height - 200,
      },
      titleBox: {
        width: DIMENSIONS.width - 140,backgroundColor:COLORS.White,borderRadius:BORDER_RADIUS.base,height: 49,paddingRight:13,
        alignContent:'center',justifyContent:'center'
      },
    },
    closeBox: {
      width: 49,height: 49,backgroundColor:COLORS.White,borderRadius:BORDER_RADIUS.base,alignItems:'center',justifyContent:'center'
    },
    titleBox: {
      width: DIMENSIONS.width - 100,
      backgroundColor:COLORS.White,
      borderRadius:BORDER_RADIUS.base,
      height: 49,
      paddingRight:13,
      alignContent:'center',
      alignItems:'center',
      justifyContent:'flex-end',
      flexDirection:'row',

    },
    titleText: {
      fontFamily:FONTS.bold,fontSize:Normalize(11),color:COLORS.DarkBlue,textAlign:'right',paddingRight:5
    }
}
// altert box
export const ALERT = {
  topFixBar: {
    width: DIMENSIONS.width,
    paddingHorizontal: 20,
    paddingTop:20,
    position:'absolute',
    zIndex:111111
  },
  alertBox: {
    position:'absolute',
    width: DIMENSIONS.width - 40,
    zIndex:111111111111111111,
    left: 20,
    backgroundColor:COLORS.Red,
    shadowColor: 'rgba(255,71,71,0.57)',
    shadowOffset: {
      width:0,
      height:8
    },
    shadowOpacity: 1,
    shadowRadius: 17,
    borderRadius:BORDER_RADIUS.items + 10,
    alignContent:'center',
    alignItems:'center',
    justifyContent:'space-between',
    flexDirection:'row',
  },
  alertBoxGreen: {
    position:'absolute',
    width: DIMENSIONS.width - 40,
    zIndex:111111111111111111,
    left: 20,
    backgroundColor:COLORS.Red,
    shadowColor: 'rgba(255,71,71,0.57)',
    shadowOffset: {
      width:0,
      height:8
    },
    shadowOpacity: 1,
    shadowRadius: 17,
    borderRadius:BORDER_RADIUS.items + 10,
    alignContent:'center',
    alignItems:'center',
    justifyContent:'space-between',
    flexDirection:'row',
  },
  alertBoxText: {
      fontFamily:FONTS.bold,
      fontSize: Normalize(11),
      color: COLORS.White,
      textAlign: 'center',
      lineHeight:16,
      paddingHorizontal:10,
      width:'100%'
  },
  closeAlertBox: {
    alignItems:'center',
    justifyContent:'center',
    position:'absolute',
    height:40,width:40,
    left:5,
    backgroundColor:'rgba(255,255,255,.1)',
    borderRadius:20
  },
}
//Functions
export const EXP_CHECK = (error) => {
    if(error.response != "" && error.response != null) {
        if(error.response.status == 403 || error.response.status == "403") {
            NavigationService.navigate('Auth');
            return "1";
        } else {
            return "-1";
        }
    } else {
        return "-2"
    }
}
export const SET_TOKEN = token => {
    if(token != "") {
        AsyncStorage.setItem("token",token);
    }
}
export const TEXT_TRUNCATE = (str, length, ending) => {
    if (length == null) {
      length = 100;
    }
    if (ending == null) {
      ending = '...';
    }
    if (str.length > length) {
      return str.substring(0, length - ending.length) + ending;
    } else {
      return str;
    }
  };
  export const FORMAT_MONEY =(n, c, d, t) => {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
      d = d == undefined ? "." : d,
      t = t == undefined ? "," : t,
      s = n < 0 ? "-" : "",
      i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
      j = (j = i.length) > 3 ? j % 3 : 0;
  
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t);
  }