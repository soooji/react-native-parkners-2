/**
 * Parkners react native app - Developed by Sajad Beheshti (@sooji)
 * https://github.com/soooji/
 *
 * @format
 * @flow
 */
// import {
//   Sentry,
//   SentrySeverity,
//   SentryLog
// } from 'react-native-sentry';

// Sentry.config("http://7cdfb437b1fa4779b81a14002b81d9f4@manaly.me/4", {
//   deactivateStacktraceMerging: false, // default: true | Deactivates the stack trace merging feature
//   logLevel: SentryLog.Debug, // default SentryLog.None | Possible values:  .None, .Error, .Debug, .Verbose
//   disableNativeIntegration: false, // default: false | Deactivates the native integration and only uses raven-js
//   handlePromiseRejection: true // default: true | Handle unhandled promise rejections
// }).install();

// Sentry.setEventSentSuccessfully((event) => {
// });

// Sentry.setShouldSendCallback((event) => {
//   return true; // if return false, event will not be sent
// });

// // export an extra context
// Sentry.setExtraContext({
//   "a_thing": 3,
//   "some_things": {"green": "red"},
//   "foobar": ["a", "b", "c"],
//   "react": true,
//   "float": 2.43
// });

// // set the tag context
// Sentry.setTagsContext({
//   "environment": "production",
//   "react": true
// });


import React, {Component} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {StyleSheet, Text, View, StatusBar,YellowBox,I18nManager,Animated,Easing,TouchableOpacity,TextInput,BackHandler,Platform,ActivityIndicator} from 'react-native';
YellowBox.ignoreWarnings([""]);
import NetInfo from "@react-native-community/netinfo";
import {logRequests} from 'react-native-requests-logger';
import Communications from 'react-native-communications';
import AppIntroSlider from 'react-native-app-intro-slider';

import chabokpush from 'chabokpush-rn';
import { PushNotificationIOS } from 'react-native';
import PushNotification from 'react-native-push-notification';
//en
import HeadBarEn from './Components/En/HeadBarEn'
import Parks from './Components/En/Parks'
import ParkInfo from './Components/En/ParkInfo';
//base
import BaseFrame from './Components/Base/BaseFrame'
import HeadBar from './Components/Base/HeadBar';
import FooterButton from './Components/Base/FooterButton'
import AnimeBaseFrame from './Components/Base/AnimeBaseFrame'
//contents
import { COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX,MODAL_STYLES,ICONS_SIZE,ALERT,BASE_IMG_URL,BASE_URL, ISEXPIRED, SETTOKEN, USER_ROLE, DIMENSIONS} from './Base/functions';
import NewReserve from './Components/Home/NewReserve/NewReserve';
import {FA,ENG} from './Base/lang'
import Dashboard from './Components/Home/Dashboard/Dashboard';
import CurrentReserve from './Components/Home/CurrentReserve/CurrentReserve';
import Login from './Components/Authen/Login';
import Wallet from './Components/Wallet/Wallet'
import Cars from './Components/Cars/Cars'
import Profile from './Components/Profile/index'
import HelpMenu from './Components/Help/HelpMenu'
import Menu from './Components/Base/Menu'
import { isIphoneX } from 'react-native-iphone-x-helper';
import { SUPPORT_NUMBER } from './Base/functions';
import axios from 'axios'

Text.defaultProps = { allowFontScaling: false };


const introStyles = StyleSheet.create({
  introImages: {
    width: DIMENSIONS.width,
    height: isIphoneX() ? DIMENSIONS.height - 200 :  DIMENSIONS.height - 100,
  },
})

const slides = [
  {
    key: 'first',
    title: '',
    text: '',
    image: require('./Static/intro1.png'),
    imageStyle: introStyles.introImages,
    backgroundColor: 'black',
  },
  {
    key: 'second',
    title: '',
    text: '',
    image: require('./Static/intro2.png'),
    imageStyle: introStyles.introImages,
    backgroundColor: 'black',
  },
  {
    key: 'third',
    title: '',
    text: '',
    image: require('./Static/intro3.png'),
    imageStyle: introStyles.introImages,
    backgroundColor: 'black',
  },
  {
    key: 'fourth',
    title: '',
    text: '',
    image: require('./Static/intro4.png'),
    imageStyle: introStyles.introImages,
    backgroundColor: 'black',
  },
  {
    key: 'fifth',
    title: '',
    text: '',
    image: require('./Static/intro5.png'),
    imageStyle: introStyles.introImages,
    backgroundColor: 'black',
  },
  {
    key: 'sixth',
    title: '',
    text: '',
    image: require('./Static/intro6.png'),
    imageStyle: introStyles.introImages,
    backgroundColor: 'black',
  }
];




TextInput.defaultProps = { ...TextInput.defaultProps,allowFontScaling: false };
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null, eventId: null,

      isIntroSeen:true,

      language : 'fa',
      langObject : FA,
      isRTL : true,
      isIr: false,
      isLocating: true,
      
      enPageTitle : 'Parks', //parks,ParkTitle
      enCurrentBase: 'main',
      enCurrentParkId: 0,
      enCurrentParkObject: {
        id:0,
        title: 'Loading...',
        img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Usa_edcp_relief_location_map.png/568px-Usa_edcp_relief_location_map.png',
        location: '...',
        info: `...`,
        map: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Usa_edcp_relief_location_map.png/568px-Usa_edcp_relief_location_map.png',
        history: `...`,
        link: 'https://en.wikipedia.org/'
      },

      currentBase: 'dashboard', //dashboard or new-reserve or current-reserve or wallet or cars or auth or help-menu
      timeBoxAnime: new Animated.Value(90),
      hasCurrentReserve: false,
      currentReserve: {
        id: 0
      },

      menuStatus: 'close',

      alertAnime: new Animated.Value(-90),
      alertText:'',

      startTime: new Date(),
      endTime: new Date(),
      countDownString: '...'
    } 
    I18nManager.allowRTL(false);
    logRequests();    
  }
  enGoBack() {
   this.setState({enCurrentParkId: 0,enPageTitle: 'Parks',enCurrentBase:'main',enCurrentParkObject:{id:0,title: 'Loading...',img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Usa_edcp_relief_location_map.png/568px-Usa_edcp_relief_location_map.png',location: '...',info: `...`,map: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Usa_edcp_relief_location_map.png/568px-Usa_edcp_relief_location_map.png',history: `...`,link: 'https://en.wikipedia.org/'}});
  }
  componentDidMount() {

    this.netInfoListener = NetInfo.isConnected.addEventListener('connectionChange', (isConnected)=>{if(!isConnected) {this.toggleAlert(true,this.state.langObject.Base.Error.net);}});

    if(Platform.OS == "ios") {
      this.checkReg();
    } else {
      this.setState({isIr: true,isLocating: false});
      AsyncStorage.setItem('reg','ir');

      this.backListener = BackHandler.addEventListener('hardwareBackPress', ()=>this.handleBackButton());

      this.getCurrentReserve();
      this.startUp();
    }
    this.initChabok();
    this.initPushNotification();
    PushNotification.setApplicationIconBadgeNumber(0);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', ()=>this.handleBackButton());
    this.netInfoListener.remove();
    this.backListener.remove();
  }
  // componentDidCatch(error, errorInfo) {
  //   this.setState({ error });
  //   Sentry.withScope(scope => {
  //       scope.setExtras(errorInfo);
  //       const eventId = Sentry.captureException(error);
  //       this.setState({eventId})
  //   });
  // }
  isIntroSeen = async ()=>{
    try {
      let reg = await AsyncStorage.getItem('isIntroSeen');
      this.setState({isIntroSeen: reg=="true"})
    } catch (error) {
      this.setState({isIntroSeen: false})
    }
  }
  checkReg = async ()=>{
    try {
      let reg = await AsyncStorage.getItem('reg');
      if(reg == "ir") {
        this.setState({isIr:true,isLocating: false});
        this.isIntroSeen().then(
          ()=>{
            this.getCurrentReserve();
            this.startUp();
          }
        )
      } else {
        this.checkip();
      }
    } catch (error) {
      this.checkip();
    }
  }
  checkip = async () => {
    try {
      var url = 'http://ip-api.com/json/';
      axios.get(url)
      .then((r) => {
        console.log(r.data);
        this.setState({
          countryName: r.data.country,
          regionName: r.data.countryCode
        });
        let cnt = r.data.country.toLocaleLowerCase();
        let code = r.data.countryCode.toLocaleLowerCase();
        if(code == "ir" || cnt.includes('iran') ) {
          AsyncStorage.setItem('reg','ir');
          this.setState({isIr: true,isLocating: false});
          this.isIntroSeen().then(
            ()=>{
              this.getCurrentReserve();
              this.startUp();
            }
          )
        } else {
          AsyncStorage.setItem('reg','out');
          this.setState({isIr: false,isLocating: false});
        }
      })
      .catch((error) => {
       console.error(error);
      });
   } catch(e) {
     alert('something went wrong! Close the app and open it again.');
   }
  }
  handleBackButton() {
    if(this.state.currentBase == 'auth') {
      return true;
    } else if (this.state.menuStatus == "open") {
      this.toggleMenu('close');
    } else if (this.state.currentBase != 'dashboard'){
      this.setState({currentBase:'dashboard'});
      return true;
    } else {
      return false;
    }
  }
changeLanguage() {
  this.setState({
    language : this.state.language == 'fa' ? 'eng' : 'fa',
    langObject : this.state.language == 'fa' ? ENG : FA,
    isRTL: this.state.language == 'fa' ? false : true,
  });
}
startUp = async () => {
  try {
      this.setState({isLoading:true});
  const token = await AsyncStorage.getItem('token');
  if(!token) {
      this.setState({isLoading:false,authState:'phone'});
      return
  }
  const options = {
      method: 'POST',
      url: `${BASE_URL}startup/`,
      headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
      data: JSON.stringify({
          'profile': {
              'app_version':CURRENT_VERSION,
              'platform_version': Platform.Version,
              'is_ios': Platform.OS == "ios"
          }
      }),
  };
  axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
      })
      .then(r=>{
          this.setState({isLoading:false});
          this.closeAll();
      })
      .catch(e=>{
          this.setState({isLoading:false});
          if(!e.response) {this.setState({authState:'phone'});return;}
          if(ISEXPIRED(e)) {this.setState({authState:'phone'});return;}
          if(e.response.status == 500) this.toggleAlert(true);
          else this.toggleAlert(true,e.response.data.error_message);
      })
  } catch (error) {
      this.setState({isLoading:false,authState:'phone'});
  }
}
getCurrentReserve = async () => {
  try {
    const token = await AsyncStorage.getItem('token');
    if(!token) {this.setCurrentBase("auth");return;}
    const options = {
        method: 'GET',
        url: `${BASE_URL}park_events/current/`,
        headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
    };
    axios(options)
    .then(r=>{
        SETTOKEN(r.data.token);
        this.setState({
          hasCurrentReserve:r.data.has_reserve,
          currentReserve: r.data.park_event ? r.data.park_event : {id:0},
          currentBase: r.data.has_reserve ? 'current-reserve' : 'dashboard'
        });
    })
    .catch(e=>{
        console.log(e);
        if(!e.response) {this.toggleAlert(true);return;}
        if(ISEXPIRED(e)) {this.setCurrentBase('auth');return;}
        if(e.response.status == 500) this.toggleAlert(true);
        else this.toggleAlert(true,e.response.data.error_message);
    })
  } catch (error) {
      console.log(error);
      this.setCurrentBase('auth');
  }
}
_onIntroDone = () => {
  AsyncStorage.setItem("isIntroSeen","true");
  this.setState({isIntroSeen: true});
}
  render() {
    if(this.state.isLocating) {
      return (
      <View style={[styles.container]}>
        <BaseFrame>
          <View style={[{height:'100%',width:'100%'},FLEX.RNwCCC]}>
            <ActivityIndicator color={'black'}/>
          </View>
        </BaseFrame>
      </View>
      )
    } else if(!this.state.isIntroSeen) {      
      return <AppIntroSlider slides={slides} onDone={this._onIntroDone} showSkipButton={true}/>;
    } else if(!this.state.isIr)
      return (
        <View style={styles.container}>
          <StatusBar animated={true} barStyle="light-content"/>
          <HeadBarEn isBackVisible={this.state.enCurrentParkId == 0 ? false : true} enGoBack={()=>this.enGoBack()} toggleTimer={(open)=>this.toggleTimer(open)} title={this.state.enPageTitle} menuStatus={this.state.menuStatus} toggleMenu={(status)=>this.toggleMenu(status)}/> 
          <BaseFrame>
            {this.state.enCurrentBase == "main" ? <Parks setParkObj={(obj)=>this.setState({enCurrentParkObject: obj,enCurrentParkId:obj.id,enCurrentBase:'info',enPageTitle:obj.title})}/> : null}
            {this.state.enCurrentBase == "info" ? <ParkInfo obj={this.state.enCurrentParkObject}/> : null}
          </BaseFrame>
          <FooterButton lang={this.state.langObject} isRTL={this.state.isRTL} backgroundColor={COLORS.Black} textColor={COLORS.White} title={"US Parks"} onPress={()=>{alert('Other countries will be available in future versions!')}}/>
        </View>  
      )
    else 
    return (
      <View style={styles.container}>
        <Animated.View style={[FLEX.RNwSbCC,ALERT.alertBox,{transform:[{translateY: this.state.alertAnime}],height:50}]}>
            <Text style={ALERT.alertBoxText}>{this.state.alertText}</Text>
        </Animated.View>

        <StatusBar animated={true} barStyle="light-content"/>
        <HeadBar  currentBase={this.state.currentBase} changeLanguage={()=>this.changeLanguage()} toggleTimer={(open)=>this.toggleTimer(open)} title={this.getPageTitle()} menuStatus={this.state.menuStatus} toggleMenu={(status)=>this.toggleMenu(status)}/>
        {this.state.currentBase == "auth" ?
        <Login lang={this.state.langObject} 
        notifReg={(userId)=>this.onRegisterTapped(userId)} 
        isRTL={this.state.isRTL} setCurrentBase={(name)=>this.setCurrentBase(name)}/> :
          <BaseFrame>

            <Animated.View style={[styles.countDownBox,{
              transform:[{translateY:this.state.timeBoxAnime}]
            }]}>
              <TouchableOpacity onPress={()=>{this.setState({currentBase:'current-reserve'});this.toggleTimer(false)}} activeOpacity={.8} style={{width:'100%',height:'100%'}}>
                <View style={[FLEX.RNwCCC,{height:'100%'}]}>
                  <Text style={styles.countDownTitle}>{this.state.langObject.Home.Dashboard.Dashboard.currentReserve}</Text>
                </View>
              </TouchableOpacity>
            </Animated.View>

            <AnimeBaseFrame baseState={this.state.menuStatus} componentStateName={"open"} isFullSize={true} startDelay={0}>
              <Menu lang={this.state.langObject} isRTL={this.state.isRTL} toggleTimer={(open)=>this.toggleTimer(open)} setCurrentBase={(name)=>this.setCurrentBase(name)} menuStatus={this.state.menuStatus} toggleMenu={(status)=>this.toggleMenu(status)} hasCurrentReserve={()=>this.hasCurrentReserve()}/>
            </AnimeBaseFrame>
            <AnimeBaseFrame baseState={this.state.currentBase} componentStateName={"wallet"} isFullSize={true} startDelay={700}>
              <Wallet lang={this.state.langObject} isRTL={this.state.isRTL}  ref={"wallet"} setCurrentBase={(name)=>this.setCurrentBase(name)} getCurrentBase={()=>this.getCurrentBase()}/>
            </AnimeBaseFrame>
            <AnimeBaseFrame baseState={this.state.currentBase} componentStateName={"new-reserve"} isFullSize={true} startDelay={700}>
              <NewReserve lang={this.state.langObject} isRTL={this.state.isRTL}  setCurrentBase={(name)=>this.setCurrentBase(name)}/>
            </AnimeBaseFrame>
            <AnimeBaseFrame baseState={this.state.currentBase} componentStateName={"dashboard"} isFullSize={true} startDelay={700}>
              <Dashboard lang={this.state.langObject} isRTL={this.state.isRTL}  setCurrentBase={(name)=>this.setCurrentBase(name)} setCurrentReserve={(has,id)=>this.setCurrentReserve(has,id)}/>
            </AnimeBaseFrame>
            <AnimeBaseFrame baseState={this.state.currentBase} componentStateName={"current-reserve"} isFullSize={true} startDelay={700}>
              <CurrentReserve lang={this.state.langObject} isRTL={this.state.isRTL} currentReserveId={this.state.currentReserve.id} setCurrentReserve={(has,id)=>this.setCurrentReserve(has,id)} setCurrentBase={(name)=>this.setCurrentBase(name)}/>
            </AnimeBaseFrame>
            <AnimeBaseFrame baseState={this.state.currentBase} componentStateName={"cars"} isFullSize={true} startDelay={700}>
              <Cars lang={this.state.langObject} isRTL={this.state.isRTL}  setCurrentBase={(name)=>this.setCurrentBase(name)} hasCurrentReserve={()=>this.hasCurrentReserve()}/>
            </AnimeBaseFrame>
            <AnimeBaseFrame baseState={this.state.currentBase} componentStateName={"profile"} isFullSize={true} startDelay={700}>
              <Profile lang={this.state.langObject} isRTL={this.state.isRTL}  setCurrentBase={(name)=>this.setCurrentBase(name)} hasCurrentReserve={()=>this.hasCurrentReserve()}/>
            </AnimeBaseFrame>
            <AnimeBaseFrame baseState={this.state.currentBase} componentStateName={"help-menu"} isFullSize={true} startDelay={700}>
              <HelpMenu lang={this.state.langObject} isRTL={this.state.isRTL}  setCurrentBase={(name)=>this.setCurrentBase(name)} hasCurrentReserve={()=>this.hasCurrentReserve()}/>
            </AnimeBaseFrame>
          </BaseFrame>
        }
        {this.state.currentBase == "auth" ? <BaseFrame/> : null}
        <FooterButton lang={this.state.langObject} isRTL={this.state.isRTL} backgroundColor={this.getDownButtonBackgroundColor()} textColor={this.getDownButtonTextColor()} title={this.getCurrentBaseDownButton()} onPress={()=>this.downButtonAction()}/>
      </View>
    );
  }
  


  //NTIF


  initChabok() {
    const authConfig = {
        devMode: true,
        appId: 'kasabuba',
        username: 'udrietuje',
        password: 'aveahuasuaz',
        apiKey: '1e0c3582b25e54b5a3badf3d0e0f577228546b47'
    };
    const options = {
        silent: true
    };
    this.chabok = new chabokpush(authConfig, options);
    this.chabok.setDefaultTracker('jtMMkQ');

    this.chabok.on('message', msg => {
        var phone = msg && msg.publishId && msg.publishId.split('/')[0];
        if (!phone) {
            phone = msg.channel.split('/')[0];
        }
        this.sendLocalPushNotification(msg,phone);
        var messageJson = this.getMessages() + JSON.stringify(msg);
        this.setState({messageReceived: messageJson});
    });

    this.chabok.on('closed', _ => {
        this.setState({
            connectionColor: 'red',
            connectionState: 'Closed'
        })
    });

    this.chabok.on('error', _ => {
        this.setState({
            connectionColor: 'red',
            connectionState: 'Error'
        })
    });

    this.chabok.on('connecting', _ => {
        this.setState({
            connectionColor: 'yellow',
            connectionState: 'Connecting'
        })
    });

    this.chabok.on('disconnected', _ => {
        this.setState({
            connectionColor: 'red',
            connectionState: 'Disconnected'
        })
    });
    this.chabok.on('connected', _ => {
        this.setState({
            connectionColor: 'green',
            connectionState: 'Connected'
        })
        this.chabok.getUserId()
            .then(userId => {
                this.setState({userId});
            });
    });

    this.chabok.getUserId()
        .then(userId => {
            if (userId) {
                this.setState({userId});
                this.chabok.register(userId);
            } else {
                this.chabok.registerAsGuest()
            }
        })
        .catch();
}

initPushNotification() {
    PushNotification.configure({
        onRegister:  ({token}) => {
            if(token){
                this.chabok.setPushNotificationToken(token)
            }
        },
        // (required) Called when a remote or local notification is opened or received
        onNotification: function(notification) {
            console.warn( 'NOTIFICATION:', notification );
            // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
            notification.finish(PushNotificationIOS.FetchResult.NoData);
        },
        senderID: "699589021268", // ANDROID ONLY: (optional) GCM Sender ID.
        permissions: {
            alert: true,
            badge: true,
            sound: true
        },
        popInitialNotification: true,
        requestPermissions: true,
    });
}

sendLocalPushNotification(msg, user) {
    const notifObject = {
        show_in_foreground: true,
        local_notification: true,
        priority: "high",
        message: msg.content,
        /* Android Only Properties */
        ticker: "My Notification Ticker", // (optional)
        autoCancel: true, // (optional) default: true
        largeIcon: "ic_launcher", // (optional) default: "ic_launcher"
        smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher"
        bigText: msg.content, // (optional) default: "message" prop
        color: "red", // (optional) default: system default
        vibrate: true, // (optional) default: true
        vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
        group: "group", // (optional) add group to message
        ongoing: false, // (optional) set whether this is an "ongoing" notification
        title: `New message from ${user}`, // (optional, for iOS this is only used in apple watch, the title will be the app name on other iOS devices)
        playSound: true, // (optional) default: true
        soundName: 'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
    };
    PushNotification.localNotification(notifObject);
    console.log(notifObject);
}

//  ----------------- Register Group -----------------
onRegisterTapped(userId) {
    if (userId) {
        this.chabok.register(userId);
    } else {
        console.warn('The userId is undefined');
    }
}
onUnregisterTapped() {
    this.chabok.unregister();
}
onSubscribeTapped() {
    if (this.state.channel) {
        this.chabok.subscribe(this.state.channel);
    } else {
        console.warn('The channel name is undefined');
    }
}
onUnsubscribeTapped() {
    if (this.state.channel) {
        this.chabok.unSubscribe(this.state.channel);
    } else {
        console.warn('The channel name is undefined');
    }
}

// ----------------- Publish Group -----------------
onPublishTapped() {
    const msg = {
        channel: "default",
        user: this.state.userId,
        content: this.state.messageBody || 'Hello world'
    };
    this.chabok.publish(msg)
}
onPublishEventTapped() {
    this.chabok.publishEvent('batteryStatus', {state: 'charging'});
}

//  ----------------- Tag Group -----------------
onAddTagTapped() {
    if (this.state.tagName) {
        this.chabok.addTag(this.state.tagName)
            .then(({count}) => {
                alert(this.state.tagName + ' tag was assign to ' + this.getUserId() + ' user with '+ count + ' devices');
            })
            .catch(_ => console.warn("An error happend adding tag ..."));
    } else {
        console.warn('The tagName is undefined');
    }
}
onRemoveTagTapped() {
    if (this.state.tagName) {
        this.chabok.removeTag(this.state.tagName)
            .then(({count}) => {
                alert(this.state.tagName + ' tag was removed from ' + this.getUserId() + ' user with '+ count + ' devices');
            })
            .catch(_ => console.warn("An error happend removing tag ..."));
    } else {
        console.warn('The tagName is undefined');
    }
}

//  ----------------- Track Group -----------------
onAddToCartTrackTapped() {
    this.chabok.track('AddToCard',{order:'200'});
}
onPurchaseTrackTapped() {
    this.chabok.track('Purchase',{price:'15000'});
}
onCommentTrackTapped() {
    this.chabok.track('Comment',{postId:'1234555677754d'});
}
onLikeTrackTapped() {
    this.chabok.track('Like',{postId:'1234555677754d'});
}

getUserId() {
    return this.state.userId || ''
}

getMessages() {
    if (this.state.messageReceived) {
        return this.state.messageReceived + '\n --------- \n\n';
    }
    return '';
}

getTagName() {
    return this.state.tagName || '';
}

getMessageBody() {
    return this.state.messageBody || '';
}


  //END NOTIF
  toggleAlert = (open,text = this.state.langObject.Base.Error.simple) => {
    if(open) {
        this.setState({alertText: text})
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: isIphoneX() ? 40 : 20,
          easing: Easing.elastic(1.2),
          duration: 1000,
          useNativeDriver:true
        },
      ).start();
      setTimeout(()=>{
        this.toggleAlert(false);
      },3000)
    } else {
      setTimeout(() => {
        this.setState({alertText: ''})
      }, 500);
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: -90,
          easing: Easing.ease,
          duration: 500,
          useNativeDriver:true
        },
      ).start();
    }
  }
  tick() {
    if(!this.state.isCountingDown)
        return
    var now = new Date;
    if (now > this.state.endTime) { // too late, go to tomorrow
        this.state.endTime.setDate(this.state.endTime.getDate() + 1);
    }
    var remain = ((this.state.endTime - now) / 1000);
    var hh = this.pad((remain / 60 / 60) % 60);
    var mm = this.pad((remain / 60) % 60);
    var ss = this.pad(remain % 60);
    this.setState({countDownString: hh + ":" + mm + ":" + ss})
    setTimeout(()=>this.tick(), 1000);
  }
  pad(num) {
    return ("0" + parseInt(num)).substr(-2);
  }
  setCurrentBase(name) {
    if(name=="dasahboard") {
      this.getCurrentReserve().then(()=>{
        this.setState({currentBase: name});    
      })
    } else {
      this.setState({currentBase: name});
    }
    if(name=="current-reserve" || name=="new-reserve") {
       this.toggleTimer(false);
    } else {
        this.toggleTimer(true);
    }
  }
  hasCurrentReserve() {
    return this.state.hasCurrentReserve
  }
  setCurrentReserve = async (has,id) => {
    await this.setState({hasCurrentReserve: has,currentReserve:{id:id}})
  }
  toggleMenu(status) {
    this.setState({menuStatus:status});
    if(status == 'close') {
      if(this.state.currentBase != "current-reserve" && this.state.hasCurrentReserve) {
        this.toggleTimer(true); 
      } 
    }
  }
  getCurrentBaseDownButton() {
    if(this.state.menuStatus=="open") {
      return "بازگشت"
    }
    switch (this.state.currentBase) {
      case 'dashboard':
        return this.state.langObject.Base.Footer.newReserve
      case 'new-reserve':
        return this.state.langObject.Base.Footer.return
      case 'current-reserve':
        return this.state.langObject.Base.Footer.return
      case 'wallet':
        return this.state.langObject.Base.Footer.sharje
      case 'cars':
        return this.state.langObject.Base.Footer.return
      case 'profile':
        return this.state.langObject.Base.Footer.return
      case 'help-menu':
        return this.state.langObject.Base.Footer.return
      default:
        return this.state.langObject.Base.Footer.support
    }
  }
  toggleTimer = (open) => {
      if(open) {
        if(!this.state.hasCurrentReserve) {
          return
        }
          Animated.timing(
            this.state.timeBoxAnime,
            {
              toValue: -20,
              easing: Easing.elastic(1.2),
              duration: 1000,
              useNativeDriver:true
            },
          ).start();
      } else {
        Animated.timing(
          this.state.timeBoxAnime,
          {
            toValue: 90,
            easing: Easing.ease,
            duration: 500,
            useNativeDriver:true
          },
        ).start();
      // }
      }
  }
  getPageTitle() {
    if(this.state.menuStatus=="open") {
      return this.state.langObject.Base.Titles.menu
    }
    switch (this.state.currentBase) {
      case 'dashboard':
        return this.state.langObject.Base.Titles.dashboard
      case 'new-reserve':
        return this.state.langObject.Base.Titles.newReserve
      case 'current-reserve':
        return this.state.langObject.Base.Titles.currentReserve
      case 'auth':
        return this.state.langObject.Base.Titles.auth
      case 'wallet':
        return this.state.langObject.Base.Titles.wallet
      case 'cars':
        return this.state.langObject.Base.Titles.cars
      case 'profile':
        return this.state.langObject.Base.Titles.profile
      case 'help-menu':
        return this.state.langObject.Base.Titles.guide
      default:
        return ''
    }
  }
  downButtonAction() {
    if(this.state.menuStatus=="open") {
      this.setState({menuStatus:"close"});
      return
    }
    switch (this.state.currentBase) {
      case 'dashboard':
        this.setCurrentBase('new-reserve');
        return;
      case 'new-reserve':
        this.setCurrentBase('dashboard');
        return;
      case 'current-reserve':
        this.setCurrentBase('dashboard');
        return;
      case 'auth':
        Communications.phonecall(SUPPORT_NUMBER,true);
        return;
      case 'wallet':
        this.refs.wallet.toggleChargeModal()
        return;
      case 'cars':
        this.setCurrentBase('dashboard');
        return;
      case 'profile':
        this.setCurrentBase('dashboard');
        return;
      default:
        this.setCurrentBase('dashboard');
        return;
    }
  }
  getDownButtonTextColor() {
    if(this.state.currentBase == "dashboard" || this.state.currentBase == "wallet") {
      if(this.state.menuStatus=="close") {
        return COLORS.White;
      } else {
        return COLORS.DarkBlue;
      }
    } else {
      return COLORS.DarkBlue;
    }
  }
  getDownButtonBackgroundColor() {
    if(this.state.currentBase == "dashboard" || this.state.currentBase == "wallet") {
      if(this.state.menuStatus=="close") {
        return COLORS.Green;
      } else {
        return COLORS.LightGrey;
      }
    } else {
      return COLORS.LightGrey;
    }
  }
}


const styles = StyleSheet.create({

  container: {
    position:'relative',
    height: '100%',
    width: '100%',
    backgroundColor: COLORS.Black
  },
  countDownBox: {
    height:50,
    bottom:0,
    position:'absolute',
    width: DIMENSIONS.width - 40,
    zIndex:111111111111111111,
    left: 20,    
    backgroundColor:COLORS.Green,
    shadowColor: 'rgba(62,209,53,0.57)',
    shadowOffset: {
      width:0,
      height:8
    },
    shadowOpacity: 1,
    shadowRadius: 17,
    borderRadius:BORDER_RADIUS.items + 10,
  },
  countDownTitle: {
      fontFamily:FONTS.bold,
      fontSize: Normalize(11.5),
      color: COLORS.White,
      textAlign: 'right',
      // paddingRight:20,
  },
  countDownText: {
      fontFamily:FONTS.bold,
      fontSize: Normalize(11.5),
      color: COLORS.White,
      textAlign: 'left',
      paddingLeft:20,
  }
});
