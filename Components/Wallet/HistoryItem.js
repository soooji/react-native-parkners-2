import React, { Component } from 'react'
import { Text, View , StyleSheet,TouchableOpacity,Image} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX,MODAL_STYLES,ICONS_SIZE,ALERT,BASE_IMG_URL,BASE_URL, ISEXPIRED, SETTOKEN, USER_ROLE, DIMENSIONS, Sooji, SUPPORT_NUMBER} from './../../Base/functions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Modal from "react-native-modal";
import axios from 'axios'
export default class HistoryItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisibleResultModal: false,
      isLoading: true,
      isInfoLoaded: false
    }
  }
  componentDidMount() {
  }
  getCurrentReserve = async () => {
    try {
      this.props.toggleLoading();
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'GET',
          url: `${BASE_URL}park_events/${this.props.eventId}/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
      };
      axios(options)
      .then(r=>{
            SETTOKEN(r.data.token);
            this.setState({currentReserve:r.data.park_event});
      })
      .then(r=>{
        this.props.toggleLoading();
        this.getTransactionInfo();
      })
      .catch(e=>{
          console.log(e);
          this.props.toggleLoading();
          if(!e.response) {this.props.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status == 500) this.props.toggleAlert(true);
          else this.props.toggleAlert(true,e.response.data.error_message);
      })
    } catch (error) {
        console.log(error);
        this.props.toggleLoading();
        this.props.setCurrentBase('auth');
    }
  }
  getTransactionInfo = async () => {
    if(this.props.currentReserveId==0) return;
    try {
      this.setState({isLoading:true,isVisibleQuitConfirmModal:false});
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'GET',
          url: `${BASE_URL}park_events/${this.props.eventId}/transactions/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
      };
      axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
          this.setState({transaction: r.data})
      })
      .then(r=>{
          this.setState({isVisibleResultModal:true,isInfoLoaded:true});
      })
      .catch(e=>{
          console.log(e);
          this.setState({isLoading:false});
          if(!e.response) {this.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status == 500) this.toggleAlert(true);
          else this.toggleAlert(true,e.response.data.error_message);
      })
    } catch (error) {
        console.log(error);
        this.setState({isLoading:false});
        this.props.setCurrentBase('auth');
    }
  }
  closeResultModal() {
    this.setState({isVisibleResultModal: false},()=>{
      setTimeout(()=>{
        this.setState({isInfoLoaded:false});
      },400)
    })
  }
  render() {
    return (
      <TouchableOpacity activeOpacity={1} onPress={()=>{this.props.isParkEvent ? this.getCurrentReserve() : null}} style={styles.container}>
        <Modal
            // backdropColor={'rgba(140,140,140,.75)'}
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleResultModal}
            onBackdropPress={() => this.closeResultModal()}
            onBackdropPress={() => this.closeResultModal()}
            backdropOpacity={.4}
            style={MODAL_STYLES.bottom.outside}
        >
          {this.state.isInfoLoaded ?
            <View style={[MODAL_STYLES.bottom.inside]}>

              <View style={[{width:'100%',marginBottom:15},this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
                <TouchableOpacity
                onPress={() => this.closeResultModal()}
                activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={[MODAL_STYLES.titleBox,this.props.isRTL ? FLEX.RNwFeCS : FLEX.RrNwFeCS]}>
                  <Text style={MODAL_STYLES.titleText}>{this.props.lang.Wallet.ReserveInfo.title}</Text>
                </View>
              </View>
                
            <View style={[styles.resultInfoBox,this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
                <Text style={[styles.infoTextDetail,{fontFamily: this.props.isRTL ? FONTS.extraBold : FONTS.secondFontFamily}]}>
                    {this.state.transaction.park_length.minute}
                </Text>
                <Text style={styles.infoTextTitle}>
                  {this.props.lang.Wallet.ReserveInfo.duration}
                </Text>
            </View>

            <View style={[styles.resultInfoBox,this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
              <Text style={[styles.infoTextDetail,{fontFamily: this.props.isRTL ? FONTS.extraBold : FONTS.secondFontFamily}]}>
                {this.state.transaction.park_event_code}
                </Text>
                <Text style={styles.infoTextTitle}>
                  {this.props.lang.Wallet.ReserveInfo.code}
                </Text>
            </View>
            
            <View style={[styles.resultInfoBox,{paddingTop:5,marginBottom:20}]}>

                {/* <View style={[styles.detailBox,this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
                    <View style={[FLEX.RNwFeCS]}>
                        <Icon name="minus-circle" solid={true} color={COLORS.Red} style={{paddingRight:5}} size={Normalize(ICONS_SIZE - 2)}/>
                        <Text style={[styles.infoTextDetail,{paddingTop:4,fontFamily: this.props.isRTL ? FONTS.extraBold : FONTS.secondFontFamily}]}>
                            5600
                        </Text>
                    </View>                    
                    <View style={[FLEX.RNwFsCS]}>
                        <Text style={styles.infoTextTitle}>
                            Base price
                        </Text>
                    </View>
                </View> */}
                {this.state.transaction ? this.state.transaction.transactions.map(
                    (v,k)=>
                    <View key={k} style={[styles.detailBox,this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
                          <View style={[FLEX.RNwFeCS]}>
                              {/* <Icon name="plus-circle" solid={true} color={COLORS.Green} style={{paddingRight:5}} size={Normalize(ICONS_SIZE - 2)}/> */}
                              <Text style={[styles.infoTextDetail,{paddingTop:4,fontFamily: this.props.isRTL ? FONTS.extraBold : FONTS.secondFontFamily}]}>
                                  {v.amount}
                              </Text>
                          </View>      
                          <View style={[this.props.isRTL ? FLEX.RNwFsCS : FLEX.RrNwFsCS]}>
                            {
                              v.point > 0 ?
                              <View style={[FLEX.RNwFsCS,{marginRight:10,marginLeft:10,backgroundColor:COLORS.LightGrey,borderRadius:BORDER_RADIUS.items - 10,padding:5}]}>
                                  <Image source={require('./../../Static/orange-gem.png')} resizeMode={"contain"} style={styles.gem}/>
                                  <Text style={styles.gemCount}>{v.point}</Text>
                              </View> : null
                            }
                              <Text style={styles.infoTextTitle}>
                                  {v.title}
                              </Text>
                          </View> 
                      </View>
                  ) : null
                }

                <View style={[styles.totalBox,this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
                    <View style={[FLEX.RNwFeCS]}>
                        <Text style={[styles.totalPrice,{color:this.state.transaction.total < 0 ? COLORS.Red : COLORS.Green,paddingTop:4}]}>
                          {this.state.transaction.total}
                        </Text>
                    </View>        
                    <View style={[FLEX.RNwFeCS]}>
                        <Text style={[styles.totalCurrency]}>
                        {this.props.isRTL ? this.props.lang.Wallet.currency : ''}
                        </Text>
                        <Text style={styles.totalTitle}>
                          {this.props.lang.Wallet.ReserveInfo.totalPrice}
                        </Text>
                    </View>            
                </View>
            </View>

            </View>
          : null}
        </Modal>
       
        <View style={[styles.flagBox,FLEX.RNwSbCC]}>
          <Text style={styles.flagBoxTextCurrency}></Text>
          <Text style={styles.flagBoxTextTitle}>{this.props.isParkEvent ? <Icon name="eye" color={COLORS.White} size={Normalize(ICONS_SIZE - 2)}/> : this.props.flagTitle}</Text>
        </View>

        <View>
        {this.props.parkingName ?
          <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
            {this.props.isParkEvent ? <Icon name={"parking"} color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE)}/> : <Sooji name={"dollar"} style={{marginRight:-3}} color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE)}/>}
            <Text style={[{fontFamily:FONTS.bold,fontSize: Normalize(14)},styles.infoText]}>
              {this.props.parkingName}
            </Text>
          </View>:null}

        {this.props.parkingDate ?
          <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
            <Icon name="calendar" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE)}/>
            <Text style={[{fontFamily:FONTS.bold,fontSize: Normalize(14)},styles.infoText]}>
              {this.props.parkingDate}
            </Text>
          </View>:null}

        </View>    
      </TouchableOpacity>
    )
  }
}
const styles = StyleSheet.create({
    gemCount:{
        fontFamily: FONTS.bold,
        textAlign:'right',
        color:COLORS.Orange,
        fontSize:Normalize(12),
        // marginRight:13,
        paddingTop:3,
    },
    gem: {
        marginRight:5,
        width:17,
        height:17
    },
    totalCurrency: {
        paddingRight:5,
        color:COLORS.DarkBlue,
        fontFamily: FONTS.light,
        textAlign:'left',
        fontSize:Normalize(10)
    },
    totalPrice: {
        fontFamily: FONTS.extraBold,
        textAlign:'right',
        fontSize:Normalize(17)
    },
    totalTitle: {
        color:COLORS.DarkBlue,
        fontFamily: FONTS.extraBold,
        textAlign:'right',
        fontSize:Normalize(13)
    },
    totalBox: {
        paddingTop:15,
        paddingHorizontal:5
    },
    detailBox: {
        borderBottomWidth:1,
        borderBottomColor:COLORS.LightGrey,
        paddingVertical:15,
        paddingHorizontal:5
    },
    infoTextDetail: {
        color:COLORS.DarkBlue,
        textAlign:'right',
        fontSize:Normalize(12)
    },
    infoTextTitle: {
        color:COLORS.DarkBlue,
        fontFamily: FONTS.bold,
        textAlign:'right',
        // paddingRight:10,
        fontSize:Normalize(12)
    },
    resultInfoBox: {
        backgroundColor:COLORS.White,
        borderRadius:BORDER_RADIUS.items,
        paddingVertical:18,
        paddingHorizontal: 17,
        marginBottom:13
    },
    container: {
        width:'100%',
        height: 'auto',
        // minHeight: 80,
        position:'relative',
        overflow: 'hidden',
        paddingHorizontal: 15,
        paddingBottom: 15,
        paddingTop:7,
        backgroundColor: COLORS.LightGrey,
        borderRadius: BORDER_RADIUS.items,
        marginBottom:10
    },
    flagBox: {
      position:'absolute',
      left: 0,
      top: 0,
      backgroundColor: COLORS.DarkBlue,
      borderBottomRightRadius: 25,
      paddingVertical: 12,
      paddingHorizontal: 12,
    },
    flagBoxTextTitle: {
      color: COLORS.White,
      fontSize: Normalize(14),
      fontFamily: FONTS.extraBold,
      paddingHorizontal:5,
    },
    flagBoxTextCurrency: {
      color: COLORS.White,
      fontSize: Normalize(9),
      fontFamily: FONTS.medium,
    },
    infoText: {
      color: COLORS.DarkBlue,
      paddingTop:2,
      paddingRight:10
    },
    infoBox: {
      paddingTop:8,
    },
    arrowBox: {
      zIndex:1111111,
      alignItems: 'center',
      justifyContent: 'center',
      width: 44,
      height: 44,
      position:'absolute',
      borderRadius:20,
      left: 5,
      bottom: 5
    },
    carTitle: {
      color: COLORS.DarkBlue,
      fontSize: Normalize(11),
      textAlign:'center',
      fontFamily: FONTS.bold,
    },
    whiteTouchBox: {
      backgroundColor:COLORS.White,
      paddingVertical: 15,
      borderRadius:BORDER_RADIUS.items,
      marginTop: 15
    },
    submitButton: {
      width: DIMENSIONS.width - 130,
      alignItems:'center',
      justifyContent:'center',
      height: 44,
      marginLeft:'auto',
      backgroundColor:COLORS.Green,
      borderRadius:BORDER_RADIUS.items,
      marginTop: 12
    },
    submitText: {
      color: COLORS.White,
      fontSize: Normalize(12),
      textAlign:'center',
      fontFamily: FONTS.extraBold
    },
    carItemBox: {
      backgroundColor:COLORS.White,borderRadius:BORDER_RADIUS.items,paddingVertical:20,paddingHorizontal:15,marginTop:9
    },
    carName: {
      fontFamily:FONTS.medium,fontSize:Normalize(11),color:COLORS.DarkBlue,textAlign:'right'
    },
    carImage: {
      width:25,height:25
    }
})