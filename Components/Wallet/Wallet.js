import React, { Component } from 'react'
import { Text, View,StyleSheet,TouchableOpacity,Animated,Easing,ScrollView,TextInput} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX,MODAL_STYLES,ICONS_SIZE,ALERT,BASE_IMG_URL,BASE_URL, ISEXPIRED, SETTOKEN, USER_ROLE, DIMENSIONS, Sooji, FIX_NUMS, FORMAT_MONEY} from '../../Base/functions';
import HistoryItem from './HistoryItem';
import FullScreenLoading from './../Base/FullScreenLoading';
import Modal from "react-native-modal";
import axios from 'axios'
import Communications from 'react-native-communications';
export default class Wallet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisibleChargeModal: false,
      priceToCharge: '',
      balance: 0,
      transactions:[],

      isLoading: false,
      alertAnime: new Animated.Value(-90),
      alertText:'',
      loadingText: '',
    }
  }
  componentDidMount() {
    this.setState({loadingText:this.props.lang.Base.Loading.wait,alertText:this.props.lang.Base.Error.simple});
    this.enterParking();
  }
  
  enterParking = async () => {
    try {
      this.setState({isLoading:true});
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'GET',
          url: `${BASE_URL}wallet/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
      };
      axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
          this.setState({balance: r.data.total,transactions:r.data.transactions});
      })
      .then(r=>{
        this.setState({isLoading:false});
      })
      .catch(e=>{
          console.log(e);
          this.setState({isLoading:false});
          if(!e.response) {this.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status == 500) this.toggleAlert(true);
          else this.toggleAlert(true,e.response.data.error_message);
      })
    } catch (error) {
        console.log(error);
        this.setState({isLoading:false});
        this.props.setCurrentBase('auth');
    }
  }
  charge = async ()=> {
    try {
      this.setState({isLoading:true});
      const token = await AsyncStorage.getItem('token');
      const phone = await AsyncStorage.getItem('phone');
      if(!token) {this.props.setCurrentBase("auth");return;}
      this.setState({isVisibleChargeModal:false});
      this.setState({isLoading:false});
      Communications.web(`${BASE_URL}zarinpal/pay/${FIX_NUMS(`${this.state.priceToCharge}`)}/${phone}/`);
    } catch (error) {
        console.log(error);
        this.setState({isLoading:false});
        this.props.setCurrentBase('auth');
    }
  }
  toggleChargeModal() {
    this.setState({isVisibleChargeModal:!this.state.isVisibleChargeModal});
  }
  render() {
    return (
      <View style={{width:'100%',height:'100%'}}>
        <FullScreenLoading isLoading={this.state.isLoading} text={this.state.loadingText}/>
        
        <Animated.View style={[FLEX.RNwSbCC,ALERT.alertBox,{
          transform: [{translateY:this.state.alertAnime}]
          ,height:50}]}>
            <Text style={ALERT.alertBoxText}>{this.state.alertText}</Text>
        </Animated.View>
        {/* Charge Modal */}
        <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleChargeModal}
            onBackdropPress={() => {this.setState({isVisibleChargeModal: false,priceToCharge:''});}}
            onBackdropPress={() => {this.setState({isVisibleChargeModal: false,priceToCharge:''});}}
            backdropOpacity={.4}
            style={MODAL_STYLES.center.outside}
        >
            <View style={[MODAL_STYLES.center.inside]}>

              <View style={[{width:'100%',marginBottom:15},this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
                <TouchableOpacity onPress={() => {this.setState({isVisibleChargeModal: false,priceToCharge:''});}} activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={[MODAL_STYLES.center.titleBox,this.props.isRTL ? FLEX.RNwFeCS : FLEX.RrNwFeCS]}>
                  <Text style={MODAL_STYLES.titleText}>{this.props.lang.Wallet.Charge.title}</Text>
                </View>
              </View>
              <View style={{width:'100%',height:'auto',paddingBottom:20}}>
                <Text style={{fontFamily:FONTS.bold,textAlign:'center',color:COLORS.DarkBlue,width:'100%',paddingTop:5,paddingBottom:15,fontSize:Normalize(13)}}>
                  {this.props.lang.Wallet.Charge.priceTitle}
                </Text>
                <TextInput
                    placeholderTextColor={COLORS.DarkGrey}
                    // value={FORMAT_MONEY(this.state.priceToCharge,'.',',')}
                    value={this.state.priceToCharge}
                    style={styles.input}
                    editable={!this.state.isLoading}
                    clearButtonMode={'while-editing'}
                    placeholder={this.props.lang.Wallet.Charge.placeholder}
                    keyboardType={"number-pad"}
                    autoCorrect={false}
                    onChangeText={(text) => this.setState({priceToCharge: text})}
                    keyboardAppearance={"dark"}
                />
                <TouchableOpacity onPress={()=>this.charge()} style={styles.chargeButton} activeOpacity={.85}>
                  <Text style={styles.chargeButtonText}>{this.props.lang.Wallet.Charge.pay}</Text>
                </TouchableOpacity>
              </View>
            </View>
        </Modal>

        <ScrollView style={{width:'100%',height:'100%'}} contentContainerStyle={{paddingHorizontal:20,paddingTop:20,paddingBottom:80}}>
          <View style={[styles.currentBalanceBox,this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
            <Text style={[styles.currentBalancePrice,{fontFamily: this.props.isRTL ? FONTS.extraBold : FONTS.secondFontFamily,color:COLORS.Green}]}>
              {FORMAT_MONEY(this.state.balance,'.',',')}
            </Text>
            <View style={[this.props.isRTL ? FLEX.RNwFeCS :FLEX.RrNwFeCS ]}>
              <Text style={styles.currentBalancePriceCurrency}>
                  ({this.props.lang.Wallet.currency})
              </Text>
              <Text style={styles.currentBalanceTitle}>
                {this.props.lang.Wallet.currentBalance}
              </Text>
            </View>
          </View>
        
          <View style={styles.historyTitle}>
            <Text style={styles.historyTitleText}>
              {this.props.lang.Wallet.history}
            </Text>
          </View>

          <View style={{width:'100%',height:'100%',marginTop:10}}>

          {
            this.state.transactions.map(
              (v,k)=>
              <HistoryItem key={k} lang={this.props.lang} isRTL={this.props.isRTL} toggleAlert={(open,text=this.props.lang.Base.Error.simple)=>this.toggleAlert(open,text)}
                parkingName={v.title}
                toggleLoading={()=>this.setState({isLoading: !this.state.isLoading})}
                parkingDate={`${v.date_created.year}/${v.date_created.month}/${v.date_created.day}`}
                flagTitle={v.total}
                isParkEvent={v.is_park_event_transaction}
                eventId={v.id}
              />
            )
          }
            
            
          </View>
        </ScrollView>
      </View>
    )
  }
  toggleAlert = (open,text = this.props.lang.Base.Error.simple) => {
    if(open) {
        this.setState({alertText: text})
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: 20,
          easing: Easing.elastic(1.2),
          duration: 1000,
          useNativeDriver:true
        },
      ).start();
      setTimeout(()=>{
        this.toggleAlert(false);
      },3000)
    } else {
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: -90,
          easing: Easing.ease,
          duration: 500,
          useNativeDriver:true
        },
      ).start();
      setTimeout(() => {
        this.setState({alertText: ''})
      }, 500);
    }
  }
}
const styles = StyleSheet.create({
  chargeButtonText: {
    fontFamily:FONTS.bold,
    color:COLORS.White,
    fontSize:Normalize(13),
    textAlign:'center'
  },
  chargeButton: {
    marginTop:15,
    backgroundColor:COLORS.Green,
    paddingVertical:15,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:BORDER_RADIUS.items,
  },
  input: {
    backgroundColor:COLORS.White,
    borderRadius:BORDER_RADIUS.base,
    paddingVertical:17,
    textAlign:'center',
    width:'100%',
    color:COLORS.Black,
    fontFamily:FONTS.bold,
    fontSize:Normalize(13),
},
  currentBalanceBox: {
    backgroundColor:COLORS.LightGrey,
    borderRadius: BORDER_RADIUS.items,
    paddingVertical: 25,
    paddingHorizontal: 17
  },
  currentBalancePrice: {
    textAlign:'left',
    fontSize: Normalize(21)
  },
  currentBalanceTitle: {
    textAlign:'right',
    fontFamily: FONTS.bold,
    fontSize: Normalize(12),
    color:COLORS.Black
  },
  currentBalancePriceCurrency: {
    fontFamily:FONTS.light,
    fontSize:Normalize(9),
    paddingHorizontal:7,
    color:COLORS.Black
  },
  historyTitle: {
    backgroundColor:COLORS.LightGrey,
    paddingVertical:17,
    alignItems:'center',
    justifyContent:'center',
    borderTopRightRadius: BORDER_RADIUS.items+10,
    borderTopLeftRadius: BORDER_RADIUS.items+10,
    marginTop:20,
    marginLeft:30,
    marginRight:30,
  },
  historyTitleText: {
    textAlign:'center',
    color: COLORS.DarkBlue,
    fontSize:Normalize(12),
    fontFamily:FONTS.bold
  }
})