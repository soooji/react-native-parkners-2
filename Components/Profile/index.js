import React, { Component } from 'react'
import { Text, View,StyleSheet,TouchableOpacity,Animated,Easing,ScrollView,TextInput,Image,Clipboard} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX,MODAL_STYLES,ICONS_SIZE,ALERT,BASE_IMG_URL,BASE_URL, ISEXPIRED, SETTOKEN, USER_ROLE, DIMENSIONS, Sooji, FIX_NUMS} from '../../Base/functions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import FullScreenLoading from './../Base/FullScreenLoading';
import Modal from "react-native-modal";
import axios from 'axios'
import Communications from 'react-native-communications';
import AutoHeightImage from 'react-native-auto-height-image';
export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisibleChargeModal: false,
      priceToCharge: '',
      balance: 0,
      transactions:[],

      useSmsNotification: false,
      usePushNotification: false,
      cancelReserveOnDelay: false,

      isLoading: false,
      isCopied: false,
      alertAnime: new Animated.Value(-90),
      alertText:'',
      loadingText: '',
      firstName: '',
      lastName:'',

      park_events_count: 0,
      park_events_second: 0,
      points: 0,
      recommend_code: '',

      recommend_prize : 'پارکنرز را به دوستان و آشنایانتان با استفاده از کد زیر معرفی کنید و امتیاز هدیه دریافت کنید '
    }
  }
  componentDidMount() {
    this.setState({loadingText:this.props.lang.Base.Loading.wait,alertText:this.props.lang.Base.Error.simple});
    // this.getUserInfo();
    this.getProfile();
  }
  getUserInfo = async ()=>{
    try {
      this.setState({
        firstName: await AsyncStorage.getItem('first_name'),
        lastName: await AsyncStorage.getItem('last_name'),
      })
    } catch (error) {
      console.log(error);
      this.setState({isLoading:false});
      this.props.setCurrentBase('auth');
    }
  }
  getProfile = async () => {
    try {
      this.setState({isLoading:true});
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'GET',
          url: `${BASE_URL}users/profile/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
      };
      axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
          this.setState({
            park_events_count: r.data.profile.park_events_count,
            park_events_second: r.data.profile.park_events_second,
            points: r.data.profile.points,
            recommend_code: r.data.profile.recommend_code,
            firstName: r.data.user.first_name,
            lastName: r.data.user.last_name,
            useSmsNotification: r.data.profile.use_sms_notification,
            usePushNotification: r.data.profile.use_push_notification,
            cancelReserveOnDelay: r.data.profile.cancel_reserve_on_delay,
          })
      })
      .then(r=>{
        this.setState({isLoading:false});
      })
      .catch(e=>{
          console.log(e);
          this.setState({isLoading:false});
          if(!e.response) {this.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status == 500) this.toggleAlert(true);
          else this.toggleAlert(true,e.response.data.error_message);
      })
    } catch (error) {
        console.log(error);
        this.setState({isLoading:false});
        this.props.setCurrentBase('auth');
    }
  }
  toggleActions = async (action) => {
    try {
      this.setState({isLoading:true});
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'POST',
          url: `${BASE_URL}users/profile/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
          data : {
            profile : {
              cancel_reserve_on_delay: action=="cancelOnDelay" ? !this.state.cancelReserveOnDelay : this.state.cancelReserveOnDelay,
              use_sms_notification: action=="sms" ? !this.state.useSmsNotification : this.state.useSmsNotification,
              use_push_notification: action=="notif" ? !this.state.usePushNotification : this.state.usePushNotification,
              user : {
                first_name: this.state.firstName,
                last_name: this.state.lastName,
              }
            }
          }
      };
      axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
          this.setState({
            useSmsNotification: r.data.profile.use_sms_notification,
            usePushNotification: r.data.profile.use_push_notification,
            cancelReserveOnDelay: r.data.profile.cancel_reserve_on_delay
          })
      })
      .then(r=>{
        this.setState({isLoading:false});
      })
      .catch(e=>{
          console.log(e);
          this.setState({isLoading:false});
          if(!e.response) {this.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status == 500) this.toggleAlert(true);
          else this.toggleAlert(true,e.response.data.error_message);
      })
    } catch (error) {
        console.log(error);
        this.setState({isLoading:false});
        this.props.setCurrentBase('auth');
    }
  }
  setCopeid() {
    this.setState({isCopied: true});
    setTimeout(()=>this.setState({isCopied: false}),3000);
  }
  secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? ":" : ":") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? ":" : ":") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? "" : "") : "";
    return hDisplay + mDisplay + sDisplay; 
  }
  render() {
    return (
      <View style={{width:'100%',height:'100%'}}>
        <FullScreenLoading isLoading={this.state.isLoading} text={this.state.loadingText}/>
        
        <Animated.View style={[FLEX.RNwSbCC,ALERT.alertBox,{
          transform: [{translateY:this.state.alertAnime}]
          ,height:50}]}>
            {/* <TouchableOpacity onPress={()=>this.toggleAlert(false)} style={ALERT.closeAlertBox}>
                <Icon name="times" color={'rgba(255,255,255,.6)'} size={Normalize(12)}/>
            </TouchableOpacity> */}
            <Text style={ALERT.alertBoxText}>{this.state.alertText}</Text>
        </Animated.View>
       
        <ScrollView style={{width:'100%',height:'100%'}} showsVerticalScrollIndicator={false} contentContainerStyle={{paddingHorizontal:20,paddingTop:20,paddingBottom:80}}>
          <View style={{width:'100%',height:'100%',marginTop:5}}>
            {/* <Image source={require('./../../Static/avatar.png')} style={styles.avatar}/>
            <Text style={styles.userTitle}>
            {this.state.firstName + " " + this.state.lastName}
            </Text> */}

            <View style={{width:'100%',height:'auto',marginTop:0}}>

              <View style={[styles.detailBox,FLEX.RNwFeCS]}>
                    {/* <View style={FLEX.RNwFsCS}> */}
                        {/* <AutoHeightImage source={require('./../../Static/orange-gem.png')} width={20} style={{marginRight:8}}/>
                        <Text style={styles.detailText}>
                        {this.state.points}
                        </Text> */}
                    {/* </View> */}
                    <Text style={[styles.detailTitle,{fontFamily:FONTS.black,fontSize:Normalize(13)}]}>
                        {this.state.firstName + " " + this.state.lastName}
                    </Text>
                    <Icon name="user" color={COLORS.DarkGrey} size={Normalize(ICONS_SIZE - 2)} style={{marginLeft:10}}/>
                </View>

                <View style={[styles.detailBox,FLEX.RNwSbCC]}>
                    <View style={FLEX.RNwFsCS}>
                        <AutoHeightImage source={require('./../../Static/orange-gem.png')} width={20} style={{marginRight:8}}/>
                        <Text style={styles.detailText}>
                        {this.state.points}
                        </Text>
                    </View>
                    <Text style={styles.detailTitle}>
                        امتیاز کل
                    </Text>
                </View>

                <View style={[styles.detailBox,FLEX.RNwSbCC]}>
                    <View style={FLEX.RNwFsCS}>
                        <Text style={styles.detailText}>
                          {this.state.park_events_count}
                        </Text>
                    </View>
                    <Text style={styles.detailTitle}>
                        تعداد کل پارک‌ها
                    </Text>
                </View>

                <View style={[styles.detailBox,FLEX.RNwSbCC]}>
                    <View style={FLEX.RNwFsCS}>
                        <Text style={styles.detailText}>
                          {this.state.park_events_second == 0 ? '0' : this.secondsToHms(this.state.park_events_second)}
                          </Text>
                    </View>
                    <Text style={styles.detailTitle}>
                        مجموع زمان
                    </Text>
                </View>

                <View style={styles.introducerBox}>
                    <View style={styles.introducerBoxDesc}>
                        <Text style={styles.introducerBoxTitle}>اعتبار هدیه</Text>
                        <Text style={styles.introducerBoxTitleDesc}>
                          {this.state.recommend_prize}
                        </Text>
                    </View>
                    <TouchableOpacity onPress={()=>{Clipboard.setString(`${this.state.recommend_code}`);this.setCopeid();}} style={[styles.codeBox,FLEX.RNwSbCC]}>
                        <Text style={styles.code}>
                          {this.state.recommend_code}
                        </Text>
                        <Text style={styles.codeText}>
                          کد اختصاصی شما
                          {this.state.isCopied ? ' کپی شد!' : ' '}
                        </Text>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity onPress={()=>this.toggleActions('notif')} style={[styles.detailBox,FLEX.RNwSbCC,{paddingVertical:11}]}>
                    <View style={[FLEX.RNwFsCS,{backgroundColor:this.state.usePushNotification ? COLORS.Green : COLORS.Red,padding:5,borderRadius:7}]}>
                        <Text style={[styles.detailText,{color:COLORS.White,fontSize:Normalize(12)}]}>
                          {this.state.usePushNotification ? 'فعال' : 'غیرفعال'}
                        </Text>
                    </View>
                    <Text style={[styles.detailTitle]}>
                      نوتیفیکیشن اطلاع رسانی
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.toggleActions('sms')} style={[styles.detailBox,FLEX.RNwSbCC,{paddingVertical:11}]}>
                    <View style={[FLEX.RNwFsCS,{backgroundColor:this.state.useSmsNotification ? COLORS.Green : COLORS.Red,padding:5,borderRadius:7}]}>
                      <Text style={[styles.detailText,{color:COLORS.White,fontSize:Normalize(12)}]}>
                        {this.state.useSmsNotification ? 'فعال' : 'غیرفعال'}
                      </Text>
                    </View>
                    <Text style={styles.detailTitle}>
                      پیامک اطلاع رسانی
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.toggleActions('cancelOnDelay')} style={[styles.detailBox,FLEX.RNwSbCC,{paddingVertical:11}]}>
                    <View style={[FLEX.RNwFsCS,{backgroundColor:this.state.cancelReserveOnDelay ? COLORS.Green : COLORS.Red,padding:5,borderRadius:7}]}>
                      <Text style={[styles.detailText,{color:COLORS.White,fontSize:Normalize(12)}]}>
                        {this.state.cancelReserveOnDelay ? 'فعال' : 'غیرفعال'}
                      </Text>
                    </View>
                    <Text style={styles.detailTitle}>
                      لغو خودکار در صورت تاخیر
                    </Text>
                </TouchableOpacity>


                <View style={[FLEX.RNwFeCS,{paddingRight:10}]}>
                  <Text style={styles.smsDescText}>
                    در صورت فعالسازی، هزینه پیامک از کیف پول شما کسر خواهد شد.
                  </Text>
                  <Sooji name="info" color={COLORS.DarkGrey} size={Normalize(ICONS_SIZE - 4)}/>
                </View>


            </View>

          </View>
        </ScrollView>
      </View>
    )
  }
  toggleAlert = (open,text = this.props.lang.Base.Error.simple) => {
    if(open) {
        this.setState({alertText: text})
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: 20,
          easing: Easing.elastic(1.2),
          duration: 1000,
          useNativeDriver:true
        },
      ).start();
      setTimeout(()=>{
        this.toggleAlert(false);
      },3000)
    } else {
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: -90,
          easing: Easing.ease,
          duration: 500,
          useNativeDriver:true
        },
      ).start();
      setTimeout(() => {
        this.setState({alertText: ''})
      }, 500);
    }
  }
}
const styles = StyleSheet.create({
  smsDescText : {
    color: COLORS.DarkBlue,
    fontSize:Normalize(9),
    textAlign:'right',
    fontFamily:FONTS.light,
    padding:10,
  },
  code : {
    color: 'white',
    fontSize:Normalize(15),
    textAlign:'left',
    fontFamily:FONTS.secondFontFamily
  },
  codeText: {
    color: 'white',
    fontSize:Normalize(11),
    textAlign:'right',
    fontFamily:FONTS.bold
  },
    codeBox :{
        backgroundColor: '#603AE3',
        borderBottomLeftRadius: BORDER_RADIUS.items - 3,
        borderBottomRightRadius: BORDER_RADIUS.items - 3,
        padding: 14
    },
    introducerBoxTitleDesc: {
        fontFamily:FONTS.light,
        color:COLORS.DarkBlue,
        fontSize:Normalize(11),
        textAlign:'center',
        paddingVertical:4,
        paddingHorizontal:12,
        lineHeight:25
    },
    introducerBoxTitle: {
        paddingTop:7,
        paddingBottom:7,
        fontFamily:FONTS.bold,
        color:COLORS.DarkBlue,
        fontSize:Normalize(12),
        textAlign:'center',
    },
    introducerBoxDesc : {
        paddingVertical:10
    },
    introducerBox:{
        width:'100%',
        borderWidth:1,
        borderColor:'#603AE3',
        borderRadius:BORDER_RADIUS.items,
        marginBottom:10,
    },
    detailText : {
        fontFamily:FONTS.extraBlack,
        color:COLORS.DarkBlue,
        fontSize:Normalize(14),
        textAlign:'left',
    },
    detailTitle: {
        fontFamily:FONTS.bold,
        color:COLORS.DarkBlue,
        fontSize:Normalize(10),
        textAlign:'right',
    },

    detailBox: {
        marginBottom:10,
        paddingVertical: 17,
        paddingHorizontal:15,
        borderWidth:1,
        borderColor:'#F1F1F1',
        borderRadius:BORDER_RADIUS.items
    },

    userTitle: {
        fontFamily:FONTS.secondFontFamily,
        color:COLORS.DarkBlue,
        fontSize:Normalize(15),
        textAlign:'center',
        paddingBottom:12
    },
    avatar: {
        width: DIMENSIONS.width/4,
        height: DIMENSIONS.width/4,
        borderRadius: DIMENSIONS.width/8,
        marginLeft:'auto',
        marginRight:'auto',
        marginTop:12,
        marginBottom:20,
    },


  chargeButtonText: {
    fontFamily:FONTS.bold,
    color:COLORS.White,
    fontSize:Normalize(13),
    textAlign:'center'
  },
  chargeButton: {
    marginTop:15,
    backgroundColor:COLORS.Green,
    paddingVertical:15,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:BORDER_RADIUS.items,
  },
  input: {
    backgroundColor:COLORS.White,
    borderRadius:BORDER_RADIUS.base,
    paddingVertical:17,
    textAlign:'center',
    width:'100%',
    color:COLORS.Black,
    fontFamily:FONTS.bold,
    fontSize:Normalize(13),
},
  currentBalanceBox: {
    backgroundColor:COLORS.LightGrey,
    borderRadius: BORDER_RADIUS.items,
    paddingVertical: 25,
    paddingHorizontal: 17
  },
  currentBalancePrice: {
    textAlign:'left',
    fontSize: Normalize(21)
  },
  currentBalanceTitle: {
    textAlign:'right',
    fontFamily: FONTS.bold,
    fontSize: Normalize(12),
    color:COLORS.Black
  },
  currentBalancePriceCurrency: {
    fontFamily:FONTS.light,
    fontSize:Normalize(9),
    paddingHorizontal:7,
    color:COLORS.Black
  },
  historyTitle: {
    backgroundColor:COLORS.LightGrey,
    paddingVertical:17,
    alignItems:'center',
    justifyContent:'center',
    borderTopRightRadius: BORDER_RADIUS.items+10,
    borderTopLeftRadius: BORDER_RADIUS.items+10,
    marginTop:20,
    marginLeft:30,
    marginRight:30,
  },
  historyTitleText: {
    textAlign:'center',
    color: COLORS.DarkBlue,
    fontSize:Normalize(12),
    fontFamily:FONTS.bold
  }
})