import React, { Component } from 'react'
import { Text, View,ScrollView,TouchableOpacity,StyleSheet,Image} from 'react-native'
import { DIMENSIONS, COLORS, BORDER_RADIUS, FONTS, Normalize , FLEX,Sooji} from '../../Base/functions';
import AutoHeightImage from 'react-native-auto-height-image';
export default class ParkInfo extends Component {
  render() {
    return (
    <ScrollView style={{width:'100%',height:'100%',backgroundColor:'white'}} contentContainerStyle={{padding:10}}>
        <Text style={{textAlign:'center',padding:10,fontSize:Normalize(22),fontWeight:'bold',fontFamily:FONTS.secondFontFamily}}>
        Image:
        </Text>
        <AutoHeightImage
        width={DIMENSIONS.width - 20}
        source={{uri:this.props.obj.img}}
        style={{borderRadius:BORDER_RADIUS.items+3}}
        />
        <Text style={{textAlign:'center',padding:10,paddingTop:25,fontSize:Normalize(22),fontWeight:'bold',fontFamily:FONTS.secondFontFamily}}>
        Information:
        </Text>
        <Text style={{fontSize:Normalize(13),paddingTop:10,paddingHorizontal:10,lineHeight:31,paddingBottom:20,textAlign:'left',color:COLORS.DarkBlue}}>
        {this.props.obj.info}
        </Text>
        <Text style={{textAlign:'center',padding:10,fontSize:Normalize(22),fontWeight:'bold',fontFamily:FONTS.secondFontFamily}}>
        Map:
        </Text>
        <AutoHeightImage
        width={DIMENSIONS.width - 20}
        source={{uri:this.props.obj.map}}
        style={{borderRadius:BORDER_RADIUS.items+3}}
        />
        <Text style={{textAlign:'center',padding:10,fontSize:Normalize(22),fontWeight:'bold',fontFamily:FONTS.secondFontFamily}}>
        History:
        </Text>
        <Text style={{fontSize:Normalize(13),paddingTop:0,paddingHorizontal:10,lineHeight:31,paddingBottom:20,textAlign:'left',color:COLORS.DarkBlue}}>
        {this.props.obj.history}
        </Text>
        <View style={{width:'100%',height:1,backgroundColor:'rgba(0,0,0,.3)'}}></View>
        <Text style={{fontStyle:'italic',opacity:.8,padding:20}}>
        {`The data are collected from WikiPedia website : \n ${this.props.obj.link}`}</Text>
    </ScrollView>
    )
  }
}
const styles = StyleSheet.create({
    parkItem: {
        width:'100%',
        height:'auto',
        padding: 15,
        backgroundColor : COLORS.Black,
        borderRadius: BORDER_RADIUS.items+4,
        marginBottom:10
    },
    title: {
        fontFamily:FONTS.secondFontFamily,
        fontSize: Normalize(16),
        color: COLORS.White,
        textAlign: 'center',
        width: '100%',
        paddingTop:23,
    },
})