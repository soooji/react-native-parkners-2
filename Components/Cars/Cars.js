import React, { Component } from 'react'
import { Text, View,StyleSheet,TouchableOpacity,Animated,Easing,ScrollView,TextInput,Image} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX,MODAL_STYLES,ICONS_SIZE,ALERT,BASE_IMG_URL,BASE_URL, ISEXPIRED, SETTOKEN, USER_ROLE, DIMENSIONS,Sooji, FIX_NUMS} from './../../Base/functions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Modal from "react-native-modal";
import FullScreenLoading from './../Base/FullScreenLoading';
import CarItem from './CarItem'
import axios from 'axios'
import AutoHeightImage from 'react-native-auto-height-image';

export default class componentName extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisibleDetailModal: false,
      isLoading: true,
      hasNoCar: false,
      alertAnime: new Animated.Value(-90),
      alertText:'',
      loadingText: '',

      cars:[],

      carIcons:[],

      selectedCarIconId: 0,
      carName: '',
      carPlakA: '',
      carPlakB: '',
      carPlakC: '',
      carPlakD: '',
    }
}

componentDidMount() {
  this.setState({loadingText:this.props.lang.Base.Loading.wait,alertText:this.props.lang.Base.Error.simple});
  this.getCarIcons();
  this.getCars()
}

getCarIcons = async () => {
  try {
      this.setState({isLoading:true});
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'GET',
          url: `${BASE_URL}car_icons/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
      };
      axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
          this.setState({carIcons:r.data.car_icons});
      })
      .then(r=>{
          this.setState({isLoading:false});
      })
      .catch(e=>{
          this.setState({isLoading:false});
          if(!e.response) {this.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status === 500) this.toggleAlert(true);
          else this.toggleAlert(true,e.response.data.error_message);
      })
  } catch (error) {
      this.setState({isLoading:false});
      this.props.setCurrentBase('auth');
  }
}
postCar = async () => {
  try {
      this.setState({isLoading:true,isVisibleNewCarModal:false});
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'POST',
          url: `${BASE_URL}cars/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
          data: {
            car: {
              given_name: this.state.carName,
              plate_a: FIX_NUMS(this.state.carPlakA),
              plate_b: this.state.carPlakB,
              plate_c: FIX_NUMS(this.state.carPlakC),
              plate_d: FIX_NUMS(this.state.carPlakD),
              car_icon_id: this.state.selectedCarIconId
            }
        },
      };
  axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
      })
      .then(r=>{
          this.setState({isLoading:false});
          this.bringEditsToBack();
      })
      .then(r=>{
        this.getCars();
      })
      .catch(e=>{
          console.log(e);
          this.setState({isLoading:false});
          if(!e.response) {this.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status == 500) this.toggleAlert(true);
          else this.toggleAlert(true,e.response.data.error_message);
      })
  } catch (error) {
      this.setState({isLoading:false});
      this.props.setCurrentBase('auth');
  }
}

getCars = async () => {
  try {
      this.setState({isLoading:true});
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'GET',
          url: `${BASE_URL}cars/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
      };
  axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
          this.setState({cars:r.data.cars});
          if(r.data.cars.length==0) {
            this.setState({hasNoCar: true})
          } else {
            this.setState({hasNoCar: false})
          }
      })
      .then(r=>{
          this.setState({isLoading:false});
      })
      .catch(e=>{
          this.setState({isLoading:false});
          if(!e.response) {this.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status === 500) this.toggleAlert(true);
          else this.toggleAlert(true,e.response.data.error_message);
      })
  } catch (error) {
      this.setState({isLoading:false});
      this.props.setCurrentBase('auth');
  }
}
setDefaultCar = async (carId) => {
  try {
      this.setState({isLoading:true});
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'GET',
          url: `${BASE_URL}cars/${carId}/set_default/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
      };
  axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
          this.setState({cars:r.data.cars});
      })
      .then(r=>{
          this.setState({isLoading:false});
      })
      .catch(e=>{
          this.setState({isLoading:false});
          if(!e.response) {this.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status === 500) this.toggleAlert(true);
          else this.toggleAlert(true,e.response.data.error_message);
      })
  } catch (error) {
      this.setState({isLoading:false});
      this.props.setCurrentBase('auth');
  }
}
bringEditsToBack() {
  this.setState({
    carName: this.props.carName,
    carPlakA: this.props.carPlakA,
    carPlakB: this.props.carPlakB,
    carPlakC: this.props.carPlakC,
    carPlakD: this.props.carPlakD,
  })
}
  render() {
    return (
      <View style={{width:'100%',height:'100%',position:'relative'}}>

        <FullScreenLoading isLoading={this.state.isLoading} text={this.state.loadingText}/>
        
        <Animated.View style={[FLEX.RNwSbCC,ALERT.alertBox,{transform: [{translateY:this.state.alertAnime}],height:50}]}>
            {/* <TouchableOpacity onPress={()=>this.toggleAlert(false)} style={ALERT.closeAlertBox}>
                <Icon name="times" color={'rgba(255,255,255,.6)'} size={Normalize(12)}/>
            </TouchableOpacity> */}
            <Text style={ALERT.alertBoxText}>{this.state.alertText}</Text>
        </Animated.View>

        <TouchableOpacity onPress={()=>this.setState({isVisibleNewCarModal:true})} style={[styles.newCarButton,{bottom:this.props.hasCurrentReserve() ? 80 : 20,}]}>
          <Icon name="plus" color={COLORS.White} size={Normalize(ICONS_SIZE - 1)}/>
        </TouchableOpacity>


        <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleNewCarModal}
            onBackdropPress={() => {this.setState({isVisibleNewCarModal: false});this.bringEditsToBack()}}
            onBackdropPress={() => {this.setState({isVisibleNewCarModal: false});this.bringEditsToBack()}}
            backdropOpacity={.4}
            style={MODAL_STYLES.center.outside}
        >
            <View style={[MODAL_STYLES.center.inside]}>

              <View style={[{width:'100%',marginBottom:15},this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
                <TouchableOpacity onPress={() => {this.setState({isVisibleNewCarModal: false});this.bringEditsToBack()}} activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={[MODAL_STYLES.center.titleBox, this.props.isRTL ? FLEX.RNwFeCS : FLEX.RrNwFeCS]}>
                  <Text style={[MODAL_STYLES.titleText]}>{this.props.lang.Cars.newCar}</Text>
                </View>
              </View>
              
              <View style={{width:'100%',height:'auto',paddingBottom:20}}>
              <TextInput
                  placeholderTextColor={COLORS.DarkGrey}
                  value={this.state.carName}
                  style={styles.input}
                  editable={!this.state.isLoading}
                  clearButtonMode={'while-editing'}
                  placeholder={this.props.lang.Cars.carName}
                  autoCorrect={false}
                  onChangeText={(text) => this.setState({carName: text})}
                  keyboardAppearance={"dark"}
              />
                <View style={[styles.itemsBox,FLEX.RNwSbCC]}>

                    <TextInput
                      placeholderTextColor={COLORS.DarkGrey}
                      value={this.state.carPlakA}
                      style={styles.plakInput}
                      editable={!this.state.isLoading}
                      placeholder="44"
                      autoCorrect={false}
                      onChangeText={(text) => this.setState({carPlakA: text})}
                      keyboardAppearance={"dark"}
                  />

                    <TextInput
                      placeholderTextColor={COLORS.DarkGrey}
                      value={this.state.carPlakB}
                      style={styles.plakInput}
                      editable={!this.state.isLoading}
                      placeholder="د"
                      autoCorrect={false}
                      onChangeText={(text) => this.setState({carPlakB: text})}
                      keyboardAppearance={"dark"}
                  />

                  <TextInput
                      placeholderTextColor={COLORS.DarkGrey}
                      value={this.state.carPlakC}
                      style={styles.plakInput}
                      editable={!this.state.isLoading}
                      placeholder="146"
                      autoCorrect={false}
                      onChangeText={(text) => this.setState({carPlakC: text})}
                      keyboardAppearance={"dark"}
                  />

                  <TextInput
                      placeholderTextColor={COLORS.DarkGrey}
                      value={this.state.carPlakD}
                      style={styles.plakInput}
                      editable={!this.state.isLoading}
                      placeholder="12"
                      autoCorrect={false}
                      onChangeText={(text) => this.setState({carPlakD: text})}
                      keyboardAppearance={"dark"}
                  />
                </View>
                <View style={[styles.itemsBox,FLEX.RNwSbCC]}>
                  {this.state.carIcons.map(
                      (v,k)=>
                          <TouchableOpacity onPress={()=>this.setState({selectedCarIconId:v.id})} key={k} style={[styles.carIconBox,{borderColor:this.state.selectedCarIconId==v.id ? COLORS.Green : COLORS.LightGrey}]}>
                                <Image source={{uri:`${BASE_IMG_URL}${v.icon.url}`}} style={{width:25,height:25}} resizeMode="contain"/>
                            </TouchableOpacity>
                  )}
                </View>
                <TouchableOpacity onPress={()=>this.postCar()} style={styles.chargeButton} activeOpacity={.85}>
                  <Text style={styles.chargeButtonText}>{this.props.lang.Cars.submitCar}</Text>
                </TouchableOpacity>
              </View>
            </View>
        </Modal>

        <ScrollView style={{width:'100%',height:'100%'}} contentContainerStyle={{paddingHorizontal:20,paddingTop:20,paddingBottom:125}}>
                  {this.state.hasNoCar ?
                  this.props.isRTL ?
                    <AutoHeightImage source={require('./../../Static/no-car.png')} width={DIMENSIONS.width / 2} style={{marginLeft:'auto',marginRight:'auto',marginTop:40}}/>
                    : <AutoHeightImage source={require('./../../Static/no-car-en.png')} width={DIMENSIONS.width / 2} style={{marginLeft:'auto',marginRight:'auto',marginTop:40}}/>
                    : null}
            {
              this.state.cars.map(
                (v,k)=>
                  <CarItem
                  lang={this.props.lang}
                  isRTL={this.props.isRTL}
                  setIsLoading={(bool)=>this.setState({isLoading:bool})}
                  onPress={()=>this.setDefaultCar(v.id)}
                  getCars={()=>this.getCars()}
                  toggleAlert={(open,text)=>this.toggleAlert(open,text)}
                  carIcons={this.state.carIcons}
                  key={k} carId={v.id}
                  toggleAlert={(open,text=this.props.lang.Base.Error.simple)=>this.toggleAlert(open,text)}
                  carPlakA={v.plate_a} carPlakB={v.plate_b} carPlakC={v.plate_c} carPlakD={v.plate_d}
                  isDefault={v.is_default} carName={v.given_name}
                  carIconUrl={v.icon.icon.url}
                  selectedCarIconId={v.icon.id}/>
              ) 
            }

        </ScrollView> 
      </View>
    )
  }
  toggleAlert = (open,text = this.props.lang.Base.Error.simple) => {
    if(open) {
        this.setState({alertText: text})
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: 20,
          easing: Easing.elastic(1.2),
          duration: 1000,
          useNativeDriver:true
        },
      ).start();
      setTimeout(()=>{
        this.toggleAlert(false);
      },3000)
    } else {
      setTimeout(() => {
        this.setState({alertText: ''})
      }, 500);
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: -90,
          easing: Easing.ease,
          duration: 500,
          useNativeDriver:true
        },
      ).start();
    }
  }
}
const styles = StyleSheet.create({
  newCarButton: {
    justifyContent:'center',
    alignItems:'center',
    position:'absolute',
    right:20,
    paddingVertical:17,
      paddingHorizontal:18,
    zIndex:111111111111111111,
    backgroundColor:COLORS.Green,
    shadowColor: 'rgba(62,209,53,0.57)',
    shadowOffset: {
      width:0,
      height:8
    },
    shadowOpacity: 1,
    shadowRadius: 17,
    borderRadius:BORDER_RADIUS.items + 10,
  },
  editButton: {
    backgroundColor:COLORS.White,
    borderRadius:BORDER_RADIUS.base,
    padding:10,
    justifyContent:'center',
    alignItems:'center'
  },
  chargeButtonText: {
    fontFamily:FONTS.bold,
    color:COLORS.White,
    fontSize:Normalize(13),
    textAlign:'center'
  },
  chargeButton: {
    marginTop:15,
    backgroundColor:COLORS.Green,
    paddingVertical:13,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:BORDER_RADIUS.items,
  },
  carIconBox: {
    width:'32%',
    paddingVertical:8,
    borderRadius:BORDER_RADIUS.items,
    backgroundColor:COLORS.White,
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
  },
  plakInput: {
    backgroundColor:COLORS.LightGrey,
    borderRadius:BORDER_RADIUS.base,
    paddingVertical:10,
    textAlign:'center',
    width:'24%',
    color:COLORS.Black,
    fontFamily:FONTS.medium,
    fontSize:Normalize(12),
  },
  itemsBox: {
    backgroundColor:COLORS.White,
    marginTop:10,
    padding:7,
    borderRadius:BORDER_RADIUS.base,
    width:'100%',
    height:'auto',
  },
  input: {
    backgroundColor:COLORS.White,
    borderRadius:BORDER_RADIUS.items,
    paddingVertical:15,
    textAlign:'center',
    width:'100%',
    color:COLORS.Black,
    fontFamily:FONTS.medium,
    fontSize:Normalize(13),
},
  carTitle: {
    fontFamily:FONTS.bold,
    fontSize: Normalize(12),
    textAlign:'right',
    color: COLORS.DarkBlue,
    paddingRight:14
  },
  carImage: {
    width: 30,
    height: 30
  },
    container: {
        backgroundColor:COLORS.LightGrey,
        borderRadius: BORDER_RADIUS.items,
        paddingVertical:25,
        paddingHorizontal:18,
        borderWidth:1,
        marginBottom:15
    }  
})