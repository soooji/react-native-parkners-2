import React, { Component } from 'react'
import { Text, View,StyleSheet,TouchableOpacity,Animated,Easing,TextInput,Image} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX,MODAL_STYLES,ICONS_SIZE,BASE_IMG_URL,BASE_URL,ISEXPIRED,USER_ROLE,SETTOKEN, Sooji } from './../../Base/functions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Modal from "react-native-modal";
import FullScreenLoading from './../Base/FullScreenLoading';
import axios from 'axios'
export default class componentName extends Component {
    constructor(props) {
        super(props);
        this.state = {
          isVisibleDetailModal: false,
          isLoading: false,
          loadingText:'',
          carName: '',
          carPlakA: '',
          carPlakB: '',
          carPlakC: '',
          carPlakD: '',

          selectedCarIconId: 0
        }
    }
    componentDidMount() {
      this.setState({loadingText:this.props.lang.Base.Loading.wait});
      this.setState({
        carName: this.props.carName,
        carPlakA: this.props.carPlakA,
        carPlakB: this.props.carPlakB,
        carPlakC: this.props.carPlakC,
        carPlakD: this.props.carPlakD,
        selectedCarIconId: this.props.selectedCarIconId
      })
    }
    bringEditsToBack() {
      this.setState({
        carName: this.props.carName,
        carPlakA: this.props.carPlakA,
        carPlakB: this.props.carPlakB,
        carPlakC: this.props.carPlakC,
        carPlakD: this.props.carPlakD,
        selectedCarIconId: 0,
      })
    }
  putCar = async () => {
      try {
          this.setState({isLoading:true});
          this.setState({isVisibleDetailModal: false});
          const token = await AsyncStorage.getItem('token');
          if(!token) {this.props.setCurrentBase("auth");return;}
          const options = {
              method: 'PUT',
              url: `${BASE_URL}cars/${this.props.carId}/`,
              headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
              data: {
                car: {
                  given_name: this.state.carName,
                  car_icon_id: this.state.selectedCarIconId
                }
            },
          };
      axios(options)
          .then(r=>{
              SETTOKEN(r.data.token);
              this.props.setIsLoading(false);
              this.setState({isLoading:false});
          })
          .then(r=>{
            this.props.getCars();
          })
          .catch(e=>{
              console.log(e);
              this.setState({isLoading:false});
              if(!e.response) {this.toggleAlert(true);return;}
              if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
              if(e.response.status == 500) this.toggleAlert(true);
              else this.toggleAlert(true,e.response.data.error_message);
          })
      } catch (error) {
        console.log(error);
          this.setState({isLoading:false});
          this.props.setCurrentBase('auth');
      }
    }
deleteCar = async () => {
  try {
      this.props.setIsLoading(true);
      this.setState({isVisibleDetailModal: false});
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'DELETE',
          url: `${BASE_URL}cars/${this.props.carId}/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE}
      };
  axios(options)
      .then(r=>{
          this.props.setIsLoading(false);
          this.setState({isLoading:false});
          SETTOKEN(r.data.token);
      })
      .then(r=>{
        this.props.getCars();
      })
      .catch(e=>{
          console.log(e);
          this.setState({isLoading:false});
          this.props.setIsLoading(false);
          if(!e.response) {this.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status == 500) this.toggleAlert(true);
          else this.toggleAlert(true,e.response.data.error_message);
      })
  } catch (error) {
    console.log(error);
      this.setState({isLoading:false});
      this.props.setIsLoading(false);
      this.props.setCurrentBase('auth');
  }
}

  render() {
    return (
      <View>
        <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleDetailModal}
            onBackdropPress={() => {this.setState({isVisibleDetailModal: false});this.bringEditsToBack()}}
            onBackdropPress={() => {this.setState({isVisibleDetailModal: false});this.bringEditsToBack()}}
            backdropOpacity={.4}
            style={MODAL_STYLES.center.outside}
        >
            <View style={[MODAL_STYLES.center.inside]}>

              <View style={[{width:'100%',marginBottom:15},this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
                <TouchableOpacity onPress={() => {this.setState({isVisibleDetailModal: false});this.bringEditsToBack()}} activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={[MODAL_STYLES.center.titleBox, this.props.isRTL ? FLEX.RNwFeCS : FLEX.RrNwFeCS]}>
                  <Text style={MODAL_STYLES.titleText}>{this.props.lang.Cars.editCar}</Text>
                </View>
              </View>
              
              <View style={{width:'100%',height:'auto',paddingBottom:20}}>
              <TextInput
                  placeholderTextColor={COLORS.DarkGrey}
                  value={this.state.carName}
                  style={styles.input}
                  editable={!this.state.isLoading}
                  clearButtonMode={'while-editing'}
                  placeholder={this.props.lang.Cars.carName}
                  autoCorrect={false}
                  onChangeText={(text) => this.setState({carName: text})}
                  keyboardAppearance={"dark"}
              />
                <View style={[styles.itemsBox,FLEX.RNwSbCC]}>

                    <TextInput
                      placeholderTextColor={COLORS.DarkGrey}
                      value={this.state.carPlakA}
                      style={styles.plakInput}
                      editable={!this.state.isLoading}
                      placeholder="44"
                      autoCorrect={false}
                      onChangeText={(text) => this.setState({carPlakA: text})}
                      keyboardAppearance={"dark"}
                      editable={false}
                  />

                    <TextInput
                      placeholderTextColor={COLORS.DarkGrey}
                      value={this.state.carPlakB}
                      style={styles.plakInput}
                      editable={!this.state.isLoading}
                      placeholder="د"
                      autoCorrect={false}
                      onChangeText={(text) => this.setState({carPlakB: text})}
                      keyboardAppearance={"dark"}
                      editable={false}
                  />

                  <TextInput
                      placeholderTextColor={COLORS.DarkGrey}
                      value={this.state.carPlakC}
                      style={styles.plakInput}
                      editable={!this.state.isLoading}
                      placeholder="146"
                      autoCorrect={false}
                      onChangeText={(text) => this.setState({carPlakC: text})}
                      keyboardAppearance={"dark"}
                      editable={false}
                  />

                  <TextInput
                      placeholderTextColor={COLORS.DarkGrey}
                      value={this.state.carPlakD}
                      style={styles.plakInput}
                      editable={!this.state.isLoading}
                      placeholder="12"
                      autoCorrect={false}
                      onChangeText={(text) => this.setState({carPlakD: text})}
                      keyboardAppearance={"dark"}
                      editable={false}
                  />
                </View>
                <View style={[styles.itemsBox,FLEX.RNwSbCC]}>
              {
                this.props.carIcons.map(
                  (v,k)=>
                  <TouchableOpacity onPress={()=>this.setState({selectedCarIconId:v.id})} key={k} style={[styles.carIconBox,{borderColor:this.state.selectedCarIconId==v.id ? COLORS.Green : COLORS.LightGrey}]}>
                    <Image source={{uri:`${BASE_IMG_URL}${v.icon.url}`}} style={{width:25,height:25}} resizeMode="contain"/>
                  </TouchableOpacity>
                )
              }   
                </View>
                <TouchableOpacity onPress={()=>this.putCar()} style={styles.chargeButton} activeOpacity={.85}>
                  <Text style={styles.chargeButtonText}>{this.props.lang.Cars.submitChanges}</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>this.deleteCar()} style={styles.deleteButton} activeOpacity={.85}>
                  <Text style={styles.deleteButtonText}>{this.props.lang.Cars.delete}</Text>
                </TouchableOpacity>
              </View>
            </View>
        </Modal>

      <TouchableOpacity onPress={()=>this.props.onPress()} style={[styles.container,{borderColor: this.props.isDefault ? COLORS.LightBlue : COLORS.LightGrey}]}>

        <View style={[this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
          <TouchableOpacity onPress={()=>this.setState({isVisibleDetailModal:true})} style={styles.editButton}>
            <Icon name="pen" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
          </TouchableOpacity>
          <View style={[this.props.isRTL ? FLEX.RNwFeCS : FLEX.RrNwFeCS ]}>
            <Text style={styles.carTitle}>
              {this.props.carName}
            </Text>
            
            <Image source={{uri:`${BASE_IMG_URL}${this.props.carIconUrl}`}} style={styles.carImage} resizeMode={"contain"}/>

          </View>

        </View>

      </TouchableOpacity>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  editButton: {
    backgroundColor:COLORS.White,
    borderRadius:BORDER_RADIUS.base,
    padding:10,
    justifyContent:'center',
    alignItems:'center'
  },
  chargeButtonText: {
    fontFamily:FONTS.bold,
    color:COLORS.White,
    fontSize:Normalize(13),
    textAlign:'center'
  },
  chargeButton: {
    marginTop:15,
    backgroundColor:COLORS.Green,
    paddingVertical:13,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:BORDER_RADIUS.items,
  },
  deleteButtonText: {
    fontFamily:FONTS.bold,
    color:COLORS.White,
    fontSize:Normalize(11),
    textAlign:'center'
  },
  deleteButton: {
    marginTop:12,
    backgroundColor:COLORS.Red,
    paddingVertical:10,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:BORDER_RADIUS.items,
  },
  carIconBox: {
    width:'32%',
    paddingVertical:8,
    borderRadius:BORDER_RADIUS.items,
    backgroundColor:COLORS.White,
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
  },
  plakInput: {
    backgroundColor:COLORS.LightGrey,
    borderRadius:BORDER_RADIUS.base,
    paddingVertical:10,
    textAlign:'center',
    width:'24%',
    color:COLORS.Black,
    fontFamily:FONTS.medium,
    fontSize:Normalize(12),
  },
  itemsBox: {
    backgroundColor:COLORS.White,
    marginTop:10,
    padding:7,
    borderRadius:BORDER_RADIUS.base,
    width:'100%',
    height:'auto',
  },
  input: {
    backgroundColor:COLORS.White,
    borderRadius:BORDER_RADIUS.items,
    paddingVertical:15,
    textAlign:'center',
    width:'100%',
    color:COLORS.Black,
    fontFamily:FONTS.medium,
    fontSize:Normalize(13),
},
  carTitle: {
    fontFamily:FONTS.bold,
    fontSize: Normalize(12),
    textAlign:'right',
    color: COLORS.DarkBlue,
    paddingRight:14,
    paddingLeft:10
  },
  carImage: {
    width: 30,
    height: 30,
  },
    container: {
        backgroundColor:COLORS.LightGrey,
        borderRadius: BORDER_RADIUS.items,
        paddingVertical:25,
        paddingHorizontal:18,
        borderWidth:1,
        marginBottom:15
    }  
})