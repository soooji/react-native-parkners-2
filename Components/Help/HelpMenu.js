import React, { Component } from 'react'
import { Text, View,StyleSheet,Animated,Easing,WebView} from 'react-native'
import { COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX,MODAL_STYLES,ICONS_SIZE,ALERT,BASE_IMG_URL,BASE_URL, ISEXPIRED, SETTOKEN, USER_ROLE, DIMENSIONS, Sooji, FIX_NUMS} from '../../Base/functions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import FullScreenLoading from './../Base/FullScreenLoading';
import Modal from "react-native-modal";
import axios from 'axios'
import Communications from 'react-native-communications';
import AutoHeightImage from 'react-native-auto-height-image';
export default class HelpMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      alertText: '',
      loadingText: '',
      alertAnime: new Animated.Value(-90),
    }
  }
  componentDidMount() {
    this.setState({loadingText:this.props.lang.Base.Loading.wait,alertText:this.props.lang.Base.Error.simple});
  }

  
  setCopeid() {
    this.setState({isCopied: true});
    setTimeout(()=>this.setState({isCopied: false}),3000);
  }
  secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? ":" : ":") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? ":" : ":") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? "" : "") : "";
    return hDisplay + mDisplay + sDisplay; 
}
  render() {
    return (
      <View style={{width:'100%',height:'100%'}}>
        <FullScreenLoading isLoading={this.state.isLoading} text={this.state.loadingText}/>
        
        <Animated.View style={[FLEX.RNwSbCC,ALERT.alertBox,{transform: [{translateY:this.state.alertAnime}],height:50}]}>
            <Text style={ALERT.alertBoxText}>{this.state.alertText}</Text>
        </Animated.View>

        <WebView
          source={{uri: `${BASE_URL}help`}}
          style={{width: '100%',height:'100%'}}
        />
      </View>
    )
  }
  toggleAlert = (open,text = this.props.lang.Base.Error.simple) => {
    if(open) {
        this.setState({alertText: text})
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: 20,
          easing: Easing.elastic(1.2),
          duration: 1000,
          useNativeDriver:true
        },
      ).start();
      setTimeout(()=>{
        this.toggleAlert(false);
      },3000)
    } else {
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: -90,
          easing: Easing.ease,
          duration: 500,
          useNativeDriver:true
        },
      ).start();
      setTimeout(() => {
        this.setState({alertText: ''})
      }, 500);
    }
  }
}
const styles = StyleSheet.create({
  smsDescText : {
    color: COLORS.DarkBlue,
    fontSize:Normalize(9),
    textAlign:'right',
    fontFamily:FONTS.light,
    padding:10,
  },
  code : {
    color: 'white',
    fontSize:Normalize(15),
    textAlign:'left',
    fontFamily:FONTS.secondFontFamily
  },
  codeText: {
    color: 'white',
    fontSize:Normalize(11),
    textAlign:'right',
    fontFamily:FONTS.bold
  },
    codeBox :{
        backgroundColor: '#603AE3',
        borderBottomLeftRadius: BORDER_RADIUS.items - 3,
        borderBottomRightRadius: BORDER_RADIUS.items - 3,
        padding: 14
    },
    introducerBoxTitleDesc: {
        fontFamily:FONTS.light,
        color:COLORS.DarkBlue,
        fontSize:Normalize(11),
        textAlign:'center',
        paddingVertical:4,
        paddingHorizontal:12,
        lineHeight:30
    },
    introducerBoxTitle: {
        paddingTop:7,
        paddingBottom:7,
        fontFamily:FONTS.bold,
        color:COLORS.DarkBlue,
        fontSize:Normalize(12),
        textAlign:'center',
    },
    introducerBoxDesc : {
        paddingVertical:10
    },
    introducerBox:{
        width:'100%',
        borderWidth:1,
        borderColor:'#603AE3',
        borderRadius:BORDER_RADIUS.items,
        marginBottom:10,
    },
    detailText : {
        fontFamily:FONTS.extraBlack,
        color:COLORS.DarkBlue,
        fontSize:Normalize(14),
        textAlign:'left',
    },
    detailTitle: {
        fontFamily:FONTS.bold,
        color:COLORS.DarkBlue,
        fontSize:Normalize(10),
        textAlign:'right',
    },

    detailBox: {
        marginBottom:10,
        paddingVertical: 17,
        paddingHorizontal:15,
        borderWidth:1,
        borderColor:'#F1F1F1',
        borderRadius:BORDER_RADIUS.items
    },

    userTitle: {
        fontFamily:FONTS.secondFontFamily,
        color:COLORS.DarkBlue,
        fontSize:Normalize(15),
        textAlign:'center',
        paddingBottom:12
    },
    avatar: {
        width: DIMENSIONS.width/4,
        height: DIMENSIONS.width/4,
        borderRadius: DIMENSIONS.width/8,
        marginLeft:'auto',
        marginRight:'auto',
        marginTop:12,
        marginBottom:20,
    },


  chargeButtonText: {
    fontFamily:FONTS.bold,
    color:COLORS.White,
    fontSize:Normalize(13),
    textAlign:'center'
  },
  chargeButton: {
    marginTop:15,
    backgroundColor:COLORS.Green,
    paddingVertical:15,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:BORDER_RADIUS.items,
  },
  input: {
    backgroundColor:COLORS.White,
    borderRadius:BORDER_RADIUS.base,
    paddingVertical:17,
    textAlign:'center',
    width:'100%',
    color:COLORS.Black,
    fontFamily:FONTS.bold,
    fontSize:Normalize(13),
},
  currentBalanceBox: {
    backgroundColor:COLORS.LightGrey,
    borderRadius: BORDER_RADIUS.items,
    paddingVertical: 25,
    paddingHorizontal: 17
  },
  currentBalancePrice: {
    textAlign:'left',
    fontSize: Normalize(21)
  },
  currentBalanceTitle: {
    textAlign:'right',
    fontFamily: FONTS.bold,
    fontSize: Normalize(12),
    color:COLORS.Black
  },
  currentBalancePriceCurrency: {
    fontFamily:FONTS.light,
    fontSize:Normalize(9),
    paddingHorizontal:7,
    color:COLORS.Black
  },
  historyTitle: {
    backgroundColor:COLORS.LightGrey,
    paddingVertical:17,
    alignItems:'center',
    justifyContent:'center',
    borderTopRightRadius: BORDER_RADIUS.items+10,
    borderTopLeftRadius: BORDER_RADIUS.items+10,
    marginTop:20,
    marginLeft:30,
    marginRight:30,
  },
  historyTitleText: {
    textAlign:'center',
    color: COLORS.DarkBlue,
    fontSize:Normalize(12),
    fontFamily:FONTS.bold
  }
})