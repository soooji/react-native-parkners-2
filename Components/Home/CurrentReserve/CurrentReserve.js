import React, { Component } from 'react'
import FullScreenLoading from '../../Base/FullScreenLoading';
import Modal from "react-native-modal";
import { Text, View,StyleSheet,TouchableOpacity,Animated,Easing,ScrollView,Image,Platform,Alert} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX,MODAL_STYLES,ICONS_SIZE,ALERT,BASE_IMG_URL,BASE_URL, ISEXPIRED, SETTOKEN, USER_ROLE, DIMENSIONS, Sooji, SUPPORT_NUMBER} from '../../../Base/functions';
import axios from 'axios'
import Icon from 'react-native-vector-icons/FontAwesome5';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Communications from 'react-native-communications';
import DateTimePicker from 'react-native-modal-datetime-picker'
export default class CurrentReserve extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isCountingDown: false,
            alertAnime: new Animated.Value(-90),
            isVisibleQuitConfirmModal:false,
            isVisibleResultModal: false,
            isTimePickerVisible: false,
            isVisibleRenewModal: false,
            fill: 10,
            alertText:'',
            loadingText: '',
            startTime: new Date(),
            endTime: new Date(),
            countDownString: '...',
            currentReserve: null,
            transaction: null,
            exitEvent: null,
            primeEndTimeHour : 0, //for renew
            primeEndTimeMinute : 0,
        }
    }
    //TODO: ورود زودهنگام و خروج دیر هنگام؟
    componentDidMount() {
        this.getCurrentReserve();
        this.setState({loadingText:this.props.lang.Base.Loading.wait,alertText:this.props.lang.Base.Error.simple,currentReserveId:this.props.currentReserveId});
    }
    componentWillUnmount() {
        clearInterval(this._interval);
    }
    countItDown() {
        this._interval = setInterval(() => {
            if(!this.state.isCountingDown) {
                clearInterval(this._interval);
                return
            }
            let now = new Date;
            if (now > this.state.endTime) {
                var remain = ((now - this.state.endTime) / 1000);
                var hh = this.pad((remain / 60 / 60) % 60);
                var mm = this.pad((remain / 60) % 60);
                var ss = this.pad(remain % 60);
                this.setState({countDownString: hh + ":" + mm + ":" + ss})
            } else {
                var remain = ((this.state.endTime - now) / 1000);
                var hh = this.pad((remain / 60 / 60) % 60);
                var mm = this.pad((remain / 60) % 60);
                var ss = this.pad(remain % 60);
                this.setState({countDownString: hh + ":" + mm + ":" + ss})
            }
        }, 1000);
    }
    pad(num) {
        return ("0" + parseInt(num < 0 ? -1 * num : num)).substr(-2);
    }
    renewReserve = async () => {
        try {
          this.setState({isLoading:true});
          const token = await AsyncStorage.getItem('token');
          if(!token) {this.props.setCurrentBase("auth");return;}
          this.setState({isVisibleRenewModal:false});
          const options = {
              method: 'PUT',
              url: `${BASE_URL}park_events/${this.props.currentReserveId}/`,
              headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
              data :{
                park_event: {
                    car_id: this.state.currentReserve.car_id,
                    start_time: {
                        hour:this.state.currentReserve.start_time.hour,
                        minute:this.state.currentReserve.start_time.minute
                    },
                    end_time: {
                        hour:parseInt(this.state.primeEndTimeHour),
                        minute:parseInt(this.state.primeEndTimeMinute)
                    },
                },
              }
          };
          axios(options)
          .then(r=>{
                SETTOKEN(r.data.token);
                this.getCurrentReserve();
          }) 
          .catch(e=>{
              console.log(e);
              this.setState({isVisibleRenewModal: false,primeEndTimeHour:this.state.currentReserve.end_time.hour,primeEndTimeMinute:this.state.currentReserve.end_time.minute});
              this.setState({isLoading:false});
              if(!e.response) {this.toggleAlert(true);return;}
              if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
              if(e.response.status == 500) this.toggleAlert(true);
              else this.toggleAlert(true,e.response.data.error_message);
          })
        } catch (error) {
            this.setState({isVisibleRenewModal: false,primeEndTimeHour:this.state.currentReserve.end_time.hour,primeEndTimeMinute:this.state.currentReserve.end_time.minute});
            console.log(error);
            this.setState({isLoading:false});
            this.props.setCurrentBase('auth');
        }
      }
    getCurrentReserve = async () => {
        try {
          this.setState({isLoading:true});
          const token = await AsyncStorage.getItem('token');
          if(!token) {this.props.setCurrentBase("auth");return;}
          const options = {
              method: 'GET',
              url: `${BASE_URL}park_events/${this.props.currentReserveId}/`,
              headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
          };
          axios(options)
          .then(r=>{
                SETTOKEN(r.data.token);
                this.setState({
                    currentReserve:r.data.park_event,
                    primeEndTimeHour:r.data.park_event.end_time.hour,
                    primeEndTimeMinute:r.data.park_event.end_time.minute,
                });
                let endTime = new Date();
                let startTime = new Date();
                startTime.setHours(r.data.park_event.actual_start_time.hour,r.data.park_event.actual_start_time.minute,r.data.park_event.actual_start_time.second);
                endTime.setHours(r.data.park_event.end_time.hour,r.data.park_event.end_time.minute,r.data.park_event.end_time.second);
                this.setState({endTime: endTime,startTime: startTime});
          })
          .then(r=>{
            this.setState({isCountingDown: true});
            this.countItDown();
          })
          .then(r=>{
            this.setState({isLoading:false});
          })
          .catch(e=>{
              console.log(e);
              this.setState({isLoading:false});
              if(!e.response) {this.toggleAlert(true);return;}
              if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
              if(e.response.status == 500) this.toggleAlert(true);
              else this.toggleAlert(true,e.response.data.error_message);
          })
        } catch (error) {
            console.log(error);
            this.setState({isLoading:false});
            this.props.setCurrentBase('auth');
        }
      }
    toggleAlert = (open,text = this.props.lang.Base.Error.simple) => {
        if(open) {
            this.setState({alertText: text})
          Animated.timing(
            this.state.alertAnime,
            {
              toValue: 20,
              easing: Easing.elastic(1.2),
              duration: 1000,
              useNativeDriver:true
            },
          ).start();
          setTimeout(()=>{
            this.toggleAlert(false);
          },3000)
        } else {
            setTimeout(() => {
                this.setState({alertText: ''})
              }, 500);
          Animated.timing(
            this.state.alertAnime,
            {
              toValue: -90,
              easing: Easing.ease,
              duration: 500,
              useNativeDriver:true
            },
          ).start();
        }
      }
      quit = async () => {
        if(this.props.currentReserveId==0) return;
        try {
          this.setState({isLoading:true,isVisibleQuitConfirmModal:false});
          const token = await AsyncStorage.getItem('token');
          if(!token) {this.props.setCurrentBase("auth");return;}
          const options = {
              method: 'GET',
              url: `${BASE_URL}park_events/${this.props.currentReserveId}/exit/`,
              headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
          };
          axios(options)
          .then(r=>{
              SETTOKEN(r.data.token);
              this.setState({exitEvent: r.data.park_event})
          })
          .then(r=>{
            this.setState({isCountingDown:false});
            // this.props.setCurrentReserve(false,0);
          })
          .then(r=>{
              this.getTransactionInfo();
          })
          .catch(e=>{
              console.log(e);
              this.setState({isLoading:false});
              if(!e.response) {this.toggleAlert(true);return;}
              if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
              if(e.response.status == 500) this.toggleAlert(true);
              else this.toggleAlert(true,e.response.data.error_message);
          })
        } catch (error) {
            console.log(error);
            this.setState({isLoading:false});
            this.props.setCurrentBase('auth');
        }
      }
      confirmTimePicker(time) {
        let t = time.toTimeString();
        this.setState({primeEndTimeHour:t.slice(0,2),primeEndTimeMinute:t.slice(3,5),isTimePickerVisible:false});
        setTimeout(()=>this.setState({isVisibleRenewModal:true}),700);
      }
      getTransactionInfo = async () => {
        try {
          this.setState({isLoading:true,isVisibleQuitConfirmModal:false});
          const token = await AsyncStorage.getItem('token');
          if(!token) {this.props.setCurrentBase("auth");return;}
          const options = {
              method: 'GET',
              url: `${BASE_URL}park_events/${this.state.currentReserveId}/transactions/`,
              headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
          };
          axios(options)
          .then(r=>{
              SETTOKEN(r.data.token);
              this.setState({transaction: r.data});
          })
          .then(r=>{
              setTimeout(()=>{
                this.setState({isVisibleResultModal:true,isLoading:false})
              },500);
            //   this.props.setCurrentBase("dashboard");
          })
          .catch(e=>{
              console.log(e);
              this.setState({isLoading:false});
              if(!e.response) {this.toggleAlert(true);return;}
              if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
              if(e.response.status == 500) this.toggleAlert(true);
              else this.toggleAlert(true,e.response.data.error_message);
          })
        } catch (error) {
            console.log(error);
            this.setState({isLoading:false});
            this.props.setCurrentBase('auth');
        }
      }
  render() {
    return (
    <View style={{position:'relative',width:'100%',height:Platform.OS == "ios" ? isIphoneX() ? DIMENSIONS.height - 165 : DIMENSIONS.height - 125 : DIMENSIONS.height - 145}}>
        <FullScreenLoading isLoading={this.state.isLoading} text={this.state.loadingText}/>
        <DateTimePicker
          date={this.state.endTime}
          is24Hour={true}
          titleIOS={`زمان خروج را انتخاب کنید`}
          titleStyle={{fontFamily:FONTS.bold,fontSize:Normalize(13),paddingBottom:10,paddingTop:10,color:COLORS.DarkBlue}}
          confirmTextIOS="تایید"
          confirmTextStyle={{fontFamily:FONTS.bold,fontSize:Normalize(16),paddingBottom:10,paddingTop:10}}
          cancelTextIOS="لغو"
          cancelTextStyle={{fontFamily:FONTS.light,fontSize:Normalize(15),paddingBottom:10,paddingTop:10}}
          isVisible={this.state.isTimePickerVisible}
          onConfirm={(time)=>{this.confirmTimePicker(time);}}
          mode={'time'}
          onCancel={()=>{
            this.setState({isTimePickerVisible:false});
            setTimeout(()=>this.setState({isVisibleRenewModal:true}),700)
          }}
        />
        <Animated.View style={[FLEX.RNwSbCC,ALERT.alertBox,{transform: [{translateY:this.state.alertAnime}],height:50}]}>
            {/* <TouchableOpacity onPress={()=>this.toggleAlert(false)} style={ALERT.closeAlertBox}>
                <Icon name="times" color={'rgba(255,255,255,.6)'} size={Normalize(12)}/>
            </TouchableOpacity> */}
            <Text style={ALERT.alertBoxText}>{this.state.alertText}</Text>
        </Animated.View>
        {/* quit confirm modal */}
        <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleQuitConfirmModal}
            onBackdropPress={() => {this.setState({isVisibleQuitConfirmModal: false});}}
            backdropOpacity={.4}
            style={MODAL_STYLES.bottom.outside}
        >
            <View style={[MODAL_STYLES.bottom.inside]}>

              <View style={[{width:'100%',marginBottom:10},FLEX.RNwSbCC]}>
                <TouchableOpacity onPress={() => {this.setState({isVisibleQuitConfirmModal: false});}} activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={[MODAL_STYLES.titleBox,FLEX.RNwFeCS]}>
                  <Text style={MODAL_STYLES.titleText}>آیا از پارکینگ خارج شده اید؟</Text>
                </View>
              </View>

              <View style={{marginBottom:10}}>
                <TouchableOpacity onPress={()=>{this.quit();}} activeOpacity={.85} style={[styles.confirmButton,{backgroundColor:COLORS.Red},FLEX.RNwSbCC]}>
                  <Sooji name="quit" color={COLORS.White} size={Normalize(ICONS_SIZE + 2)}/>
                  <Text style={styles.confirmButtonText}>بله، خارج شده ام</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {this.setState({isVisibleQuitConfirmModal: false});}} activeOpacity={.85} style={[styles.confirmButton,{backgroundColor:COLORS.Green},FLEX.RNwSbCC]}>
                  <Sooji name="times-circle" color={COLORS.White} size={Normalize(ICONS_SIZE + 2)}/>
                  <Text style={styles.confirmButtonText}>خیر، انصراف</Text>
                </TouchableOpacity>
              </View>

            </View>
        </Modal>
        <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleRenewModal}
            onBackdropPress={() => {this.setState({isVisibleRenewModal: false,primeEndTimeHour:this.state.currentReserve.end_time.hour,primeEndTimeMinute:this.state.currentReserve.end_time.minute});}}
            backdropOpacity={.4}
            style={MODAL_STYLES.center.outside}
        >
            <View style={[MODAL_STYLES.center.inside]}>

              <View style={[{width:'100%',marginBottom:10},FLEX.RNwSbCC]}>
                <TouchableOpacity
                    onPress={() => {this.setState({isVisibleRenewModal: false,primeEndTimeHour:this.state.currentReserve.end_time.hour,primeEndTimeMinute:this.state.currentReserve.end_time.minute});}}
                    activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={[MODAL_STYLES.center.titleBox,FLEX.RNwFeCS]}>
                  <Text style={MODAL_STYLES.titleText}>زمان جدید خروج را وارد کنید</Text>
                </View>
              </View>

              <View style={{marginBottom:15}}>
                <TouchableOpacity
                    onPress={()=>{
                        this.setState({isVisibleRenewModal:false});
                        setTimeout(()=>{this.setState({isTimePickerVisible:true})},700);
                    }}
                    style={[styles.actionButton,{paddingVertical:15,backgroundColor:COLORS.White,width: '100%'}]}>
                        <Text style={[styles.actionButtonText,{color: COLORS.DarkBlue}]}>
                        {this.state.currentReserve && !this.state.isLoading ?
                        `${this.state.primeEndTimeHour == 0 ? "00" : this.state.primeEndTimeHour}:${this.state.primeEndTimeMinute == 0 ? "00" : this.state.primeEndTimeMinute}`
                        : 'در حال بارگزاری'
                        }
                        </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{
                    this.renewReserve()
                }} style={[styles.submitButton,{width: '100%',backgroundColor: COLORS.Green}]} disabled={this.state.isLoading}>
                    <Text style={styles.submitText}>تایید تمدید رزرو</Text>
                </TouchableOpacity>
              </View>
            </View>
        </Modal>
        {/* result modal */}
        <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleResultModal}
            onBackdropPress={() => {
                this.props.setCurrentReserve(false,0).then(
                    ()=>{
                        this.setState({isVisibleResultModal: false},()=>{
                            this.props.setCurrentBase('dashboard');
                        });
                    }
                )
            }}
            backdropOpacity={.4}
            style={MODAL_STYLES.bottom.outside}
        >
            <View style={[MODAL_STYLES.bottom.inside]}>

              <View style={[{width:'100%',marginBottom:15},FLEX.RNwSbCC]}>
                <TouchableOpacity onPress={() => {
                    this.props.setCurrentReserve(false,0).then(
                        ()=>{
                            this.setState({isVisibleResultModal: false},()=>{
                                this.props.setCurrentBase('dashboard');
                            });
                        }
                    )
                }} activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={[MODAL_STYLES.titleBox,FLEX.RNwFeCS]}>
                  <Text style={MODAL_STYLES.titleText}>خروج با موفقیت ثبت شد!</Text>
                  <Icon name="check-circle" color={COLORS.Green} style={{paddingLeft:4}} size={Normalize(ICONS_SIZE + 3)}/>
                </View>
              </View>
                
            <View style={[styles.resultInfoBox,FLEX.RNwSbCC]}>
                <Text style={styles.infoTextDetail}>
                    {this.state.exitEvent ? this.state.exitEvent.duration.minute : ""}
                </Text>
                <Text style={styles.infoTextTitle}>
                 {this.props.lang.Wallet.ReserveInfo.duration}
                </Text>
            </View>

            <View style={[styles.resultInfoBox,FLEX.RNwSbCC]}>
                <Text style={styles.infoTextDetail}>
                    {this.state.currentReserve ?
                        this.state.currentReserve.park_event_code : null}
                </Text>
                <Text style={styles.infoTextTitle}>
                    {this.props.lang.Wallet.ReserveInfo.code}
                </Text>
            </View>
            
            <View style={[styles.resultInfoBox,{paddingTop:5,marginBottom:20}]}>
                {this.state.transaction ? this.state.transaction.transactions.map(
                    (v,k)=>
                    <View key={k} style={[styles.detailBox,this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
                        <View style={[FLEX.RNwFeCS]}>
                            <Text style={[styles.infoTextDetail,{paddingTop:4,fontFamily: this.props.isRTL ? FONTS.extraBold : FONTS.secondFontFamily}]}>
                                {v.amount}
                            </Text>
                        </View>      
                        <View style={[this.props.isRTL ? FLEX.RNwFsCS : FLEX.RrNwFsCS]}>
                            {
                            v.point > 0 ?
                            <View style={[FLEX.RNwFsCS,{marginRight:10,marginLeft:10,backgroundColor:COLORS.LightGrey,borderRadius:BORDER_RADIUS.items - 10,padding:5}]}>
                                <Image source={require('./../../../Static/orange-gem.png')} resizeMode={"contain"} style={styles.gem}/>
                                <Text style={styles.gemCount}>{v.point}</Text>
                            </View> : null
                            }
                            <Text style={styles.infoTextTitle}>
                                {v.title}
                            </Text>
                        </View> 
                    </View>
                )
                :null}
                <View style={[styles.totalBox,FLEX.RNwSbCC]}>
                    <View style={[FLEX.RNwFeCS]}>
                        <Text style={[styles.totalPrice,{color:parseInt(this.state.transaction ? this.state.transaction.total : 0) >= 0 ? COLORS.Green : COLORS.Red,paddingTop:4}]}>
                        {this.state.transaction ? this.state.transaction.total : ''}
                        </Text>
                    </View>        
                    <View style={[FLEX.RNwFeCS]}>
                        <Text style={[styles.totalCurrency]}>
                            ({this.props.lang.Wallet.currency})
                        </Text>
                        <Text style={styles.totalTitle}>
                            {this.props.lang.Wallet.ReserveInfo.totalPrice}
                        </Text>
                    </View>            
                </View>
            </View>

            </View>
        </Modal>
        <View style={styles.container} showsVerticalScrollIndicator={false}>
        {/* ALL */}
            <View style={[FLEX.CNwSbSS,{minHeight:Platform.OS == "ios" ? isIphoneX() ? DIMENSIONS.height - 195 : DIMENSIONS.height - 155 : DIMENSIONS.height - 175,}]}>
                {/* UP */}
                <View style={{position:'absolute',top:0,right:0,left:0}}>
                    <View style={styles.titleBox}>
                        <Text style={styles.placeName}>
                            {this.state.currentReserve ? this.state.currentReserve.park_slot.place_title : '...'}
                        </Text>
                        <Image source={require('./../../../Static/blue-spacer.png')} style={styles.spacer} resizeMode={"contain"}/>
                        <Text style={styles.parkingName}>
                            {this.state.currentReserve ? this.state.currentReserve.park_slot.title : '...'}
                        </Text>
                    </View>

                    <AnimatedCircularProgress
                        size={DIMENSIONS.width/2}
                        style={{marginLeft:'auto',marginRight:'auto',marginTop:20}}
                        width={16}
                        // fill={10}
                        fill={
                            this.state.currentReserve ? 
                            this.state.endTime.getTime() > Date.now() ?
                            ( Date.now() - this.state.startTime.getTime() )/( this.state.endTime.getTime() - this.state.startTime.getTime() )*100 
                            :0
                            :0
                        }
                        tintColor={
                            this.state.currentReserve ? 
                            Date.now() - this.state.endTime.getTime() < 0 ?
                            COLORS.Black
                            : COLORS.Red
                            : COLORS.Black
                        }
                        rotation={180}
                        backgroundColor={
                            this.state.currentReserve ? 
                            Date.now() - this.state.endTime.getTime() < 0 ?
                            COLORS.LightGrey
                            : COLORS.Red
                            : COLORS.LightGrey
                            }>
                        {
                            () => (
                            <View style={{width:'100%',height:'100%',justifyContent:'center',alignContent:'center',alignItems:'center'}}>
                                <Text style={styles.counterTitle}>{this.state.currentReserve ? 'کد رزرو : '+this.state.currentReserve.park_event_code : ''}</Text>
                                <Text style={styles.timer}>{this.state.countDownString}</Text>
                                <View style={[FLEX.RNwCCC,{marginTop:'10%'}]}>
                                    <Text style={styles.endTime}>
                                    {
                                        this.state.currentReserve ?
                                        `${this.state.currentReserve.end_time.hour}:${this.state.currentReserve.end_time.minute == 0 ? "00" : this.state.currentReserve.end_time.minute}`
                                        : '...'
                                    }
                                    </Text>
                                    <Icon name="clock" solid={true} color={COLORS.DarkBlue} size={Normalize(13)}/>
                                </View>
                            </View>
                            )
                        }
                    </AnimatedCircularProgress>
                    {this.state.endTime.getTime() < Date.now() && !this.state.isLoading ?
                    <View style={{width:DIMENSIONS.width - 100,paddingTop:30,paddingBottom:30,marginLeft:'auto',marginRight:'auto'}}>
                        <Text style={{fontFamily:FONTS.bold,fontSize:Normalize(12),color:COLORS.Red,textAlign:'center',lineHeight:30}}>
                            زمان مجاز حضور شما در پارکینگ به پایان رسیده
                            {`\n`}
                             خروج خود را اعلام کنید!
                        </Text>
                    </View>
                    : null}
                </View>

                        {/* DOWN */}
                <View style={{position:'absolute',bottom:0,left:0,right:0}}>
                    <View style={[FLEX.RNwSbCS,{width:'100%',paddingTop:20}]}>
                        <TouchableOpacity onPress={()=>Communications.phonecall(SUPPORT_NUMBER,true)} style={styles.actionButton}>
                            <View style={[FLEX.CWCC]}>
                                {Platform.OS == "ios" && isIphoneX() ?
                                <Sooji name="danger" color={COLORS.Orange} size={Normalize(23)} style={{marginBottom:20}}/>
                                 : null}
                                <Text style={styles.actionText}>پشتیبانی</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.setState({isVisibleRenewModal:true})} style={styles.actionButton}>
                            <View style={[FLEX.CWCC]}>
                                {Platform.OS == "ios" && isIphoneX() ?
                                <Sooji name="renew" color={COLORS.Green} size={Normalize(23)} style={{marginBottom:20}}/>
                                : null}
                                <Text style={styles.actionText}>تمدید رزرو</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={()=>this.setState({isVisibleQuitConfirmModal:true})} style={styles.quit}>
                        <Text style={styles.quitText}>خروج از پارکینگ</Text>
                    </TouchableOpacity>
                </View>
            </View>
            
        </View>

    </View>
    )
  }
}

const styles = StyleSheet.create({
    submitButton: {
        alignItems:'center',
        justifyContent:'center',
        height: 50,
        marginLeft:'auto',
        borderRadius:BORDER_RADIUS.items,
        marginTop: 12
      },
      submitText: {
        color: COLORS.White,
        fontSize: Normalize(12),
        textAlign:'center',
        fontFamily: FONTS.extraBold
      },
    gemCount:{
        fontFamily: FONTS.bold,
        textAlign:'right',
        color:COLORS.Orange,
        fontSize:Normalize(12),
        // marginRight:13,
        paddingTop:3
    },
    gem: {
        marginRight:5,
        width:17,
        height:17
    },
    totalCurrency: {
        paddingRight:5,
        color:COLORS.DarkBlue,
        fontFamily: FONTS.light,
        textAlign:'left',
        fontSize:Normalize(10)
    },
    totalPrice: {
        fontFamily: FONTS.extraBold,
        textAlign:'right',
        fontSize:Normalize(17)
    },
    totalTitle: {
        color:COLORS.DarkBlue,
        fontFamily: FONTS.extraBold,
        textAlign:'right',
        fontSize:Normalize(13)
    },
    totalBox: {
        paddingTop:15,
        paddingHorizontal:5
    },
    detailBox: {
        borderBottomWidth:1,
        borderBottomColor:COLORS.LightGrey,
        paddingVertical:15,
        paddingHorizontal:5
    },
    infoTextDetail: {
        color:COLORS.DarkBlue,
        fontFamily: FONTS.extraBold,
        textAlign:'right',
        fontSize:Normalize(12)
    },
    infoTextTitle: {
        color:COLORS.DarkBlue,
        fontFamily: FONTS.bold,
        textAlign:'right',
        fontSize:Normalize(12)
    },
    resultInfoBox: {
        backgroundColor:COLORS.White,
        borderRadius:BORDER_RADIUS.items,
        paddingVertical:18,
        paddingHorizontal: 17,
        marginBottom:13
    },
    confirmButton: {
        borderRadius: BORDER_RADIUS.items,
        padding: 16,
        marginBottom:10
      },
      confirmButtonText: {
        color:COLORS.White,
        fontFamily: FONTS.bold,
        textAlign:'right',
        fontSize:Normalize(13)
      },
    quit: {
        backgroundColor:COLORS.Red,
        borderRadius:BORDER_RADIUS.items,
        padding:Platform.OS == "ios" ? 17 : 12,
        marginTop:10
    },
    quitText: {
        fontFamily:FONTS.extraBold,
        fontSize: Normalize(14),
        textAlign:'center',
        color:COLORS.White,
    },
    actionButton: {
        width:DIMENSIONS.width/2-16,
        height:'auto',
        paddingVertical:Platform.OS == "ios" ? 20 : 15,
        borderRadius: BORDER_RADIUS.items,
        backgroundColor:COLORS.LightGrey
    },
    actionButtonText :{
        fontSize: Normalize(13),
        textAlign:'center',
        fontFamily: FONTS.bold
    },
    actionText: {
        fontFamily:FONTS.bold,
        fontSize: Normalize(13),
        textAlign:'center',
        // paddingTop:15,
        color:COLORS.DarkBlue,
    },
    container: {
        width:'100%',
        padding:10,
        height:'100%',
        height: Platform.OS == "ios" ? isIphoneX() ? DIMENSIONS.height - 165 : DIMENSIONS.height - 125 : DIMENSIONS.height - 145,
        overflow:'hidden',
    },
    titleBox: {
        width:'100%',
        height: 'auto',
        paddingTop:10,
        justifyContent:'center',
        alignItems:'center',
    },
    placeName: {
        fontFamily:FONTS.extraBold,
        fontSize: Normalize(12),
        textAlign:'center',
        color:COLORS.DarkBlue,
    },
    spacer: {
        width: DIMENSIONS.width / 2.3,
        height: 5,
        marginBottom:10,
        marginTop:10
    },
    parkingName: {
        fontFamily:FONTS.secondFontFamily,
        textAlign:'center',
        fontSize: Normalize(18),
        color:COLORS.DarkBlue,
    },
    counterTitle: {
        fontFamily:FONTS.secondFontFamily,
        textAlign:'center',
        fontSize: Normalize(13),
        // letterSpacing:2,
        color:COLORS.DarkBlue,      
    },
    timer: {
        fontFamily:FONTS.bold,
        marginTop:'10%',
        textAlign:'center',
        fontSize: Normalize(22),
        color:COLORS.DarkBlue, 
    },
    endTime: {
        fontFamily:FONTS.bold,
        textAlign:'center',
        fontSize: Normalize(14),
        color:COLORS.DarkBlue, 
        paddingRight:5,
        paddingTop:2,
    }
})