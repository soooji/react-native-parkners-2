import React, { Component } from 'react'
import { Text, View , StyleSheet,TouchableOpacity,Image} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX,MODAL_STYLES,ICONS_SIZE,ALERT,BASE_IMG_URL,BASE_URL, ISEXPIRED, SETTOKEN, USER_ROLE, DIMENSIONS, Sooji,SUPPORT_NUMBER} from '../../../Base/functions';
import Communications from 'react-native-communications';
import Icon from 'react-native-vector-icons/FontAwesome5';
import axios from 'axios'
import { isIphoneX } from 'react-native-iphone-x-helper';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Modal from "react-native-modal";
export default class UserReserveItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,

      isVisibleEdit: false,

      selectedCarName: '',
      selectedCarId: 0,

      isTimePickerVisible: false,
      isStartTime: true,
      startTimeHours: '',
      startTimeMiuntes: '',
      endTimeHours: '',
      endTimeMiuntes: '',
      startTimeObject: new Date(),
      endTimeObject: new Date(),

      isVisibleResultModal: false,
      isVisibleCarModal: false,
      isVisibleEnterConfirmModal: false,
      isVisibleCancelModal: false,
      isVisibleDelayModal: false
    }
  }
  componentDidMount() {
    this.resetFields();
    this.createDateObject();
  }
  enterParking = async () => {
    try {
      this.setState({isLoading:true});
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'GET',
          url: `${BASE_URL}park_events/${this.props.parkEventId}/enter/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
      };
      axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
      })
      .then(r=>{
        this.setState({isVisibleEnterConfirmModal:false});
        this.props.setCurrentReserve(true,this.props.parkEventId);
        this.setState({isLoading:false});
      })
      .then(r=>{
        setTimeout(()=>this.props.setCurrentBase("current-reserve"),
        500)
      })
      .catch(e=>{
          console.log(e);
          this.setState({isLoading:false});
          if(!e.response) {this.props.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status == 500) this.props.toggleAlert(true);
          else this.props.toggleAlert(true,e.response.data.error_message);
      })
    } catch (error) {
        console.log(error);
        this.setState({isLoading:false});
        this.props.setCurrentBase('auth');
    }
  }
  deleteReserve = async () => {
    try {
      this.setState({isLoading:true});
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      this.setState({isVisibleCancelModal:false});
      const options = {
          method: 'DELETE',
          url: `${BASE_URL}park_events/${this.props.parkEventId}/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
      };
      axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
          this.setState({isLoading:false});
      })
      .then(r=>{
        this.props.getParkingList();
      })
      .catch(e=>{
          console.log(e);
          this.setState({isLoading:false});
          if(!e.response) {this.props.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status == 500) this.props.toggleAlert(true);
          else this.props.toggleAlert(true,e.response.data.error_message);
      })
    } catch (error) {
        console.log(error);
        this.setState({isLoading:false});
        this.props.setCurrentBase('auth');
    }
  }
  putReserve = async () => {
    try {
      this.setState({isLoading:true});
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'PUT',
          url: `${BASE_URL}park_events/${this.props.parkEventId}/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
          data : {
            park_event : {
              car_id : this.state.selectedCarId,
              start_time: {
                day:this.props.startTime.day,
                hour: parseInt(this.state.startTimeHours),
                minute: parseInt(this.state.startTimeMiuntes),
                month: this.props.startTime.month,
                year: this.props.startTime.year,
                microsecond: 0,
                second: 0,
              },
              end_time : {
                day:this.props.endTime.day,
                hour: parseInt(this.state.endTimeHours),
                minute: parseInt(this.state.endTimeMiuntes),
                month: this.props.endTime.month,
                year: this.props.endTime.year,
                microsecond: 0,
                second: 0,
              }
            }
          }
      };
      axios(options)
      .then(r=>{
          SETTOKEN(r.data.token);
          this.setState({isLoading:false});
      })
      .then(r=>{
        this.setState({isVisibleEdit: false});
        this.props.getParkingList();
      })
      .catch(e=>{
          console.log(e);
          this.setState({isLoading:false});
          if(!e.response) {this.props.toggleAlert(true);return;}
          if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
          if(e.response.status == 500) this.props.toggleAlert(true);
          else this.props.toggleAlert(true,e.response.data.error_message);
      })
    } catch (error) {
        console.log(error);
        this.setState({isLoading:false});
        this.props.setCurrentBase('auth');
    }
  }
  resetFields() {
    this.setState({
      selectedCarId:this.props.carId,
      selectedCarName:this.props.carName,
      startTimeHours:this.props.startTime ? this.props.startTime.hour.toString() : null,
      startTimeMiuntes:this.props.startTime ? this.props.startTime.minute.toString() : null,
      endTimeHours:this.props.endTime ? this.props.endTime.hour.toString() : null,
      endTimeMiuntes:this.props.endTime ? this.props.endTime.minute.toString() : null,
    });
  }
  toggleActions(open) {
    this.setState({isOpen:open})
    if(!open) {
      this.setState({isVisibleEdit:false});
      this.resetFields();
    }
  }
  createDateObject() {
    let startTime = new Date();
    startTime.setHours(this.props.startTime.hour,this.props.startTime.minute);
    this.setState({startTimeObject:startTime});

    let endTime = new Date();
    endTime.setHours(this.props.endTime.hour,this.props.endTime.minute);
    this.setState({endTimeObject:endTime});
  }
  confirmTimePicker(time) {
    let t = time.toTimeString();
    if(this.state.isStartTime) {
      this.setState({startTimeHours:t.slice(0,2),startTimeMiuntes:t.slice(3,5),isTimePickerVisible:false});
      let newTime = new Date();
      newTime.setHours(t.slice(0,2),t.slice(3,5));
      this.setState({startTimeObject:newTime});
    } else {
      this.setState({endTimeHours:t.slice(0,2),endTimeMiuntes:t.slice(3,5),isTimePickerVisible:false});
      let newTime = new Date();
      newTime.setHours(t.slice(0,2),t.slice(3,5));
      this.setState({endTimeObject:newTime});
    }
  }
  render() {
    return (
      <TouchableOpacity activeOpacity={1} onPress={()=>{!this.state.isOpen ? this.toggleActions(true) : null}} style={styles.container}>      
      
        <DateTimePicker
          date={this.state.isStartTime ? this.state.startTimeObject : this.state.endTimeObject}
          is24Hour={true}
          titleIOS={`زمان ${this.state.isStartTime ? `ورود` : `خروج`} را انتخاب کنید`}
          titleStyle={{fontFamily:FONTS.bold,fontSize:Normalize(13),paddingBottom:10,paddingTop:10,color:COLORS.DarkBlue}}
          confirmTextIOS="تایید"
          confirmTextStyle={{fontFamily:FONTS.bold,fontSize:Normalize(16),paddingBottom:10,paddingTop:10}}
          cancelTextIOS="لغو"
          cancelTextStyle={{fontFamily:FONTS.light,fontSize:Normalize(15),paddingBottom:10,paddingTop:10}}
          isVisible={this.state.isTimePickerVisible}
          onConfirm={(time)=>{this.confirmTimePicker(time);}}
          mode={'time'}
          onCancel={()=>this.setState({isTimePickerVisible:false})}
        />
      {/* select car modal */}
      <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleCarModal}
            onBackdropPress={() => this.setState({isVisibleCarModal: false})}
            onBackdropPress={() => this.setState({isVisibleCarModal: false})}
            backdropOpacity={.4}
            style={MODAL_STYLES.bottom.outside}
        >
            <View style={[MODAL_STYLES.bottom.inside]}>

              <View style={[{width:'100%',marginBottom:10},FLEX.RNwSbCC]}>
                <TouchableOpacity onPress={()=>this.setState({isVisibleCarModal:false})} activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={MODAL_STYLES.titleBox}><Text style={MODAL_STYLES.titleText}>
                خودرو خود را انتخاب کنید
                </Text></View>
              </View>
            
                <View style={{paddingBottom:25}}>
                  {this.props.cars.map(
                      (v,k)=>
                      <TouchableOpacity key={k} onPress={()=>this.setState({selectedCarName: v.given_name,selectedCarId:v.id,isVisibleCarModal:false})} activeOpacity={.85} style={[FLEX.RNwSbCS,styles.carItemBox]}>
                        <Image source={{uri:`${BASE_IMG_URL}${v.icon.icon.url}`}} style={styles.carImage} resizeMode={"contain"}/>
                        <Text style={styles.carName}>
                          {v.given_name}
                        </Text>
                      </TouchableOpacity>
                  )}
                </View>
                
            </View>
        </Modal>
      {/* enter confirmation */}
      <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleEnterConfirmModal}
            onBackdropPress={() => {this.setState({isVisibleEnterConfirmModal: false});}}
            onBackdropPress={() => {this.setState({isVisibleEnterConfirmModal: false});}}
            backdropOpacity={.4}
            style={MODAL_STYLES.bottom.outside}
        >
            <View style={[MODAL_STYLES.bottom.inside]}>

              <View style={[{width:'100%',marginBottom:10},FLEX.RNwSbCC]}>
                <TouchableOpacity onPress={() => {this.setState({isVisibleEnterConfirmModal: false});}} activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={[MODAL_STYLES.titleBox,FLEX.RNwFeCS]}>
                  <Text style={MODAL_STYLES.titleText}>آیا وارد پارکینگ شده اید؟</Text>
                </View>
              </View>

              <View style={{marginBottom:10}}>
                <TouchableOpacity onPress={()=>{                  
                  this.enterParking()
                  }} activeOpacity={.85} style={[styles.confirmButton,{backgroundColor:COLORS.Green},FLEX.RNwSbCC]}>
                  <Sooji name="quit" color={COLORS.White} size={Normalize(ICONS_SIZE + 2)}/>
                  <Text style={styles.confirmButtonText}>بله، وارد شده ام</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {this.setState({isVisibleEnterConfirmModal: false});}} activeOpacity={.85} style={[styles.confirmButton,{backgroundColor:COLORS.DarkGrey},FLEX.RNwSbCC]}>
                  <Sooji name="times-circle" color={COLORS.White} size={Normalize(ICONS_SIZE + 2)}/>
                  <Text style={styles.confirmButtonText}>خیر، وارد نشده ام</Text>
                </TouchableOpacity>
              </View>
            
            </View>
        </Modal>
      {/* cancel confirmation */}
      <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleCancelModal}
            onBackdropPress={() => {this.setState({isVisibleCancelModal: false});this.props.setCurrentBase('dashboard');}}
            onBackdropPress={() => {this.setState({isVisibleCancelModal: false});this.props.setCurrentBase('dashboard');}}
            backdropOpacity={.4}
            style={MODAL_STYLES.bottom.outside}
        >
            <View style={[MODAL_STYLES.bottom.inside]}>

              <View style={[{width:'100%',marginBottom:10},FLEX.RNwSbCC]}>
                <TouchableOpacity onPress={() => {this.setState({isVisibleCancelModal: false});this.props.setCurrentBase('dashboard');}} activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={[MODAL_STYLES.titleBox,FLEX.RNwFeCS]}>
                  <Text style={MODAL_STYLES.titleText}>آیا از لغو رزرو اطمینان دارید؟</Text>
                  <Icon name="check-circle" color={COLORS.Green} style={{paddingLeft:4}} size={Normalize(ICONS_SIZE + 3)}/>
                </View>
              </View>

              <View style={{marginBottom:10}}>
                <TouchableOpacity onPress={()=>this.deleteReserve()} activeOpacity={.85} style={[styles.confirmButton,{backgroundColor:COLORS.Red},FLEX.RNwSbCC]}>
                <Sooji name="times-circle" color={COLORS.White} size={Normalize(ICONS_SIZE + 2)}/>
                  <Text style={styles.confirmButtonText}>بله، رزرو را لغو کن</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.setState({isVisibleCancelModal:false})} activeOpacity={.85} style={[styles.confirmButton,{backgroundColor:COLORS.Green},FLEX.RNwSbCC]}>
                <Sooji name="quit" color={COLORS.White} size={Normalize(ICONS_SIZE + 2)}/>
                  <Text style={styles.confirmButtonText}>خیر، انصراف</Text>
                </TouchableOpacity>
              </View>
            
            </View>
        </Modal>
      {/* Delay confirmation */}
      <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleDelayModal}
            onBackdropPress={() => {this.setState({isVisibleDelayModal: false});}}
            onBackdropPress={() => {this.setState({isVisibleDelayModal: false});}}
            backdropOpacity={.4}
            style={MODAL_STYLES.bottom.outside}
        >
            <View style={[MODAL_STYLES.bottom.inside]}>

              <View style={[{width:'100%',marginBottom:10},FLEX.RNwSbCC]}>
                <TouchableOpacity onPress={() => {this.setState({isVisibleDelayModal: false});}} activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={[MODAL_STYLES.titleBox,FLEX.RNwFeCS]}>
                  <Text style={MODAL_STYLES.titleText}>اعلام تاخیر در ورود</Text>
                  <Icon name="check-circle" color={COLORS.Green} style={{paddingLeft:4}} size={Normalize(ICONS_SIZE + 3)}/>
                </View>
              </View>
                <View style={{width:'100%',backgroundColor:'white',borderRadius:BORDER_RADIUS.items,marginBottom:10}}>
                  <Text style={{textAlign:'center',color:COLORS.DarkBlue,fontSize:Normalize(11),fontFamily:FONTS.bold,padding:15,lineHeight:25}}>
                    ۱۵۰۰ تومان برای ثبت ۱۵ دقیقه تاخیر در ورود کسر و در غیر این صورت رزرو جاری لغو خواهد شد
                  </Text>
                </View>
              <View style={{marginBottom:10}}>
                <TouchableOpacity onPress={()=>this.deleteReserve()} activeOpacity={.85} style={[styles.confirmButton,{backgroundColor:COLORS.Red},FLEX.RNwSbCC]}>
                  <View></View>
                  <Text style={styles.confirmButtonText}>ثبت تاخیر</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.setState({isVisibleDelayModal:false})} activeOpacity={.85} style={[styles.confirmButton,{backgroundColor:COLORS.Green},FLEX.RNwSbCC]}>
                  {/* <Sooji name="times-circle" color={COLORS.White} size={Normalize(ICONS_SIZE + 2)}/> */}
                  <View></View>
                  <Text style={styles.confirmButtonText}>انصراف</Text>
                </TouchableOpacity>
              </View>
            
            </View>
        </Modal>

      <View style={[styles.flagBox,FLEX.RNwSbCC]}>
        <Text style={styles.flagBoxTextCurrency}>{this.props.flagCurrency}</Text>
        <Text style={styles.flagBoxTextTitle}>{this.props.flagTitle}</Text>
      </View>
      {/* more arrow */}
      <TouchableOpacity onPress={()=>{
        this.toggleActions(!this.state.isOpen)
        }} style={[styles.arrowBox,{backgroundColor:this.state.isOpen ? COLORS.White : 'transparent'}]}>
      {
        !this.state.isOpen ?
        <Sooji name="top" color={COLORS.DarkBlue} style={{paddingTop:4,transform:[{rotate:'180deg'}]}} size={Normalize(ICONS_SIZE - 5)}/> :
        <Sooji name="top" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 5)}/>
      }
      </TouchableOpacity>
       
        <View>
          {
            this.props.parkingName ?
            <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
              <Icon name="parking" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE)}/>
              <Text style={[{fontFamily:FONTS.bold,fontSize: Normalize(14)},styles.infoText]}>
                {this.props.parkingName}
              </Text>
            </View>:null
          }
          {
            this.props.date ?
            <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
              <Icon name="calendar" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE)}/>
              <Text style={[{fontFamily:FONTS.bold,fontSize: Normalize(13)},styles.infoText]}>
                {this.props.date}
              </Text>
            </View>:null
          }
          {
            this.props.startTime || this.props.endTime ?
            <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
              <Icon name="clock" solid={true} color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE)}/>
              <Text style={[{fontFamily:FONTS.bold,fontSize: Normalize(14)},styles.infoText]}>
                {this.props.startTime ?
                `از ${this.props.startTime.hour}:${this.props.startTime.minute == 0 ? '00' : this.props.startTime.minute}` : ""}

                {this.props.endTime ?
                ` ${this.props.isEndless ? "حداکثر تا" : "تا"} ${this.props.endTime.hour}:${this.props.endTime.minute == 0 ? '00' : this.props.endTime.minute}` : ""}
              </Text>
            </View>
            :null
          }
          {this.props.desc ?
            <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
            <Icon name="info-circle" solid={true} color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE)}/>
            <Text style={[{fontFamily:FONTS.light,fontSize:Normalize(12)},styles.infoText]}>
              {this.props.desc}
            </Text>
          </View> : null}
        </View>
      
        {/* action bar */}
        {
          this.state.isOpen ?
          <View>

          {this.props.carId ?
            <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
            <Icon name="car" solid={true} color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE)}/>
            <Text style={[{fontFamily:FONTS.light,fontSize:Normalize(12)},styles.infoText]}>
              {this.props.carName}
            </Text>
          </View> : null}
          
          {this.state.isVisibleEdit ?
              <View>
                <TouchableOpacity onPress={()=>this.setState({isVisibleCarModal:true})} style={styles.whiteTouchBox}>
                    <View style={[FLEX.RNwSbCC,{paddingHorizontal:12}]}>
                        <Text style={styles.carTitle}>
                            {this.state.selectedCarId == 0 ? "انتخاب کنید" : this.state.selectedCarName}
                        </Text>
                        <Text style={styles.carTitle}>
                            تغییر خودرو :
                        </Text>
                    </View>
                </TouchableOpacity>

                <View style={[FLEX.RNwSbCC,{width:'100%'}]}>
                    <TouchableOpacity
                    onPress={()=>{this.setState({isStartTime: false,isTimePickerVisible:true});}}
                    style={[styles.actionButton,{backgroundColor:COLORS.White,width: '42%'}]}>
                    <Text style={[styles.actionButtonText,{color: COLORS.DarkBlue}]}>
                    {`${this.state.endTimeHours}:${this.state.endTimeMiuntes == 0 ? '00' : this.state.endTimeMiuntes}`}
                    </Text>
                    </TouchableOpacity>
                    <View style={[styles.actionButton,{backgroundColor:COLORS.LightGrey,width: '16%'}]}>
                        <Text style={[styles.actionButtonText,{color: COLORS.DarkBlue}]}>تا</Text>
                    </View>
                    <TouchableOpacity
                    onPress={()=>{this.setState({isStartTime: true,isTimePickerVisible:true});}}
                    style={[styles.actionButton,{backgroundColor:COLORS.White,width: '42%'}]}>
                        <Text style={[styles.actionButtonText,{color: COLORS.DarkBlue}]}>
                        {`${this.state.startTimeHours}:${this.state.startTimeMiuntes == 0 ? "00" : this.state.startTimeMiuntes}`}
                        </Text>
                    </TouchableOpacity>
                </View>

                <View style={[FLEX.RNwSbCC,{width: (DIMENSIONS.width - 125),marginLeft:'auto'}]}>
                  <TouchableOpacity onPress={()=>{this.setState({isVisibleEdit: false});}} style={[styles.submitButton,{width: (DIMENSIONS.width - 135)/2,backgroundColor: COLORS.Orange,marginLeft:0}]} disabled={this.props.isLoading}>
                    <Text style={styles.submitText}>انصراف</Text>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={()=>this.putReserve()} style={[styles.submitButton,{width: (DIMENSIONS.width - 135)/2,backgroundColor: COLORS.Green}]} disabled={this.state.isLoading}>
                    <Text style={styles.submitText}>تایید</Text>
                  </TouchableOpacity>
                </View>
                
              </View>
            : 
            <View>
              <View style={[FLEX.RNwSbCC,{width:'100%'}]}>
                  {/* <TouchableOpacity onPress={()=>this.setState({isVisibleDelayModal:true})} style={[styles.actionButton,{backgroundColor:COLORS.White,width: '48.5%',}]}>
                  <Text style={[styles.actionButtonText,{color: COLORS.Black}]}>
                    اعلام تاخیر
                  </Text>
                  </TouchableOpacity> */}
                  <TouchableOpacity onPress={()=>this.setState({isVisibleCancelModal:true})} style={[styles.actionButton,{backgroundColor:COLORS.White,width: '100%'}]}>
                      <Text style={[styles.actionButtonText,{color: COLORS.Black}]}>لغو رزرو</Text>
                  </TouchableOpacity>
              </View>
              <TouchableOpacity onPress={()=>{this.setState({isVisibleEdit: true});}} style={[styles.editButton]}>
                <Text style={styles.editText}>ویرایش رزرو</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>{this.setState({isVisibleEnterConfirmModal: true});}} style={[styles.submitButton,{width: DIMENSIONS.width - 130,backgroundColor: this.props.enterIsVisible ? COLORS.Green : COLORS.DarkGrey}]} disabled={!this.props.enterIsVisible}>
                <Text style={styles.submitText}>ورود به پارکینگ</Text>
              </TouchableOpacity>
            </View>}

          </View>
          :null
        }
        
      </TouchableOpacity>
    )
  }
}
const styles = StyleSheet.create({
    container: {
        width:'100%',
        height: 'auto',
        minHeight: 80,
        position:'relative',
        overflow: 'hidden',
        paddingHorizontal: 15,
        paddingBottom: 15,
        paddingTop:10,
        backgroundColor: COLORS.LightGrey,
        borderRadius: BORDER_RADIUS.items,
        marginTop:15
    },
    confirmButton: {
      borderRadius: BORDER_RADIUS.items,
      padding: 16,
      marginBottom:10
    },
    confirmButtonText: {
      color:COLORS.White,
      fontFamily: FONTS.bold,
      textAlign:'right',
      fontSize:Normalize(13)
    },
    flagBox: {
      position:'absolute',
      left: 0,
      top: 0,
      backgroundColor: COLORS.DarkBlue,
      borderBottomRightRadius: 25,
      paddingVertical: 12,
      paddingHorizontal: 12,
    },
    flagBoxTextTitle: {
      color: COLORS.White,
      fontSize: Normalize(14),
      fontFamily: FONTS.secondFontFamily,
      paddingHorizontal:4,
    },
    flagBoxTextCurrency: {
      color: COLORS.White,
      fontSize: Normalize(9),
      fontFamily: FONTS.medium,
    },
    infoText: {
      color: COLORS.DarkBlue,
      paddingTop:2,
      paddingRight:10,
    },
    infoBox: {
      paddingTop:8,
    },
    arrowBox: {
      zIndex:1111111,
      alignItems: 'center',
      justifyContent: 'center',
      width: 44,
      height: 44,
      position:'absolute',
      borderRadius:20,
      left: 15,
      bottom: 15
    },
    carTitle: {
      color: COLORS.DarkBlue,
      fontSize: Normalize(11),
      textAlign:'center',
      fontFamily: FONTS.bold,
    },
    whiteTouchBox: {
      backgroundColor:COLORS.White,
      paddingVertical: 15,
      borderRadius:BORDER_RADIUS.items,
      marginTop: 11
    },
    editButton : {
      width: '100%',
      alignItems:'center',
      backgroundColor: COLORS.LightBlue,
      justifyContent:'center',
      height: 44,
      marginLeft:'auto',
      borderRadius:BORDER_RADIUS.items,
      marginTop: 12
    },
    editText: {
      color: COLORS.White,
      fontSize: Normalize(12),
      textAlign:'center',
      fontFamily: FONTS.bold
    },
    submitButton: {
      alignItems:'center',
      justifyContent:'center',
      height: 44,
      marginLeft:'auto',
      borderRadius:BORDER_RADIUS.items,
      marginTop: 12
    },
    submitText: {
      color: COLORS.White,
      fontSize: Normalize(12),
      textAlign:'center',
      fontFamily: FONTS.extraBold
    },
    carItemBox: {
      backgroundColor:COLORS.White,borderRadius:BORDER_RADIUS.items,paddingVertical:20,paddingHorizontal:15,marginTop:9
    },
    carName: {
      fontFamily:FONTS.medium,fontSize:Normalize(11),color:COLORS.DarkBlue,textAlign:'right'
    },
    carImage: {
      width:25,height:25
    },
    actionButton: {
        alignItems:'center',
        justifyContent:'center',
        height: 44,
        borderRadius:BORDER_RADIUS.items,
        marginTop: 12
    },
    actionButtonText :{
        fontSize: Normalize(11),
        textAlign:'center',
        fontFamily: FONTS.bold
    },
})