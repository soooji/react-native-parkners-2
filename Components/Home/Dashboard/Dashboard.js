import React, { Component } from 'react';
import { Text, View,StyleSheet,TouchableOpacity,Animated,Easing,ScrollView,TouchableHighlight} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX,MODAL_STYLES,ICONS_SIZE,ALERT,BASE_IMG_URL,BASE_URL, ISEXPIRED, SETTOKEN, USER_ROLE, DIMENSIONS} from '../../../Base/functions';

import axios from 'axios'
import Modal from "react-native-modal";

import AnimeBaseFrame from './../../Base/AnimeBaseFrame';
import Icon from 'react-native-vector-icons/FontAwesome5';
import FullScreenLoading from '../../Base/FullScreenLoading';
import UserReserveItem from './UserReserveItem';
import AutoHeightImage from 'react-native-auto-height-image';

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentActiveTab: 1,
            cars:[],

            isVisibleUpdateModal: false,

            isLoading: false,
            hasNoReserveToday: false,
            hasNoReserveFuture: false,

            tabActiveBoxAnime: new Animated.Value((DIMENSIONS.width - 65)/2 + 20),
            alertAnime: new Animated.Value(-90),

            alertText:'',
            loadingText:'',

            todayParkEvents: [],
            futureParkEvents: []
        }
    }
    componentDidMount() {
        this.getUserParkEvents(true);
        this.setState({loadingText:this.props.lang.Base.Loading.wait,alertText:this.props.lang.Base.Error.simple});
        this.getCars();
    }
    getUserParkEvents = async (isToday) => {
        try {
            this.setState({isLoading:true,hasNoReserveToday: false,hasNoReserveFuture: false});
            this.setState({currentActiveTab:isToday ? 1 : 2});
            const token = await AsyncStorage.getItem('token');
            if(!token) {this.props.setCurrentBase("auth");return;}
            const options = {
                method: 'GET',
                url: `${BASE_URL}park_events/${isToday ? 'today' : 'future'}/`,
                headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
            };
            axios(options)
            .then(r=>{
                SETTOKEN(r.data.token);
                if(isToday) {
                    this.setState({todayParkEvents : r.data.park_events})
                    if(r.data.park_events.length == 0) {
                        this.setState({hasNoReserveToday: true});
                    }
                } else {
                    this.setState({futureParkEvents : r.data.park_events})
                    if(r.data.park_events.length == 0) {
                        this.setState({hasNoReserveFuture: true});
                        // if(this.state.hasNoReserveToday) {
                        //     this.props.setCurrentBase("new-reserve");
                        // }
                    }
                }
            })
            .then(r=>{
                this.setState({isLoading:false});
            })
            .catch(e=>{
                console.log(e);
                this.setState({isLoading:false});
                if(!e.response) {this.toggleAlert(true);return;}
                if(ISEXPIRED(e)) {this.setCurrentBase('auth');return;}
                if(e.response.status == 500) this.toggleAlert(true);
                else this.toggleAlert(true,e.response.data.error_message);
            })
        } catch (error) {
            console.log(error);
            this.setState({isLoading:false});
            this.props.setCurrentBase('auth');
        }
    }
    getCars = async () => {
        try {
            const token = await AsyncStorage.getItem('token');
            if(!token) {this.props.setCurrentBase("auth");return;}
            const options = {
                method: 'GET',
                url: `${BASE_URL}cars/`,
                headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
            };
        axios(options)
            .then(r=>{
                SETTOKEN(r.data.token);
                this.setState({cars:r.data.cars});
            })
            .catch(e=>{
                if(!e.response) {this.toggleAlert(true);return;}
                if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
                if(e.response.status === 500) this.toggleAlert(true);
                else this.toggleAlert(true,e.response.data.error_message);
            })
        } catch (error) {
            this.props.setCurrentBase('auth');
        }
      }
    goToTab(index) {
        if(index == this.state.currentActiveTab) return;
        if(index == 1) {
            this.getUserParkEvents(true);
            Animated.timing(
                this.state.tabActiveBoxAnime,
                {
                  toValue: (DIMENSIONS.width - 65)/2 + 20,
                  easing: Easing.elastic(1.1),
                  duration: 700,
                  useNativeDriver:true
                },
              ).start();
        } else {
            this.getUserParkEvents(false);
            Animated.timing(
                this.state.tabActiveBoxAnime,
                {
                  toValue: 5,
                  easing: Easing.elastic(1.1),
                  duration: 700,
                  useNativeDriver:true
                },
              ).start();
        }
    }
    toggleReserving() {
        this.setState({isLoading: !this.state.isLoading})
    }
    toggleAlert = (open,text = this.props.lang.Base.Error.simple) => {
        if(open) {
            this.setState({alertText: text})
          Animated.timing(
            this.state.alertAnime,
            {
              toValue: 20,
              easing: Easing.elastic(1.2),
              duration: 1000,
              useNativeDriver:true
            },
          ).start();
          setTimeout(()=>{
            this.toggleAlert(false);
          },3000)
        } else {
            setTimeout(() => {
                this.setState({alertText: ''})
              }, 500);
          Animated.timing(
            this.state.alertAnime,
            {
              toValue: -90,
              easing: Easing.ease,
              duration: 500,
              useNativeDriver:true
            },
          ).start();
        }
      }
  render() {
    return (
    <View style={{position:'relative',width:'100%',height:'100%'}}>
    <FullScreenLoading isLoading={this.state.isLoading} text={this.state.loadingText}/>
 
        {/* <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleUpdateModal}
            onBackdropPress={() => {this.setState({isVisibleUpdateModal: false});}}
            onBackdropPress={() => {this.setState({isVisibleUpdateModal: false});}}
            backdropOpacity={.4}
            style={MODAL_STYLES.bottom.outside}
        >
            <View style={[MODAL_STYLES.bottom.inside]}>

              <View style={[{width:'100%',marginBottom:10},FLEX.RNwSbCC]}>
                <TouchableOpacity onPress={() => {this.setState({isVisibleUpdateModal: false});}} activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={[MODAL_STYLES.titleBox,FLEX.RNwFeCS]}>
                  <Text style={MODAL_STYLES.titleText}>بروزرسانی</Text>
                  <Icon name="check-circle" color={COLORS.Green} style={{paddingLeft:4}} size={Normalize(ICONS_SIZE + 3)}/>
                </View>
              </View> 
            
            <View>
                <AutoHeightImage source={require('./../../../Static/rocket.png')} width={1.5 * DIMENSIONS.width} style={{marginLeft:'auto',marginRight:'auto'}}/>

            </View>

            </View>

        </Modal> */}

        <Animated.View style={[FLEX.RNwSbCC,ALERT.alertBox,{
            transform: [{translateY:this.state.alertAnime}]
            ,height:50}]}>
            <Text style={ALERT.alertBoxText}>{this.state.alertText}</Text>
        </Animated.View>

        <ScrollView stickyHeaderIndices={[0]} showsVerticalScrollIndicator={false} style={{width:'100%',height:'100%'}} contentContainerStyle={{paddingHorizontal:20,paddingBottom:80}}>
        <View style={{paddingTop:20,}}>
            <View style={[styles.tabBar]}>

                <Animated.View style={[{
                    transform: [{translateX:this.state.tabActiveBoxAnime}]
                },styles.tabActiveBox]}></Animated.View>

                <View style={FLEX.RNwSbCC}>
                    <TouchableOpacity onPress={()=>this.goToTab(2)} style={[styles.tabItem]}>
                        <Text style={[{color:this.state.currentActiveTab == 2 ? COLORS.White : COLORS.DarkBlue},styles.tabItemText]}>
                            {this.props.lang.Home.Dashboard.Dashboard.otherDays}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.goToTab(1)} style={[styles.tabItem]}>
                        <Text style={[{color:this.state.currentActiveTab == 1 ? COLORS.White : COLORS.DarkBlue},styles.tabItemText]}>
                            {this.props.lang.Home.Dashboard.Dashboard.today}
                        </Text>
                    </TouchableOpacity>
                </View>

            </View>
        </View>

        <AnimeBaseFrame baseState={this.state.currentActiveTab} componentStateName={1} isFullSize={false} startDelay={700}>
            <View style={{width:'100%'}}>
            {this.state.hasNoReserveToday ?
                this.props.isRTL ?
                <AutoHeightImage source={require('./../../../Static/no-reserve-2.png')} width={DIMENSIONS.width / 2} style={{marginLeft:'auto',marginRight:'auto',marginTop:40}}/>
                : <AutoHeightImage source={require('./../../../Static/no-reserve-en.png')} width={DIMENSIONS.width / 2} style={{marginLeft:'auto',marginRight:'auto',marginTop:40}}/>
            : null}

            {this.state.hasNoReserveToday ?
            <TouchableOpacity onPress={()=>this.props.setCurrentBase('new-reserve')} style={[styles.newReserveButton]}>
                <Text style={styles.newReserveButtonText}>
                    رزرو جای پارک جدید
                </Text>
            </TouchableOpacity>
            : null}

            {this.state.todayParkEvents.map(
                (v,k)=>
                <UserReserveItem
                    getParkingList={()=>this.getUserParkEvents(true)}
                    key={k}
                    lang={this.props.lang}
                    isRTL={this.props.isRTL}
                    parkEventId={v.id}
                    setCurrentReserve={(has,id)=>this.props.setCurrentReserve(has,id)}
                    setCurrentBase={(name)=>this.props.setCurrentBase(name)}
                    toggleReserving={()=>this.toggleReserving()}
                    toggleAlert={(open,text)=>this.toggleAlert(open,text)}
                    flagTitle={v.park_slot.title}
                    parkingName={v.park_slot.place_title} 
                    desc={v.parking_type.description}
                    carId={v.car.id}
                    isEndless={v.is_endless}
                    carName={v.car.given_name}
                    startTime={v.start_time}
                    endTime={v.end_time}
                    cars={this.state.cars}
                    enterIsVisible={true}
                />  
            )}
            </View>
        </AnimeBaseFrame>

        <AnimeBaseFrame baseState={this.state.currentActiveTab} componentStateName={2} isFullSize={false} startDelay={700}>
            <View style={{width:'100%'}}>
                {this.state.hasNoReserveFuture ?
                    this.props.isRTL ?
                    <AutoHeightImage source={require('./../../../Static/no-reserve-2.png')} width={DIMENSIONS.width / 2} style={{marginLeft:'auto',marginRight:'auto',marginTop:40}}/>
                    : <AutoHeightImage source={require('./../../../Static/no-reserve-en.png')} width={DIMENSIONS.width / 2} style={{marginLeft:'auto',marginRight:'auto',marginTop:40}}/>
                : null}
                {this.state.hasNoReserveFuture ?
                <TouchableOpacity onPress={()=>this.props.setCurrentBase('new-reserve')} style={[styles.newReserveButton]}>
                    <Text style={styles.newReserveButtonText}>
                        رزرو جای پارک جدید
                    </Text>
                </TouchableOpacity>
                : null}
                {this.state.futureParkEvents.map(
                (v,k)=>
                <UserReserveItem 
                    getParkingList={()=>this.getUserParkEvents(false)}
                    key={k}
                    parkEventId={v.id}
                    isEndless={v.is_endless}
                    lang={this.props.lang}
                    isRTL={this.props.isRTL}
                    setCurrentReserve={(has,id)=>this.props.setCurrentReserve(has,id)}
                    setCurrentBase={(name)=>this.props.setCurrentBase(name)}
                    toggleReserving={()=>this.toggleReserving()}
                    toggleAlert={(open,text)=>this.toggleAlert(open,text)}
                    flagTitle={v.park_slot.title}
                    parkingName={v.parking_type.place_title} 
                    desc={v.parking_type.description}
                    carId={v.car.id}
                    carName={v.car.given_name}
                    startTime={v.start_time}
                    endTime={v.end_time}
                    cars={this.state.cars}
                    enterIsVisible={false}
                    date={`${v.start_time.day_name} ${v.start_time.day} ${v.start_time.month_name}`}
                />
            )}
            </View>
        </AnimeBaseFrame>

        </ScrollView>
    </View>
    )
  }
}
const styles = StyleSheet.create({
    newReserveButtonText: {
        fontFamily:FONTS.extraBold,
        fontSize: Normalize(12.5),
        color: 'white'
    },
    newReserveButton: {
        justifyContent:'center',
        alignItems:'center',
        marginRight:'auto',
        marginLeft:'auto',
        marginTop:30,
        marginBottom:40,
        paddingVertical:13,
        paddingHorizontal:20,
        zIndex:111111111111111111,
        backgroundColor:COLORS.Green,
        shadowColor: 'rgba(62,209,53,0.57)',
        shadowOffset: {
          width:0,
          height:8
        },
        shadowOpacity: 1,
        shadowRadius: 17,
        borderRadius:BORDER_RADIUS.items + 10,
    },
    container: {
        width:'100%',
        height: '100%',
        overflow:'hidden'
    },
    tabBar: {
        padding:5,
        backgroundColor: COLORS.LightGrey,
        borderRadius: BORDER_RADIUS.base,
        position:'relative',
    },
    tabItem: {
        width: (DIMENSIONS.width - 65)/2,
        height:'100%',
        justifyContent:'center',
        alignItems:'center',
        paddingVertical:12,
    },
    tabItemText: {
        fontFamily:FONTS.bold,
        fontSize: Normalize(12.5),
    },
    tabActiveBox : {
        // zIndex:111,
        backgroundColor: COLORS.DarkBlue,
        borderRadius:BORDER_RADIUS.base,
        width: (DIMENSIONS.width - 65)/2,
        height:'100%',
        height:'100%',
        top:5,
        position:'absolute'
    }
})