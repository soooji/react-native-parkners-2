import React, { Component } from 'react'
import { Text, View,StyleSheet,ScrollView,Animated,Easing,TouchableOpacity } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX,MODAL_STYLES,ICONS_SIZE,ALERT,BASE_IMG_URL,BASE_URL, ISEXPIRED, SETTOKEN, USER_ROLE, DIMENSIONS, Sooji} from '../../../Base/functions';
import ReserveItem from './ReserveItem'
import Icon from 'react-native-vector-icons/FontAwesome5';
import Modal from "react-native-modal";
import axios from 'axios'
import FullScreenLoading from './../../Base/FullScreenLoading'
import { isIphoneX } from 'react-native-iphone-x-helper';
export default class SearchResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isReserving: false,
            alertAnime: new Animated.Value(-90),
            cars: [],
            alertText:'',
        }
    }
    componentDidMount() {
      this.setState({alertText:this.props.lang.Base.Error.simple})
      this.getCars();
    }
    getCars = async () => {
      try {
          this.setState({isLoading:true});
          const token = await AsyncStorage.getItem('token');
          if(!token) {this.props.setCurrentBase("auth");return;}
          const options = {
              method: 'GET',
              url: `${BASE_URL}cars/`,
              headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
          };
      axios(options)
          .then(r=>{
              SETTOKEN(r.data.token);
              this.setState({cars:r.data.cars});
          })
          .then(r=>{
              this.setState({isLoading:false});
          })
          .catch(e=>{
              console.log(e);
              this.setState({isLoading:false});
              if(!e.response) {this.toggleAlert(true);return;}
              if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
              if(e.response.status === 500) this.toggleAlert(true);
              else this.toggleAlert(true,e.response.data.error_message);
          })
      } catch (error) {
          console.log(error);
          this.setState({isLoading:false});
          this.props.setCurrentBase('auth');
      }
    }
    toggleReserving() {
        this.setState({isReserving: !this.state.isReserving})
    }
    toggleAlert = (open,text = "خطا در رزرو") => {
        if(open) {
            this.setState({alertText: text})
          Animated.timing(
            this.state.alertAnime,
            {
              toValue: 20,
              easing: Easing.elastic(1.2),
              duration: 1000,
              useNativeDriver:true
            },
          ).start();
          setTimeout(()=>{
            this.toggleAlert(false);
          },3000)
        } else {
          setTimeout(() => {
            this.setState({alertText: ''})
          }, 500);
          Animated.timing(
            this.state.alertAnime,
            {
              toValue: -90,
              easing: Easing.ease,
              duration: 500,
              useNativeDriver:true
            },
          ).start();
        }
      }
  render() {
    return (
        <View style={{width:'100%',height:'100%'}}>
            <FullScreenLoading isLoading={this.state.isReserving} text={this.props.lang.Home.NewReserve.isReserving}/>
              <Animated.View style={[ALERT.topFixBar,{height:50,transform: [{translateY:this.state.alertAnime}]}]}>
                <View style={[FLEX.RNwSbCC,ALERT.alertBox,{height:50}]}>
                    <Text style={ALERT.alertBoxText}>{this.state.alertText}</Text>
                </View>
              </Animated.View>
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{paddingBottom:50}} stickyHeaderIndices={[0]}>
                    <View style={{paddingTop:20,overflow:'visible'}}>
                      <View style={[{position:'relative'},styles.placeTitleBox,this.props.isRTL ? FLEX.RNwSbCS : FLEX.RrNwSbCS]}>
                        <TouchableOpacity onPress={()=>this.props.backToSearch()} style={{padding:20}}>
                          <Sooji name="left" color={COLORS.Black} size={Normalize(10)}/>
                        </TouchableOpacity>
                        <Text style={[styles.placeTitleText]}>{this.props.placeName} ، {this.props.fullDate}</Text>
                      </View>
                    </View>
                    
                    <View>
                      {console.log(this.props.possible_park_events)}
                      {this.props.possible_park_events.map(
                          (v,k)=>
                          <ReserveItem
                            key={k}
                            isRTL={this.props.isRTL}
                            isYellow={v.is_yellow}
                            lang={this.props.lang}
                            cars={this.state.cars}
                            parkSlotId={v.park_slot.id}
                            setCurrentBase={(name)=>this.props.setCurrentBase(name)}
                            toggleReserving={()=>this.toggleReserving()}
                            toggleAlert={(open,text)=>this.toggleAlert(open,text)}
                            flagTitle={v.price}
                            flagCurrency={this.props.isRTL ? "تومان" : "$"}
                            parkingName={v.park_slot.title}
                            startTime={v.start_time ? v.start_time : null}
                            endTime={v.end_time ? v.end_time : null}
                            desc={v.description}
                          />
                        )}
                    </View>
                </ScrollView>
            </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
    container: {
        width:'100%',
        height: '100%',
        paddingHorizontal: 20,        
    },
    placeTitleBox: {
        width: '100%',
        // paddingVertical: 0,
        borderRadius: BORDER_RADIUS.base,
        backgroundColor: COLORS.LightGrey,
    },
    placeTitleText: {
        fontFamily:FONTS.bold,
        textAlign: 'right',
        paddingRight:20,
        paddingLeft:20,
        color: COLORS.DarkBlue,
        fontSize: Normalize(10)
    },
    topFixBar: {
      width: DIMENSIONS.width,
      paddingHorizontal: 20,
      paddingTop:20,
      position:'absolute',
      zIndex:111111
    },
    prevStepButton : {
      overflow: 'hidden',
      backgroundColor:COLORS.LightGrey,
      borderRadius:BORDER_RADIUS.items + 10,
      paddingHorizontal:20,
    },
    prevStepButtonText: {
        fontFamily:FONTS.bold,
        fontSize: Normalize(12),
        color: COLORS.Black,
        textAlign: 'right',
    },
    alertBox: {
      position:'absolute',
      width: DIMENSIONS.width - 40,
      zIndex:111111111111111111,
      left: 20,
      backgroundColor:COLORS.Red,
      shadowColor: 'rgba(255,71,71,0.57)',
      shadowOffset: {
        width:0,
        height:8
      },
      shadowOpacity: 1,
      shadowRadius: 17,
      borderRadius:BORDER_RADIUS.items + 10,
    },
    alertBoxText: {
        fontFamily:FONTS.bold,
        fontSize: Normalize(12),
        color: COLORS.White,
        textAlign: 'right',
        paddingRight:20,
        width:'100%'
    },
    closeAlertBox: {
      alignItems:'center',
      justifyContent:'center',
      position:'absolute',
      height:40,width:40,
      left:5,
      backgroundColor:'rgba(255,255,255,.1)',
      borderRadius:20
    },
})