import React, { Component } from 'react'
import { Text, View , StyleSheet,TouchableOpacity,Image} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, BORDER_RADIUS, FONTS, Normalize, FLEX,MODAL_STYLES,ICONS_SIZE,ALERT,BASE_IMG_URL,BASE_URL, ISEXPIRED, SETTOKEN, USER_ROLE, DIMENSIONS,Sooji} from './../../../Base/functions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { isIphoneX } from 'react-native-iphone-x-helper';
import axios from 'axios'
import Modal from "react-native-modal";
export default class ReserveItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,

      selectedCarName: '',
      selectedCarId: 0,

      isVisibleResultModal: false,
      isVisibleCarModal: false,

      resultParkEvent: null
    }
  }
  toggleActions(open) {
    this.setState({isOpen:open})
    if(!open) 
      this.setState({selectedCarId:0,selectedCarName:''})
  }
  reserve = async ()=>{
    if(!this.state.selectedCarId) {
      this.props.toggleAlert(true,this.props.lang.Base.Error.chooseCar)
      return
    }
    try {
      this.props.toggleReserving()
      const token = await AsyncStorage.getItem('token');
      if(!token) {this.props.setCurrentBase("auth");return;}
      const options = {
          method: 'POST',
          url: `${BASE_URL}reserve/`,
          headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
          data: {
            park_event: {
                park_slot: {id: this.props.parkSlotId},
                car_id: this.state.selectedCarId,
                start_datetime: this.props.startTime,
                end_datetime: this.props.endTime
            }
        }
      };
      axios(options)
          .then(r=>{
              SETTOKEN(r.data.token);
              this.setState({resultParkEvent: r.data.park_event});
          })
          .then(r=>{
            this.props.toggleReserving()
            this.setState({isVisibleResultModal:true});
          })
          .catch(e=>{
            console.log(e);
            this.props.toggleReserving()
              if(!e.response) {this.props.toggleAlert(true);return;}
              if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
              if(e.response.status == 500) this.props.toggleAlert(true);
              else this.props.toggleAlert(true,e.response.data.error_message);
          })
      } catch (error) {
        console.log(error);
          this.setState({isLoadingStep1:false});
          this.props.setCurrentBase('auth');
      }
  }
  render() {
    return (
      <TouchableOpacity activeOpacity={1} onPress={()=>{!this.state.isOpen ? this.toggleActions(true) : null}} style={styles.container}>
      {/* select car modal */}
        <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleCarModal}
            onBackdropPress={() => this.setState({isVisibleCarModal: false})}
            onBackdropPress={() => this.setState({isVisibleCarModal: false})}
            backdropOpacity={.4}
            style={MODAL_STYLES.bottom.outside}
        >
            <View style={[MODAL_STYLES.bottom.inside]}>

              <View style={[{width:'100%',marginBottom:10},FLEX.RNwSbCC]}>
                <TouchableOpacity onPress={()=>this.setState({isVisibleCarModal:false})} activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={MODAL_STYLES.titleBox}><Text style={MODAL_STYLES.titleText}>
                {
                  this.props.lang.Home.NewReserve.chooseCar
                }
                </Text></View>
              </View>
            
                <View style={{paddingBottom:25}}>
                  {this.props.cars.map(
                      (v,k)=>
                      <TouchableOpacity key={k} onPress={()=>this.setState({selectedCarName: v.given_name,selectedCarId:v.id,isVisibleCarModal:false})} activeOpacity={.85} style={[FLEX.RNwSbCS,styles.carItemBox]}>
                        <Image source={{uri:`${BASE_IMG_URL}${v.icon.icon.url}`}} style={styles.carImage} resizeMode={"contain"}/>
                        <Text style={styles.carName}>
                          {v.given_name}
                        </Text>
                      </TouchableOpacity>
                  )}
                </View>
            </View>
        </Modal>
      {/* reserve result modal */}
      <Modal
            backdropColor={'rgba(140,140,140,.75)'}
            animationInTiming={400}
            animationOutTiming={400}
            isVisible={this.state.isVisibleResultModal}
            onBackdropPress={() => {this.setState({isVisibleResultModal: false});this.props.setCurrentBase('dashboard');}}
            onBackdropPress={() => {this.setState({isVisibleResultModal: false});this.props.setCurrentBase('dashboard');}}
            backdropOpacity={.4}
            style={MODAL_STYLES.bottom.outside}
        >
            <View style={[MODAL_STYLES.bottom.inside]}>

              <View style={[{width:'100%',marginBottom:10},FLEX.RNwSbCC]}>
                <TouchableOpacity onPress={() => {this.setState({isVisibleResultModal: false});this.props.setCurrentBase('dashboard');}} activeOpacity={.85} style={MODAL_STYLES.closeBox}>
                  <Sooji name="times" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE - 1)}/>
                </TouchableOpacity>
                <View style={[MODAL_STYLES.titleBox,FLEX.RNwFeCS]}>
                  <Text style={MODAL_STYLES.titleText}>{this.props.lang.Home.NewReserve.success}</Text>
                  <Sooji name="tick" color={COLORS.Green} style={{paddingLeft:4}} size={Normalize(ICONS_SIZE + 3)}/>
                </View>
              </View>

              {this.state.resultParkEvent ?
              <View style={{backgroundColor:COLORS.White,borderRadius:BORDER_RADIUS.items,padding:25,marginBottom:24}}>

                <View style={[FLEX.RrWFsFsFs,styles.infoBox,{paddingTop:0}]}>
                  <Sooji name="parking" color={COLORS.LighterGrey} size={Normalize(ICONS_SIZE)}/>
                  <Text style={[{fontFamily:FONTS.bold,fontSize: Normalize(13)},styles.infoText]}>
                    {this.state.resultParkEvent.park_slot.place_title}
                  </Text>
                </View>

                <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
                  <Sooji name="carpark" color={COLORS.LighterGrey} size={Normalize(ICONS_SIZE)}/>
                  <Text style={[{fontFamily:FONTS.secondFontFamily,fontSize: Normalize(15)},styles.infoText]}>
                  {this.state.resultParkEvent.park_slot.title}
                  </Text>
                </View>

                <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
                  <Sooji name="calendar" color={COLORS.LighterGrey} size={Normalize(ICONS_SIZE)}/>
                  <Text style={[{fontFamily:FONTS.bold,fontSize: Normalize(13)},styles.infoText]}>
                  {`${this.state.resultParkEvent.start_time.day_name} ${this.state.resultParkEvent.start_time.year}/${this.state.resultParkEvent.start_time.month}/${this.state.resultParkEvent.start_time.day}`}
                  </Text>
                </View>

                <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
                  <Sooji name="clock" solid={true} color={COLORS.LighterGrey} size={Normalize(ICONS_SIZE)}/>
                  <Text style={[{fontFamily:FONTS.bold,fontSize: Normalize(13)},styles.infoText]}>
                  {this.state.resultParkEvent.start_time ?
                  `از ${this.state.resultParkEvent.start_time.hour}:${this.state.resultParkEvent.start_time.minute == 0 ? '00' : this.state.resultParkEvent.start_time.minute}` : ""}
                  {this.props.endTime ?
                  ` تا ${this.state.resultParkEvent.end_time.hour}:${this.state.resultParkEvent.end_time.minute == 0 ? '00' : this.state.resultParkEvent.end_time.minute}` : ""}
                  </Text>
                </View>

                <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
                  <Sooji name="info" solid={true} color={COLORS.LighterGrey} size={Normalize(ICONS_SIZE)}/>
                  <Text style={[{fontFamily:FONTS.bold,fontSize: Normalize(13)},styles.infoText]}>
                    {this.state.resultParkEvent.parking_type.description}
                  </Text>
                </View>

                <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
                  <Sooji name="dollar" solid={true} color={COLORS.LighterGrey} size={Normalize(ICONS_SIZE)}/>
                  <Text style={[{fontFamily:FONTS.extraBold,fontSize: Normalize(14)},styles.infoText]}>
                    {`${this.state.resultParkEvent.pre_price}`}
                  </Text>
                </View>
              
              </View> : null }
              
            </View>
        </Modal>

        <View style={[styles.flagBox,FLEX.RNwSbCC]}>
          {/* <Text style={styles.flagBoxTextCurrency}>{this.props.flagCurrency}</Text> */}
          <Text style={styles.flagBoxTextTitle}>{this.props.flagTitle}</Text>
        </View>
        
        <TouchableOpacity onPress={()=>{
          this.toggleActions(!this.state.isOpen)
          }} style={[styles.arrowBox,{backgroundColor:this.state.isOpen ? COLORS.White : 'transparent'}]}>
        {
          !this.state.isOpen ?
          <Sooji name="top" style={{transform:[{rotate:'180deg'}]}} color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE-5)}/> :
          <Sooji name="top" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE-5)}/>
        }
        </TouchableOpacity>
       
        <View>
          {
            this.props.parkingName ?
            <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
              <Sooji name="parking" color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE)}/>
              <Text style={[{fontFamily:FONTS.secondFontFamily,fontSize: Normalize(14)},styles.infoText]}>
                {this.props.parkingName}
              </Text>
            </View>:null
          }
          {
            this.props.startTime || this.props.endTime ?
            <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
              <Sooji name="clock" solid={true} color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE)}/>
              <Text style={[{
                fontFamily:FONTS.bold,
                fontSize: Normalize(14),
                color:!this.props.isYellow ? COLORS.DarkBlue : 'rgba(252,132,84,1)',
                paddingTop:2,
                paddingRight:10}]}>
              {this.props.startTime ?
                `از ${this.props.startTime.hour}:${this.props.startTime.minute == 0 ? '00' : this.props.startTime.minute}` : ""}
                {this.props.endTime.hour != -1 ?
                ` تا ${this.props.endTime.hour}:${this.props.endTime.minute == 0 ? '00' : this.props.endTime.minute}` : ""}
              </Text>
            </View>
            :null
          }
          {this.props.desc ?
            <View style={[FLEX.RrWFsFsFs,styles.infoBox]}>
            <Sooji name="info" style={{right:1}} solid={true} color={COLORS.DarkBlue} size={Normalize(ICONS_SIZE)}/>
            <Text style={[{fontFamily:FONTS.light,fontSize:Normalize(12)},styles.infoText]}>
              {this.props.desc}
            </Text>
          </View> : null}
        </View>
      
        {/* action bar */}
        {
          this.state.isOpen ?
          <View>
            <TouchableOpacity onPress={()=>this.setState({isVisibleCarModal:true})} style={styles.whiteTouchBox}>
              <Text style={styles.carTitle}>
                {this.state.selectedCarId == 0 ? this.props.lang.Home.NewReserve.chooseCar : this.state.selectedCarName}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{
              this.reserve();
              }} style={styles.submitButton}>
              <Text style={styles.submitText}>{this.props.lang.Home.NewReserve.confrim}</Text>
            </TouchableOpacity>
          </View>
          :null
        }
        
      </TouchableOpacity>
    )
  }
}
const styles = StyleSheet.create({
    container: {
        width:'100%',
        height: 'auto',
        minHeight: 80,
        position:'relative',
        overflow: 'hidden',
        paddingHorizontal: 15,
        paddingBottom: 15,
        paddingTop:10,
        backgroundColor: COLORS.LightGrey,
        borderRadius: BORDER_RADIUS.items,
        marginTop:15
    },
    flagBox: {
      position:'absolute',
      left: 0,
      top: 0,
      backgroundColor: COLORS.DarkBlue,
      borderBottomRightRadius: 25,
      paddingVertical: 12,
      paddingHorizontal: 12,
    },
    flagBoxTextTitle: {
      color: COLORS.White,
      fontSize: Normalize(14),
      fontFamily: FONTS.extraBold,
      paddingLeft:5
    },
    flagBoxTextCurrency: {
      color: COLORS.White,
      fontSize: Normalize(9),
      fontFamily: FONTS.medium,
    },
    infoText: {
      color: COLORS.DarkBlue,
      paddingTop:2,
      paddingRight:10
    },
    infoBox: {
      paddingTop:8,
    },
    arrowBox: {
      zIndex:1111111,
      alignItems: 'center',
      justifyContent: 'center',
      width: 44,
      height: 44,
      position:'absolute',
      borderRadius:20,
      left: 15,
      bottom: 15
    },
    carTitle: {
      color: COLORS.DarkBlue,
      fontSize: Normalize(11),
      textAlign:'center',
      fontFamily: FONTS.bold,
    },
    whiteTouchBox: {
      backgroundColor:COLORS.White,
      paddingVertical: 15,
      borderRadius:BORDER_RADIUS.items,
      marginTop: 15
    },
    submitButton: {
      width: DIMENSIONS.width - 130,
      alignItems:'center',
      justifyContent:'center',
      height: 44,
      marginLeft:'auto',
      backgroundColor:COLORS.Green,
      borderRadius:BORDER_RADIUS.items,
      marginTop: 12
    },
    submitText: {
      color: COLORS.White,
      fontSize: Normalize(12),
      textAlign:'center',
      fontFamily: FONTS.extraBold
    },
    carItemBox: {
      backgroundColor:COLORS.White,borderRadius:BORDER_RADIUS.items,paddingVertical:20,paddingHorizontal:15,marginTop:9
    },
    carName: {
      fontFamily:FONTS.medium,fontSize:Normalize(13),color:COLORS.DarkBlue,textAlign:'right'
    },
    carImage: {
      width:25,height:25
    }
})