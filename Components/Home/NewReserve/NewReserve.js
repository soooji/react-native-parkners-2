import React, { Component } from 'react'
import { StyleSheet ,Text, View, ScrollView,Image,TouchableHighlight,TouchableOpacity,Animated,Easing,Platform } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, Normalize, FLEX, FONTS, BORDER_RADIUS, DIMENSIONS, ALERT,USER_ROLE,BASE_URL,SETTOKEN,BASE_IMG_URL,ISEXPIRED,Sooji } from '../../../Base/functions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import DateTimePicker from 'react-native-modal-datetime-picker';
import FullScreenLoading from './../../Base/FullScreenLoading';
import SearchResult from './Result'
import axios from 'axios'
let now = new Date()
export default class NewReserve extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading:false,
      loadingText: '',
      isLoadingStep1: false,
      isLoadingStep2: false,
      isSearching: false,
      baseStep: 'search', //search or result
      currentStep: 1, //1,2,3
      places:[],
      possible_park_events:[],
      days:[],
      tips:[],
      //animations
      prevStateButtonAnime: new Animated.Value(0),
      alertAnime: new Animated.Value(-90),
      alertText:'',

      now: new Date(),

      //selected parking
      placeId: 0,
      placeName: '',

      //selected day
      selectedDateObject:{},
      //time picker
      isTimePickerVisible: false,
      isStartTime: true,
      startTimeHours: null,
      startTimeMiuntes: null,
      endTimeHours: null,
      endTimeMiuntes: null,
      // startTimeHours: new Date().getHours(), //shoud be null
      // startTimeMiuntes: new Date().getMinutes(), //shoud be null
      // endTimeHours: now.getHours() < 21 ? now.getHours()+2 : 0, //shoud be null
      // endTimeMiuntes: now.getMinutes(), //shoud be null

      //layouts
      firstStepHeight:0,
      secondStepHeight:0,
      thirdStepHeight:0
    }
  }
  //Main methods
  componentDidMount() {
    this.getPlaces();
    this.setState({loadingText:this.props.lang.Base.Loading.wait,alertText:this.props.lang.Base.Error.simple});
  }
  getPlaces = async () => {
    try {
        this.setState({isLoadingStep1:true});
        const token = await AsyncStorage.getItem('token');
        if(!token) {this.props.setCurrentBase("auth");return;}
        const options = {
            method: 'POST',
            url: `${BASE_URL}reserve_cycle/0/`,
            headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
        };
    axios(options)
        .then(r=>{
            SETTOKEN(r.data.token);
            this.setState({places:r.data.places});
        })
        .then(r=>{
            this.setState({isLoadingStep1:false});
        })
        .catch(e=>{
          console.log(e);
            this.setState({isLoadingStep1:false});
            if(!e.response) {this.toggleAlert(true);return;}
            if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
            if(e.response.status == 500) this.toggleAlert(true);
            else this.toggleAlert(true,e.response.data.error_message);
        })
    } catch (error) {
      console.log(error);
        this.setState({isLoadingStep1:false});
        this.props.setCurrentBase('auth');
    }
  }
  getDays = async (id,name) => {
    try {
        this.setState({isLoadingStep2:true,currentStep:2,placeId:id,placeName:name,isLoading:true});
        this.scrollTo(this.state.secondStepHeight);
        this.openPrevStateButton(true);
        const token = await AsyncStorage.getItem('token');
        if(!token) {this.props.setCurrentBase("auth");return;}
        const options = {
            method: 'POST',
            url: `${BASE_URL}reserve_cycle/1/`,
            headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
            data: {
              place_id: id
            }
        };
    axios(options)
        .then(r=>{
            SETTOKEN(r.data.token);
            this.setState({days:r.data.available_days});
        })
        .then(r=>{
            this.setState({isLoadingStep2:false,currentStep:2,placeId:id,placeName:name,isLoading:false});
            this.scrollTo(this.state.secondStepHeight);
        })
        .catch(e=>{
            console.log(e);
            this.setState({isLoadingStep2:false,isLoading:false});
            if(!e.response) {this.toggleAlert(true);return;}
            if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
            if(e.response.status == 500) this.toggleAlert(true);
            else this.toggleAlert(true,e.response.data.error_message);
        })
    } catch (error) {
        console.log(error);
        this.setState({isLoadingStep2:false,isLoading:false});
        this.props.setCurrentBase('auth');
    }
  }
  getTimes = async (selectedDateObject) => {
    try {
        this.setState({isLoadingStep3:true,isLoading:true,selectedDateObject: selectedDateObject});
        const token = await AsyncStorage.getItem('token');
        if(!token) {this.props.setCurrentBase("auth");return;}
        const options = {
            method: 'POST',
            url: `${BASE_URL}reserve_cycle/2/`,
            headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
            data: {
              place_id: this.state.placeId,
              date: selectedDateObject
            }
        };
    axios(options)
        .then(r=>{
            SETTOKEN(r.data.token);
            this.setState({tips:r.data.descriptions});
        })
        .then(r=>{
          this.setState({currentStep:3,isLoadingStep3:false,isLoading:false});
          this.scrollTo(this.state.thirdStepHeight);
        })
        .catch(e=>{
            console.log(e);
            this.setState({isLoadingStep3:false,isLoading:false});
            if(!e.response) {this.toggleAlert(true);return;}
            if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
            if(e.response.status == 500) this.toggleAlert(true);
            else this.toggleAlert(true,e.response.data.error_message);
        })
    } catch (error) {
        console.log(error);
        this.setState({isLoadingStep3:false,isLoading:false});
        this.props.setCurrentBase('auth');
    }
  }
  search = async () => {
    try {
        this.setState({isLoading:true,loadingText:this.props.lang.Base.Loading.searchingParking});
        const token = await AsyncStorage.getItem('token');
        if(!token) {this.props.setCurrentBase("auth");return;}
        const options = {
            method: 'POST',
            url: `${BASE_URL}reserve_list/`,
            headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
            data: {
              place_id:this.state.placeId,
              start_datetime: 
              !this.state.startTimeHours || !this.state.startTimeMiuntes ?
              {
                day:this.state.selectedDateObject.day,
                month: this.state.selectedDateObject.month,
                year: this.state.selectedDateObject.year,
                hour: -1,
                minute: 0,
                microsecond: 0,
                second: 0,
              } :
              {
                day:this.state.selectedDateObject.day,
                hour: parseInt(this.state.startTimeHours),
                minute: parseInt(this.state.startTimeMiuntes),
                month: this.state.selectedDateObject.month,
                year: this.state.selectedDateObject.year,
                microsecond: 0,
                second: 0,
              },
              end_datetime : 
              !this.state.endTimeHours || !this.state.endTimeMiuntes ? 
              {
                day:this.state.selectedDateObject.day,
                hour: -1,
                minute: 0,
                month: this.state.selectedDateObject.month,
                year: this.state.selectedDateObject.year,
                microsecond: 0,
                second: 0,
              } :
              {
                day:this.state.selectedDateObject.day,
                hour: parseInt(this.state.endTimeHours),
                minute: parseInt(this.state.endTimeMiuntes),
                month: this.state.selectedDateObject.month,
                year: this.state.selectedDateObject.year,
                microsecond: 0,
                second: 0,
              }
            }
        };
    axios(options)
        .then(r=>{
            SETTOKEN(r.data.token);
            this.setState({possible_park_events:r.data.possible_park_events});
        })
        .then(r=>{
          this.setState({isLoading:false,baseStep:'result'});
        })
        .then(e=>{
          this.setState({loadingText:this.props.lang.Base.Loading.wait});
        })
        .catch(e=>{
            console.log(e);
            this.setState({isLoading:false});
            this.setState({loadingText:this.props.lang.Base.Loading.wait});
            if(!e.response) {this.toggleAlert(true);return;}
            if(ISEXPIRED(e)) {this.props.setCurrentBase('auth');return;}
            if(e.response.status == 500) this.toggleAlert(true);
            else this.toggleAlert(true,e.response.data.error_message);
        })
    } catch (error) {
        console.log(error);
        this.setState({isLoading:false});
        this.setState({loadingText:this.props.lang.Base.Loading.wait});
        this.props.setCurrentBase('auth');
    }
  }
  render() {
    if(this.state.baseStep == "search") {
      return (
        <View style={{position:'relative'}}>
  
        <FullScreenLoading isLoading={this.state.isLoading} text={this.state.loadingText}/>
  
        <View style={[ALERT.topFixBar,{height:50}]}>

          <Animated.View style={[FLEX.RNwSbCC,ALERT.alertBox,{
            transform: [{translateY:this.state.alertAnime}],height:50}]}>
            <Text style={ALERT.alertBoxText}>{this.state.alertText}</Text>
          </Animated.View>

          <TouchableOpacity onPress={()=>this.prevState()} activeOpacity={.85}>
            <Animated.View style={[this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC,styles.prevStepButton,{height:this.state.prevStateButtonAnime}]}>
              <Sooji name="top" color={COLORS.Black} size={Normalize(10)}/>
              <Text style={styles.prevStepButtonText}>{this.props.lang.Home.NewReserve.previous}</Text>
            </Animated.View>
          </TouchableOpacity>
        </View>
  
        <ScrollView ref={'_scrollview'} scrollEnabled={false} style={styles.container} showsVerticalScrollIndicator={false}>
  
          <View 
          ref={"firstStepComponent"}
          style={{
            opacity:this.state.currentStep == 1 ? 1 : .5}} onLayout={(l)=>{this.setState({firstStepHeight:l.nativeEvent.layout.y})}}>
            <PartTitle isRTL={this.props.isRTL} title={this.props.lang.Home.NewReserve.chooseParking} iconName="parking"/>
            {!this.state.isLoadingStep1 ?
            <View>

              {this.state.places.map(
                  (v,k)=>
                  <TouchableOpacity
                  key={k}
                  onPress={()=>{
                    if(v.is_available) {
                      this.getDays(v.id,v.title);
                    }
                  }}
              activeOpacity={.5} style={[styles.placeBox,{opacity:v.is_available ? 1 : .5}]}>
              <View style={[this.props.isRTL ? FLEX.RNwSbCS : FLEX.RrNwSbCS]}>
                <Image source={{uri:`${BASE_IMG_URL}${v.icon.url}`}} style={styles.placeImage} resizeMode="contain"/>
                <Text style={styles.placeTitle}>{v.title}</Text>
              </View>
            </TouchableOpacity>
                )
              }
              
            
            </View> :
            <View>
              <PlaceholderPlace delay={500}/>
              <PlaceholderPlace delay={1000}/>
              <PlaceholderPlace delay={1500}/>
            </View>
            }
  
          </View>
          <View 
          ref={"secondStepComponent"}
          style={{
            paddingTop:45,
            opacity:this.state.currentStep == 2 ? 1 : .5}} onLayout={(l)=>{this.setState({secondStepHeight:l.nativeEvent.layout.y})}}>
            <PartTitle isRTL={this.props.isRTL} title={this.props.lang.Home.NewReserve.chooseDay} iconName="calendar" style={{marginTop:20}}/>
            {this.state.currentStep != 1 && !this.state.isLoadingStep2 ?
              <View style={[styles.dates,this.props.isRTL ? FLEX.RrWFsFsFs : FLEX.RWFsFsFs]}>
          {
            this.state.days.map(
              (v,k)=>
              <TouchableOpacity 
                key={k}
                onPress={()=>{this.getTimes(v);}}
                activeOpacity={.5} style={[styles.dateItem,{backgroundColor:COLORS.LightGrey}]}>
                <View style={[FLEX.CWCC]}>
                  <Text style={[styles.dateItemWeekDay,{color: COLORS.DarkBlue}]}>{v.day_name}</Text>
                  <Text style={[styles.dateItemDate,{color: COLORS.DarkBlue}]}>{v.year}/{v.month}/{v.day}</Text>
                </View>
              </TouchableOpacity>
            )
          }
  
            </View>
          :
          <PlaceholderDates isLoading={this.state.isLoadingStep2}/>
            }
          </View>
  
          <View
          ref={"thirdStepComponent"}
          style={{
            paddingTop:45,
            position:'relative',
            paddingBottom:DIMENSIONS.height,
            opacity:this.state.currentStep == 3 ? 1 : .5}} onLayout={(l)=>{this.setState({thirdStepHeight:l.nativeEvent.layout.y})}}>
            <PartTitle isRTL={this.props.isRTL} title={this.props.lang.Home.NewReserve.chooseTime} iconName="clock" style={{marginTop:20}}/>
  
            <TouchableOpacity onPress={()=>{this.state.currentStep == 3 ? this.setState({isStartTime: true,isTimePickerVisible:true}): null}} activeOpacity={.85} style={[styles.timeBox]}>
              <View style={[this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
                <View style={styles.timeBoxInnerBox}>
                  <Text style={styles.timeBoxInnerText}>
                  {
                    !this.state.startTimeHours || !this.state.startTimeMiuntes ? this.props.lang.Home.NewReserve.noStart
                  : 
                  `${this.state.startTimeHours}:${this.state.startTimeMiuntes}`}
                  </Text>
                </View>
                <Text style={styles.timeBoxTitle}>{this.props.lang.Home.NewReserve.enterTime}</Text>
              </View>
            </TouchableOpacity>
  
            <TouchableOpacity onPress={()=>{this.state.currentStep == 3 ? this.setState({isStartTime: false,isTimePickerVisible:true}) : null}} activeOpacity={.85} style={[styles.timeBox]}>
              <View style={[this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC]}>
                <View style={styles.timeBoxInnerBox}>
                    <Text style={styles.timeBoxInnerText}>
                    {
                      !this.state.endTimeHours || !this.state.endTimeMiuntes ? this.props.lang.Home.NewReserve.noEnd
                      : 
                    `${this.state.endTimeHours}:${this.state.endTimeMiuntes}`}
                    </Text>
                  </View>
                <Text style={styles.timeBoxTitle}>{this.props.lang.Home.NewReserve.quitTime}</Text>
              </View>
            </TouchableOpacity>
            <DateTimePicker
              is24Hour={true}
              titleIOS={
                `${this.state.isStartTime ? this.props.lang.Home.NewReserve.enter : this.props.lang.Home.NewReserve.quit}`
              }
              titleStyle={{fontFamily:FONTS.bold,fontSize:Normalize(13),paddingBottom:10,paddingTop:10,color:COLORS.DarkBlue}}
              minuteInterval={15}
              confirmTextIOS={this.props.lang.Home.NewReserve.confirm}
              confirmTextStyle={{fontFamily:FONTS.bold,fontSize:Normalize(16),paddingBottom:10,paddingTop:10}}
              cancelTextIOS={
                this.state.isStartTime ?
                this.props.lang.Home.NewReserve.noStart :
                this.props.lang.Home.NewReserve.noEnd
              }
              cancelTextStyle={{fontFamily:FONTS.light,fontSize:Normalize(15),paddingBottom:10,paddingTop:10}}
              isVisible={this.state.isTimePickerVisible}
              onConfirm={(time)=>{
                let t = time.toTimeString();
                this.state.isStartTime ?
                this.setState({startTimeHours:t.slice(0,2),startTimeMiuntes:t.slice(3,5),isTimePickerVisible:false}) :
                this.setState({endTimeHours:t.slice(0,2),endTimeMiuntes:t.slice(3,5),isTimePickerVisible:false})
              }}
              mode={'time'}
              onCancel={()=>{
                this.state.isStartTime ?
                this.setState({startTimeHours:null,startTimeMiuntes:null,isTimePickerVisible:false}) :
                this.setState({endTimeHours:null,endTimeMiuntes:null,isTimePickerVisible:false})
                }
              }
            />
            <TouchableHighlight 
            onPress={()=>{this.search();}}
            activeOpacity={.85} style={styles.submitButton}>
              <Text style={styles.submitButtonText}>{this.props.lang.Home.NewReserve.search}</Text>
            </TouchableHighlight>
            {
              this.state.tips.length !=0 ?
              <View style={[styles.pointsBox]}>
              {this.state.tips.map(
                  (v,k)=>
                  <View key={k} style={[styles.pointBox,this.props.isRTL ? FLEX.RNwFeCS : FLEX.RrNwFeCS]}>
                    <Text key={k} style={styles.pointItemText}>
                      {v}
                    </Text>
                    <Icon name={"circle"} solid={true} color={COLORS.DarkBlue} style={{paddingLeft:10}} size={Normalize(4)}/>
                  </View>
                )}
            </View>
            :null}

          </View>
        </ScrollView>
        
        </View>
      )
    } else {
      return(
        <SearchResult
        lang={this.props.lang}
        isRTL={this.props.isRTL}
        backToSearch={()=>this.backToSearch().then(()=>{
          this.scrollTo(0);
          this.getPlaces();
          this.openPrevStateButton(false);
          this.setState({currentStep:1});
        })} possible_park_events={this.state.possible_park_events} setCurrentBase={(name)=>this.props.setCurrentBase(name)} placeName={this.state.placeName} placeId={this.state.placeId} fullDate={`${this.state.selectedDateObject.day_name} ${this.state.selectedDateObject.day} ${this.state.selectedDateObject.month_name}`}/>
      )
    }
  }

  backToSearch = async ()=> {
    await this.setState({baseStep: 'search'});
  }
  //Appearancial methods
  scrollTo = (y) => {
    this.refs._scrollview.scrollTo({x: 0, y: y, animated: true});
  }
  prevState = ()=> {
    switch (this.state.currentStep) {
      case 2:
        this.scrollTo(0);
        this.getPlaces();
        this.openPrevStateButton(false);
        this.setState({currentStep:1});
        break;
      case 3 :
        this.getDays(this.state.placeId,this.state.placeName);
        this.scrollTo(this.state.secondStepHeight);
        this.setState({currentStep:2,startTimeHours:'',endTimeHours:'',startTimeMiuntes:'',endTimeMiuntes:'',tips:[]});
        break;
      default:
        break;
    }
  }
  toggleAlert = (open,text = this.props.lang.Base.Error.simple) => {
    if(open) {
        this.setState({alertText: text})
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: 20,
          easing: Easing.elastic(1.2),
          duration: 1000,
          useNativeDriver:true
        },
      ).start();
      setTimeout(()=>{
        this.toggleAlert(false);
      },3000)
    } else {
      setTimeout(() => {
        this.setState({alertText: ''})
      }, 500);
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: -90,
          easing: Easing.ease,
          duration: 500,
          useNativeDriver:true
        },
      ).start();
    }
  }
  openPrevStateButton = (open)=>{
    if(open) {
      Animated.timing(
        this.state.prevStateButtonAnime,
        {
          toValue: 50,
          easing: Easing.linear,
          duration: 500,
          useNativeDriver:false
        },
      ).start();
    } else {
      Animated.timing(
        this.state.prevStateButtonAnime,
        {
          toValue: 0,
          easing: Easing.linear,
          duration: 500,
          useNativeDriver:false
        },
      ).start();
    }
  }
}

class PartTitle extends Component {
  render() {
    return (
      <View style={[this.props.isRTL ? FLEX.RNwFeCS : FLEX.RrNwFeCS,styles.partTitleBox,this.props.style]}>
        <Text style={styles.partTitleText}>{this.props.title}</Text>
        <Sooji name={this.props.iconName} color={COLORS.DarkBlue} size={Normalize(17)}/>
      </View>
    )
  }
}




// PLACEHOLDERS
class PlaceholderPlace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      opacityAnime: new Animated.Value(0.3),
    }
  }
  componentDidMount() {
    setTimeout(()=>{
      Animated.loop(
        Animated.sequence([
          Animated.timing(this.state.opacityAnime, {
            toValue: 1,
            duration: 700,
            delay: 500
          }),
          Animated.timing(this.state.opacityAnime, {
            toValue: 0.3,
            duration: 700
          })
        ]),
        {iterations: -1}
      ).start()
    },this.props.delay ? this.props.delay : 0);
  }
  render() {
    return (
        <Animated.View style={[styles.placeBox,{opacity:this.state.opacityAnime}]}>
              <View style={[FLEX.RNwSbCS]}>
                <Image source={require('./../../../Static/fum-logo.png')} style={[styles.placeImage,{opacity:0}]} resizeMode="contain"/>
                <Text style={[styles.placeTitle,{color:COLORS.LightGrey}]}></Text>
              </View>
        </Animated.View>
    )
  }
}
class PlaceholderDates extends Component {
  constructor(props) {
    super(props);
    this.state = {
      count: [0,1,2,3,4,5],
      opacityAnime: new Animated.Value(1),
    }
  }
  componentDidMount() {
    this.props.isLoading ? this.loading().start() : this.loading().stop()
  }
  componentWillReceiveProps(prop) {
    prop.isLoading ? this.loading().start() : this.loading().stop()
  }
  loading() {
    return Animated.loop(
      Animated.sequence([
        Animated.timing(this.state.opacityAnime, {
          toValue: 0.3,
          duration: 1000,
          delay: 500
        }),
        Animated.timing(this.state.opacityAnime, {
          toValue: 1,
          duration: 1000
        })
      ]),
      {
        iterations: -1
      }
    )
  }
  render() {
    return (
      <View style={[styles.dates,FLEX.RWFsFsFs]}>
      {this.state.count.map((v,k)=><Animated.View key={k} style={[styles.dateItem,{backgroundColor:COLORS.LightGrey,opacity: this.state.opacityAnime}]}><View style={[FLEX.CWCC]}><Text style={[styles.dateItemWeekDay,{color: COLORS.LightGrey}]}>شنبه</Text><Text style={[styles.dateItemDate,{color: COLORS.LightGrey}]}>98/02/18</Text></View></Animated.View>)}
    </View>
    )
  }
}
// END PLACEHOLDERS


const styles = StyleSheet.create({
  pointBox: {
    padding:5,
  },
  pointItemText: {
    fontFamily:FONTS.bold,
    fontSize: Normalize(10),
    color: 'rgba(0,0,0,.7)',
    textAlign: 'right',
    paddingLeft:5
  },
  pointsBox: {
    backgroundColor:COLORS.LightGrey,
    borderRadius:BORDER_RADIUS.items,
    marginTop:10,
    padding: Platform.OS == "ios" ? 15 : 7,
    minHeight:20,
    width:'100%',
  },
  container: {
    width:'100%',
    height: '100%',
    paddingHorizontal: 18
  },
    prevStepButton : {
      overflow: 'hidden',
      backgroundColor:COLORS.LightGrey,
      borderRadius:BORDER_RADIUS.items + 10,
      paddingHorizontal:20,
    },
    prevStepButtonText: {
        fontFamily:FONTS.bold,
        fontSize: Normalize(12),
        color: COLORS.Black,
        textAlign: 'right',
    },
    //part item
    partTitleBox: {
      width:'100%',
      paddingRight:15,
      paddingVertical: 25,
    },
    partTitleText :{
        fontFamily:FONTS.extraBold,
        fontSize: Normalize(12),
        color: COLORS.DarkBlue,
        textAlign: 'right',
        paddingTop:3,
        paddingRight:10,
        paddingLeft:10
    },
    //place
    placeBox: {
      backgroundColor:COLORS.LightGrey,
      borderRadius: BORDER_RADIUS.items,
      paddingVertical:13,
      paddingHorizontal:15,
      marginBottom:13
    },
    placeImage: {
      height: 45,
      width: 45,
      borderRadius:6
    },
    placeTitle: {
      fontFamily:FONTS.bold,
      fontSize: Normalize(13),
      color: COLORS.DarkBlue,
      textAlign: 'right',
      paddingRight:10
    },
    //next step button
    nextStepButton : {
      backgroundColor:COLORS.Black,
      borderRadius:BORDER_RADIUS.items,
      paddingVertical:16,
      paddingHorizontal:20
    },
    nextStepButtonText: {
        fontFamily:FONTS.bold,
        fontSize: Normalize(12),
        color: COLORS.White,
        textAlign: 'right',
    },
    // dates
    dates: {
      paddingBottom:10,
      // direction:'rtl',
    },
    dateItem : {      
      borderRadius:BORDER_RADIUS.items,
      paddingVertical:24,
      width: DIMENSIONS.width/3 - 22,
      paddingHorizontal:10,
      marginBottom:11,
      marginLeft:9
    },
    dateItemWeekDay: {
      fontFamily:FONTS.bold,
      fontSize: Normalize(12),
      textAlign: 'center',
    },
    dateItemDate: {
      fontFamily:FONTS.light,
      fontSize: Normalize(12),
      textAlign: 'center',
      paddingTop:13
    },
    // times
    timeBox: {
      backgroundColor:COLORS.LightGrey,
      borderRadius:BORDER_RADIUS.items,
      padding: 7,
      marginBottom:10
    },
    timeBoxTitle : {
      fontFamily:FONTS.bold,
      fontSize: Normalize(12),
      color: COLORS.DarkBlue,
      textAlign: 'right',
      paddingRight:15,
      paddingLeft:10
    },
    timeBoxInnerBox: {
      backgroundColor: COLORS.Black,
      borderRadius: BORDER_RADIUS.items - 2,
      paddingVertical:Platform.OS == "ios" ? 14 :8,
      paddingHorizontal:17,
    },
    timeBoxInnerText : {
      fontFamily:FONTS.bold,
      fontSize: Normalize(12),
      color: COLORS.White,
      textAlign: 'center',
    },
    submitButton : {
      backgroundColor: COLORS.Green,
      paddingVertical:Platform.OS == "ios" ? 18 : 13,
      borderRadius: BORDER_RADIUS.items,
      width:'100%',
      marginTop:0,
    },
    submitButtonText: {
      fontFamily:FONTS.bold,
      fontSize: Normalize(13),
      color: COLORS.White,
      textAlign: 'center',
    }
})


{/* <NextStep onPress={()=>alert('hi')}/> */}

// class NextStep extends Component {
//   render() {
//     return (
//       <TouchableOpacity onPress={this.props.onPress} activeOpacity={.85} style={[styles.nextStepButton,this.props.style]}>
//         <View style={[FLEX.RNwSbCS]}>
//           <Icon name="chevron-left" color={COLORS.White} size={Normalize(15)}/>
//           <Text style={styles.nextStepButtonText}>مرحله بعد</Text>
//         </View>
//       </TouchableOpacity>
//     )
//   }
// }