import React, { Component } from 'react'
import { Text, View,Animated,Image,Easing } from 'react-native'
import { DIMENSIONS, FLEX, FONTS, Normalize, COLORS } from '../../Base/functions';
var Spinner = require('react-native-spinkit');
export default class FullScreenLoading extends Component {
    constructor(props) {
        super(props);
        this.state = {
            spinValue: new Animated.Value(0),
            opacityAnime: new Animated.Value(0),

            isMounted: true
        }
    }
    componentDidMount() {
    }
    componentWillReceiveProps(props) {
        if(props.isLoading) {
            this.setState({isMounted:false})
            Animated.loop(
                Animated.sequence([
                Animated.timing(
                    this.state.spinValue,{
                    toValue: 1,
                    duration: 1500,
                    easing: Easing.linear
                }),
            ]),{iterations: -1}
            ).start();
            Animated.timing(
                this.state.opacityAnime,{
                toValue: 1,
                duration: 700,
                easing: Easing.ease
            }).start();
        } else {
            Animated.timing(
                this.state.opacityAnime,{
                toValue: 0,
                duration: 400,
                easing: Easing.ease
            }).start()
            setTimeout(()=>this.setState({isMounted:true}),400)
        }
        // isMounted
    }
  render() {
      const spin =
        this.state.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '360deg']
        })
        if(!this.state.isMounted) {
            return (
            <Animated.View style={[FLEX.CWCC,{opacity:this.state.opacityAnime,width:'100%',zIndex:1111111111,height: '100%',position:'absolute',backgroundColor:'rgba(255,255,255,.85)'}]}>
            {/* <Animated.Image
                style={{transform: [{rotate: spin}],width:DIMENSIONS.width/5,height:DIMENSIONS.width/5}}
                resizeMode={"contain"}
                source={require('./../../Static/spin.png')} /> */}

            <Spinner isVisible={this.state.isVisible} size={DIMENSIONS.width/5} type={'Bounce'} color={'black'}/>

            <Text style={{fontFamily:FONTS.bold,fontSize:Normalize(13),textAlign:'center',color:COLORS.DarkBlue,paddingTop:30}}>
                {this.props.text ? this.props.text : 'Loading...'}
            </Text>
          </Animated.View>)
        } else {
            return(<View style={{width:0,height:0,position:'absolute',top:100,backgroundColor:'red',zIndex:1111111111}}></View>)
        }
  }
}


// // First set up animation 
// Animated.timing(
//     this.spinValue,
//   {
//     toValue: 1,
//     duration: 3000,
//     easing: Easing.linear
//   }
// ).start()

// // Second interpolate beginning and end values (in this case 0 and 1)
