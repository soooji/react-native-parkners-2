import React, { Component } from 'react'
import { StyleSheet , View,Platform } from 'react-native'
import {isIphoneX} from 'react-native-iphone-x-helper'
import { DIMENSIONS, COLORS, BORDER_RADIUS } from '../../Base/functions';
export default class BaseFrame extends Component {
  render() {
    return (
      <View style={styles.container}>
        {this.props.children}
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        width:'100%',
        height: Platform.OS == "ios" ? isIphoneX() ? DIMENSIONS.height - 165 : DIMENSIONS.height - 125 : DIMENSIONS.height - 150,
        position: 'absolute',
        zIndex: 11111111,
        top: isIphoneX() ? 95 : 70,
        borderRadius: BORDER_RADIUS.base,
        backgroundColor : COLORS.White,
        overflow:'hidden'
    }
})