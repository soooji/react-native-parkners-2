import React, { Component } from 'react'
import {Platform,StyleSheet,Text,View,TouchableOpacity} from 'react-native'
import { DIMENSIONS, COLORS, BORDER_RADIUS, FONTS, Normalize } from '../../Base/functions';
import {isIphoneX} from 'react-native-iphone-x-helper'
export default class FooterBar extends Component {
  render() {
    return (
        <TouchableOpacity
            activeOpacity={.95}
            onPress={this.props.onPress}
            style={[styles.container,{backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : COLORS.LightGrey},this.props.style]}>
            <Text style={[styles.titleStyle,{color: this.props.textColor ? this.props.textColor : COLORS.DarkBlue}]}>
                {this.props.title}
            </Text>
        </TouchableOpacity>
    )
  }
}
const styles = StyleSheet.create({
    container: {
        width:'100%',
        height: 105,
        position: 'absolute',
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        justifyContent: 'center',
        paddingTop:Platform.OS === "ios" ? isIphoneX() ? 25 : 45 : 46,
        // TODO: Repair it for Android
        bottom: 0,
    },
    titleStyle: {
        fontFamily:FONTS.bold,
        fontSize: Normalize(12),
        textAlign: 'center',
        // paddingBottom:Platform.OS === "ios" ? isIphoneX() ? 55 : 70 : 20
    }
})