import React, { Component } from 'react'
import {StyleSheet,Text,View,TouchableOpacity,Platform,Image } from 'react-native'
import { DIMENSIONS, COLORS, BORDER_RADIUS, FONTS, Normalize , FLEX,Sooji} from '../../Base/functions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {isIphoneX} from 'react-native-iphone-x-helper'
import AutoHeightImage from 'react-native-auto-height-image';
import Communications from 'react-native-communications';
export default class HeadBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuStatus: 'close'
        }
    } 
    componentDidMount() {
        this.props.toggleTimer(false);
    }
    componentWillReceiveProps(props) {
        this.setState({menuStatus:props.menuStatus});
    }
  render() {
    return (
        <View style={[styles.container,FLEX.RNwSbFsS]}>
            <TouchableOpacity
            // onPress={()=>Communications.web('http://parkners.com/')}
            // onPress={()=>this.props.changeLanguage()}
            style={[styles.button]}>
                <Sooji name="parkners" color={COLORS.White} size={Normalize(24)}/>
            </TouchableOpacity>
            <View style={{width:'60%'}}>
                <Text style={styles.title}>
                    {this.props.title}
                </Text>
            </View>
            <TouchableOpacity onPress={()=>{
                if(this.state.menuStatus == "close"){
                    this.props.toggleMenu('open');
                    this.props.toggleTimer(false);
                }
                else {
                    this.props.toggleMenu("close");
                }
            }} style={[styles.button]}>
                {this.state.menuStatus == "close" ?
                <Sooji name="menu" color={COLORS.White} size={Normalize(20)}/>:
                <Sooji name="times" color={COLORS.White} size={Normalize(20)}/>}
            </TouchableOpacity>
        </View>
    )
  }
}
const styles = StyleSheet.create({
    container: {
        width:'100%',
        height: isIphoneX() ? 130 : 80,
        position: 'absolute',
        top: 0,
        paddingTop:Platform.OS == "ios" ? isIphoneX() ? 27 : 10 : 0,
        backgroundColor : COLORS.Black
    },
    title: {
        fontFamily:FONTS.secondFontFamily,
        fontSize: Normalize(16),
        color: COLORS.White,
        textAlign: 'center',
        width: '100%',
        paddingTop:23,
    },
    button: {
        width:'20%',
        alignItems: 'center',
        paddingVertical: 20
    }
})