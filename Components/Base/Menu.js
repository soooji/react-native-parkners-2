import React, { Component } from 'react'
import { Text, View,StyleSheet,TouchableOpacity,Animated,Easing,ScrollView,Platform} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { COLORS, BORDER_RADIUS, Normalize, FONTS, FLEX, ICONS_SIZE, DIMENSIONS, MODAL_STYLES,SUPPORT_NUMBER } from './../../Base/functions';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { isIphoneX } from 'react-native-iphone-x-helper';
import Communications from 'react-native-communications';
export default class componentName extends Component {
    componentDidMount() {
        
    }
  render() {
    return ( 
      <ScrollView showsVerticalScrollIndicator={false} style={{width:'100%',height: Platform.OS == "ios" ? isIphoneX() ? DIMENSIONS.height - 165 : DIMENSIONS.height - 125 : DIMENSIONS.height - 145}} contentContainerStyle={{padding:20}}>
        <View style={[FLEX.CNwSbSS,{minHeight:Platform.OS == "ios" ? isIphoneX() ? DIMENSIONS.height - 305 : DIMENSIONS.height - 180 : DIMENSIONS.height - 217}]}>
            <View>
                <TouchableOpacity onPress={()=>{
                    this.props.toggleMenu('close');
                    this.props.setCurrentBase('dashboard');
                    }} style={[styles.menuItem,{backgroundColor:COLORS.Black}]}>
                    <View style={this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC}>
                        <Icon name="home" color={COLORS.White} size={Normalize(ICONS_SIZE - 1)}/>
                        <Text style={styles.menuTextItem}>
                            {this.props.lang.Menu.dashboard}
                        </Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{this.props.toggleMenu('close');this.props.setCurrentBase('cars')}} style={[styles.menuItem,{backgroundColor:COLORS.Black}]}>
                    <View style={this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC}>
                        <Icon name="car" color={COLORS.White} size={Normalize(ICONS_SIZE - 1)}/>
                        <Text style={styles.menuTextItem}>
                            {this.props.lang.Menu.cars}
                        </Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{this.props.toggleMenu('close');this.props.setCurrentBase('wallet')}} style={[styles.menuItem,{backgroundColor:COLORS.Black}]}>
                    <View style={this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC}>
                        <Icon name="wallet" color={COLORS.White} size={Normalize(ICONS_SIZE - 1)}/>
                        <Text style={styles.menuTextItem}>
                            {this.props.lang.Menu.wallet}
                        </Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{this.props.toggleMenu('close');this.props.setCurrentBase('profile')}} style={[styles.menuItem,{backgroundColor:COLORS.Black}]}>
                    <View style={this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC}>
                        <Icon name="user" color={COLORS.White} size={Normalize(ICONS_SIZE - 1)} solid={true}/>
                        <Text style={styles.menuTextItem}>
                            {this.props.lang.Menu.profile}
                        </Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{this.props.toggleMenu('close');this.props.setCurrentBase('help-menu')}} style={[styles.menuItem,{backgroundColor:COLORS.Black}]}>
                    <View style={this.props.isRTL ? FLEX.RNwSbCC : FLEX.RrNwSbCC}>
                        <Icon name="question" color={COLORS.White} size={Normalize(ICONS_SIZE - 1)} solid={true}/>
                        <Text style={styles.menuTextItem}>
                            {this.props.lang.Menu.guide}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View>
                <TouchableOpacity onPress={()=>Communications.phonecall(SUPPORT_NUMBER,true)} style={[styles.menuItem,{paddingVertical:15,backgroundColor:COLORS.DarkGrey,marginTop:20}]}>
                    <View style={FLEX.RNwCCC}>
                        <Text style={styles.menuTextItem}>
                            {this.props.lang.Menu.support}
                        </Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{AsyncStorage.clear();this.props.setCurrentBase('auth');this.props.toggleMenu('close');}} style={[styles.menuItem,{paddingVertical:15,backgroundColor:COLORS.Red}]}>
                    <View style={FLEX.RNwCCC}>
                        <Text style={styles.menuTextItem}>
                            {this.props.lang.Menu.logOut}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
      </ScrollView>
    )
  }
}
const styles = StyleSheet.create({
    menuItem: {
        width:'100%',
        padding:Platform.OS == "ios" ? 16 : 16,
        borderRadius: BORDER_RADIUS.base,
        marginBottom:15
    },
    menuTextItem: {
        color: 'white',
        textAlign:'right',
        fontFamily: FONTS.bold,
        fontSize: Normalize(13)
    }
})