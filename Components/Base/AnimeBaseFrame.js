import React, { Component } from 'react'
import { StyleSheet , View, Animated, Easing } from 'react-native'
import { DIMENSIONS, COLORS, BORDER_RADIUS } from '../../Base/functions';
export default class AnimeBaseFrame extends Component {
    constructor(props) {
        super(props);
        this.state = {
            opacityAnime: new Animated.Value(0),
            bottomAnime: new Animated.Value(20),
            isMounted: false
        }        
    }
    componentDidMount() {
        if(this.props.baseState == this.props.componentStateName) {
            setTimeout(
                ()=>{this.setState({isMounted:true});}
            ,this.props.startDelay ? this.props.startDelay : 0);
            Animated.timing(
                this.state.opacityAnime,{
                toValue: 1,
                duration: 300,
                delay:this.props.startDelay ? this.props.startDelay : 0,
                easing: Easing.ease,
                useNativeDriver: true
            }).start();
            Animated.timing(
                this.state.bottomAnime,{
                toValue: 0,
                delay:this.props.startDelay ? this.props.startDelay : 0,
                duration: 500,
                easing: Easing.elastic(1.05),
                useNativeDriver: true
            }).start();
        }
    }
    componentWillReceiveProps(props) {
        if(props.baseState == props.componentStateName) {
            setTimeout(
                ()=>{this.setState({isMounted:true});}
            ,this.props.startDelay ? this.props.startDelay : 0);
            // this.setState({isMounted:true});
            Animated.timing(
                this.state.opacityAnime,{
                toValue: 1,
                duration: 300,
                delay:this.props.startDelay ? this.props.startDelay : 0,
                easing: Easing.ease,
                useNativeDriver: true
            }).start();
            Animated.timing(
                this.state.bottomAnime,{
                toValue: 0,
                duration: 500,
                delay:this.props.startDelay ? this.props.startDelay : 0,
                easing: Easing.elastic(1.05),
                useNativeDriver: true
            }).start();
        } else {
            Animated.timing(
                this.state.opacityAnime,{
                toValue: 0,
                duration: 300,
                easing: Easing.ease,
                useNativeDriver: true
            }).start();
            Animated.timing(
                this.state.bottomAnime,{
                toValue: 20,
                duration: 500,
                easing: Easing.elastic(1.05),
                useNativeDriver: true
            }).start();
            setTimeout(
                ()=>{this.setState({isMounted:false});}
            ,500);
        }
    }
  render() {
      if(this.state.isMounted) {
        return (
            <Animated.View style={[this.props.style,this.props.isFullSize ? {width: '100%',height: '100%',overflow:'hidden'} : {overflow:'hidden'} ,{
                opacity:this.state.opacityAnime,
                transform:[{translateY:this.state.bottomAnime}],
                }]}>
              {this.props.children}
            </Animated.View>
          )
      } else {
        return (<View style={{height:0,width:0,overflow:'hidden',position:'absolute'}}></View>)
      }
  }
}

