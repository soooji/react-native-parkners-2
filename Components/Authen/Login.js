import React, { Component } from 'react'
import { Text, View,StyleSheet,TextInput,TouchableOpacity,Platform,Animated,Easing,BackHandler} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios'
import { COLORS, BORDER_RADIUS, FLEX, FONTS, Normalize, DIMENSIONS,ALERT,ISEXPIRED,BASE_URL,SETTOKEN, USER_ROLE, Sooji } from './../../Base/functions';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AutoHeightImage from 'react-native-auto-height-image';
import Communications from 'react-native-communications';
import FullScreenLoading from './../Base/FullScreenLoading';
var counterTimeOut;
export default class componentName extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading:false,

            tempToken: '',

            authState:'phone', //code register phone
            phone: '',
            firstName:'',
            lastName:'',
            code:'',
            introducer: '',

            counter: 0,

            alertAnime: new Animated.Value(-90),
            mainAnime: new Animated.Value(0),
            loadingText:''
        }
    }
    componentDidMount() {
        this.backListener = BackHandler.addEventListener('hardwareBackPress', ()=>this.handleBackButton());
        this.setState({loadingText:this.props.lang.Base.Loading.wait});
        AsyncStorage.setItem("isIntroSeen","true");
        AsyncStorage.setItem('reg', "ir");
    }
    componentWillUnmount() {
        this.backListener.remove();
    }
    handleBackButton() {
        if(this.state.authState != 'phone') {
            this.resetFileds();
            this.setState({authState:'phone'});
            return true;
        } else {
            return true;
        }
    }
    sendPhone = async()=> {
        this.setState({isLoading:true});
        if(this.state.phone.length == 0) {
            this.toggleAlert(true,this.props.lang.Login.enterPhone);
            return
        }
        const options = {
            method: 'GET',
            url: `${BASE_URL}auth/check_phone/${this.state.phone}/`,
            headers: { 'content-type': 'application/json','role':USER_ROLE},
        };
        axios(options)
        .then(()=>{
            this.setState({authState:'code',isLoading:false});
            this.countItDown(60);
        })
        .catch(e=>{
            this.setState({isLoading:false});
            if(e.response) {
                if(e.response.status == 500) this.toggleAlert(true);
                else this.toggleAlert(true,e.response.data.error_message);
            } else this.toggleAlert(true);
        })
    }
    checkCode = async()=> {
        this.setState({isLoading:true});
        const options = {
            method: 'GET',
            headers: {'content-type': 'application/json','role':USER_ROLE},
            url: `${BASE_URL}auth/check_code/${this.state.phone}/${this.state.code}/`,
        };
        axios(options)
        .then(r=>{
            this.setState({isLoading:false});
            if(r.status == 201 || r.status == 202) {
                AsyncStorage.setItem('phone', r.data.user.phone.toString());
                this.setState({authState:'register',tempToken: r.data.token});
            } else {
                AsyncStorage.setItem('token', r.data.token);
                AsyncStorage.setItem('first_name', r.data.user.first_name);
                AsyncStorage.setItem('last_name', r.data.user.last_name);
                AsyncStorage.setItem('phone', r.data.user.phone.toString());

                //NOTIF
                this.props.notifReg(r.data.user.phone.toString());
                // this.registerNotif(r.data.user.phone.toString());
                //END NOTIF
                //sentry log
                // Sentry.setUserContext({
                //     phone: r.data.user.phone.toString(),
                //     firstName: r.data.user.first_name,
                //     lastName: r.data.user.last_name,
                // });
                //sentry log
                this.props.setCurrentBase('dashboard');
            }
        })
        .catch(e=>{
            this.setState({isLoading:false});
            if(e.response) {
                if(e.response.status == 500) this.toggleAlert(true);
                else this.toggleAlert(true,e.response.data.error_message);
            } else this.toggleAlert(true);
        })
    }
    register = async()=> {
        try {
            const token = this.state.tempToken;
            this.setState({isLoading:true});
            const options = {
                method: 'POST',
                headers: { 'content-type': 'application/json','token': token,'role':USER_ROLE},
                data: JSON.stringify({
                    'profile': {
                        'user': {
                            'first_name': this.state.firstName,
                            'last_name': this.state.lastName
                        },                        
                        'recommended_by': this.state.introducer
                    }
                }),
                url: `${BASE_URL}users/profile/`,
            };
            axios(options)
            .then(r=>{
                AsyncStorage.setItem('first_name', r.data.user.first_name);
                AsyncStorage.setItem('last_name', r.data.user.last_name);
                AsyncStorage.setItem('phone', r.data.user.phone.toString());

                this.props.notifReg(r.data.user.phone.toString());

                //sentry log
                // Sentry.setUserContext({
                //     phone: r.data.user.phone.toString(),
                //     firstName: r.data.user.first_name,
                //     lastName: r.data.user.last_name,
                // });
                //sentry log
                // this.registerNotif(r.data.user.phone.toString());
                SETTOKEN(r.data.token);
            })
            .then(()=>{
                this.setState({isLoading:false});
                this.closeAll();
            })
            .catch(e=>{
                if(!e.response) return
                this.setState({isLoading:false});
                if(ISEXPIRED(e)) {this.setState({authState:'phone'});return;}
                if(e.response) {
                    if(e.response.status == 500) this.toggleAlert(true);
                    else this.toggleAlert(true,e.response.data.error_message);
                } else this.toggleAlert(true);
            })
        } catch (error) {
            this.setState({isLoading:false});
            this.toggleAlert(true,error);
        }
    }
    resetFileds() {
        this.setState({
            phone:'',
            firstName:'',
            lastName:'',
            code:'',
            introducer: ''
        })
    }
  render() {
    return (
      <Animated.View style={[{transform:[{translateY:this.state.mainAnime}]},styles.container]}>

        <FullScreenLoading isLoading={this.state.isLoading} text={this.state.loadingText}/>
        <Animated.View style={[FLEX.RNwSbCC,ALERT.alertBox,{transform: [{translateY:this.state.alertAnime}],height:50}]}>
            {/* <TouchableOpacity onPress={()=>this.toggleAlert(false)} style={ALERT.closeAlertBox}>
                <Icon name="times" color={'rgba(255,255,255,.6)'} size={Normalize(12)}/>
            </TouchableOpacity> */}
            <Text style={ALERT.alertBoxText}>{this.state.alertText}</Text>
        </Animated.View>
        {this.state.authState != "phone" ?
            <TouchableOpacity onPress={()=>this.setState({
                authState:'phone',
                phone:'',
                firstName:'',
                lastName:'',
                introducer:'',
                code:'',
            })} style={styles.backButton}>
                <Sooji name="left" color={COLORS.White} size={Normalize(13)}/>
            </TouchableOpacity>
            : null}

        <KeyboardAwareScrollView bounces={false} keyboardShouldPersistTaps={'handled'}>
            <View style={[FLEX.CWCC,{minHeight: DIMENSIONS.height - 70}]}>
                <AutoHeightImage source={require('./../../Static/white-logo.png')} width={DIMENSIONS.width/4.7}/>
                <Text style={styles.logoText}>Parkners</Text>
            
                <View style={{minHeight:300}}>

                {this.state.authState == "phone" ?
                    <View>
                    <TextInput
                        placeholderTextColor="white"
                        value={this.state.phone}
                        style={styles.input}
                        editable={!this.state.isLoading}
                        clearButtonMode={'while-editing'}
                        placeholder={this.props.lang.Login.phone}
                        keyboardType="number-pad"
                        autoCorrect={false}
                        onChangeText={(text) => this.setState({phone: text})}
                        keyboardAppearance={"dark"}
                    />
                    <TouchableOpacity
                    onPress={()=>this.sendPhone()}
                    style={styles.submitButton}><Text style={styles.submitText}>{this.props.lang.Login.login}</Text></TouchableOpacity>
                </View> : null}
                {/* <AnimeBaseFrame baseState={this.state.authState} componentStateName={"phone"} isFullSize={false} startDelay={700}> */}
                {/* </AnimeBaseFrame> */}


                {/* <AnimeBaseFrame baseState={this.state.authState} componentStateName={"register"} isFullSize={false} startDelay={750}> */}
                {this.state.authState == "register" ?
                <View>
                    <TextInput
                        placeholderTextColor="white"
                        value={this.state.firstName}
                        style={styles.input}
                        editable={!this.state.isLoading}
                        clearButtonMode={'while-editing'}
                        placeholder={this.props.lang.Login.firstName}
                        // keyboardType="number-pad"
                        autoCorrect={false}
                        onChangeText={(text) => this.setState({firstName: text})}
                        keyboardAppearance={"dark"}
                    />
                    <TextInput
                        placeholderTextColor="white"
                        value={this.state.lastName}
                        style={styles.input}
                        editable={!this.state.isLoading}
                        clearButtonMode={'while-editing'}
                        placeholder={this.props.lang.Login.lastName}
                        // keyboardType="number-pad"
                        autoCorrect={false}
                        onChangeText={(text) => this.setState({lastName: text})}
                        keyboardAppearance={"dark"}
                    />
                    <TextInput
                        placeholderTextColor="white"
                        value={this.state.introducer}
                        style={styles.input}
                        editable={!this.state.isLoading}
                        clearButtonMode={'while-editing'}
                        placeholder={this.props.lang.Login.invitationCode}
                        // keyboardType="number-pad"
                        autoCorrect={false}
                        onChangeText={(text) => this.setState({introducer: text})}
                        keyboardAppearance={"dark"}
                    />
                    <TouchableOpacity onPress={()=>{this.register()}} style={styles.submitButton}><Text style={styles.submitText}>{this.props.lang.Login.register}</Text></TouchableOpacity>
                    <TouchableOpacity onPress={()=>{Communications.web(`${BASE_URL}terms`)}} style={[FLEX.RNwCCC,{width:DIMENSIONS.width - 100,marginLeft:'auto',marginRight:'auto',paddingTop:15}]}>
                        <Text style={{opacity:.8,color:'white',fontFamily:FONTS.bold,fontSize:Normalize(9),textAlign:'center',lineHeight:23}}>
                        ثبت نام به معنی مطالعه و پذیرش شرایط و قوانین استفاده می باشد
                        </Text>
                    </TouchableOpacity>
                </View>
                : null}
                {/* </AnimeBaseFrame> */}

                {/* <AnimeBaseFrame baseState={this.state.authState} componentStateName={"code"} isFullSize={false} startDelay={700}> */}
                {this.state.authState == "code" ?
                <View>
                    <TextInput
                        placeholderTextColor="white"
                        value={this.state.code}
                        style={styles.input}
                        editable={!this.state.isLoading}
                        clearButtonMode={'while-editing'}
                        placeholder={this.props.lang.Login.codeIsSended}
                        keyboardType="number-pad"
                        autoCorrect={false}
                        onChangeText={(text) => this.setState({code: text})}
                        keyboardAppearance={"dark"}
                    />
                    <TouchableOpacity onPress={()=>{this.checkCode()}} style={styles.submitButton}><Text style={styles.submitText}>{this.props.lang.Login.confirm}</Text></TouchableOpacity>

                    {this.state.counter>0 ?
                        <Text style={styles.resendText}>
                            {`${this.state.counter} ${this.props.lang.Login.secondsToResend}`}
                        </Text> :
                        <TouchableOpacity onPress={()=>{this.sendPhone()}} style={styles.resendButton}>
                            <Text style={styles.resendButtonText}>
                            ارسال مجدد
                                {/* {this.props.lang.resendCode} */}
                            </Text>
                        </TouchableOpacity>}
                </View>
                : null}
                {/* </AnimeBaseFrame> */}

            </View>
            </View>
        </KeyboardAwareScrollView>
      </Animated.View>
    )
  }
  closeAll() {
    this.props.setCurrentBase('dashboard');
    Animated.timing(
        this.state.mainAnime,
        {
          toValue: -(isIphoneX() ? DIMENSIONS.height - 70 : DIMENSIONS.height - 30),
          easing: Easing.exp,
          duration: 1000,
          useNativeDriver:true
        },
      ).start();
}
countItDown(time) {
    
    this.setState({counter: time});
    counterTimeOut = setInterval(()=>{
        if(this.state.counter>0) {
            this.setState({counter: this.state.counter - 1});
        } else {
            clearInterval(counterTimeOut);
            return;
        }
    },1000);
}
toggleAlert = (open,text = this.props.lang.Base.Error.simple) => {
    if(open) {
        this.setState({alertText: text})
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: isIphoneX() ? 50 : 30,
          easing: Easing.elastic(1.2),
          duration: 1000,
          useNativeDriver:true
        },
      ).start();
      setTimeout(()=>{
        this.toggleAlert(false);
      },3000)
    } else {
        setTimeout(() => {
            this.setState({alertText: ''})
          }, 500);
      Animated.timing(
        this.state.alertAnime,
        {
          toValue: -90,
          easing: Easing.ease,
          duration: 500,
          useNativeDriver:true
        },
      ).start();
    }
  }
}

const styles = StyleSheet.create({
    backButton: {
        zIndex:1111111,
        position:'absolute',left:0,top:isIphoneX() ? 50 : 30,borderTopRightRadius:BORDER_RADIUS.base,borderBottomRightRadius:BORDER_RADIUS.base,backgroundColor:'#333333',paddingVertical:16,paddingLeft:40,paddingRight:17,
    },
    inputAccessoryText: {
        color:COLORS.White,
        fontFamily:Platform.OS == "ios" ? FONTS.bold : FONTS.regular,
        fontSize:Normalize(12),
        paddingVertical:7,
        paddingRight:7
    },
    inputAccessory: {
        paddingRight:10   
    },
    resendButtonText: {
        textAlign:'center',
        fontSize: Normalize(12),
        color:COLORS.White,
        fontFamily: FONTS.extraBold
    },
    resendButton: {
        backgroundColor:'#333333',
        borderRadius:BORDER_RADIUS.items,
        paddingVertical:12,
        marginLeft:'auto',
        marginRight:'auto',
        paddingHorizontal:20,
        marginTop:10
    },
    resendText: {
        textAlign:'center',
        fontSize: Normalize(11),
        paddingTop:30,
        color:COLORS.White,
        fontFamily: FONTS.bold
    },
    submitButton: {
        backgroundColor:COLORS.White,
        paddingVertical:14,
        marginTop:20,
        borderRadius:BORDER_RADIUS.items,
        paddingHorizontal: 40,
        marginLeft:'auto',
        marginRight:'auto'
    },
    submitText: {
        textAlign:'center',
        fontSize: Normalize(12),
        color:COLORS.Black,
        fontFamily: FONTS.bold
    },
    input: {
        backgroundColor:'#333333',
        borderRadius:BORDER_RADIUS.base,
        paddingVertical:Platform.OS == "ios" ? 15 : 12,
        textAlign:'center',
        width:DIMENSIONS.width-100,
        borderWidth:3,
        borderColor:COLORS.White,
        color:COLORS.White,
        fontFamily:FONTS.bold,
        fontSize:Normalize(15),
        marginTop:15,
    },
    logoText: {
        textAlign:'center',
        fontSize: Normalize(22),
        color:COLORS.White,
        paddingTop:10,
        fontFamily: FONTS.secondFontFamily,
        marginBottom:25
    },
    container: {
        width:'100%',
        position: 'absolute',
        zIndex: 1111111111,
        height:Platform.OS =="ios" ? isIphoneX() ? DIMENSIONS.height - 70 : DIMENSIONS.height - 55 : DIMENSIONS.height - 80,
        // top: isIphoneX() ? 95 : 70,
        borderBottomLeftRadius: BORDER_RADIUS.base,
        borderBottomRightRadius: BORDER_RADIUS.base,
        backgroundColor : COLORS.Black,
        overflow:'hidden',
    }
})